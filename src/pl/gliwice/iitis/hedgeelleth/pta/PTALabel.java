/*
 * PTALabel.java
 *
 * Created on Jul 28, 2008
 *
 * Copyright (c) 2008, 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import java.util.Objects;

/**
 * Transition label or a declaration of one.<br>
 * 
 * Labels are identified by their names, and not instances.
 * 
 * @author Artur Rataj
 */
public class PTALabel implements Cloneable {
    /**
     * <code>SENDER</code> triggers only <code>RECEIVER</code> counterparts.
     * If there is more than a single receiver, an indeterminate one is chosen. If no receiver
     * is waiting, a sender blocks. Senders belong to the class of directional labels.<br>
     * 
     * <code>RECEIVER</code> waits to be chosen by a sender. Receivers belong to the
     * class of directional labels.<br>
     * 
     * <code>DIRECTIONAL</code> is used only in a declaration of a name of a directional
     * label; the declaration means, that labels having the name can be either
     * <code>SENDER</code> or <code>RECEIVER</code>.<br>
     * 
     * <code>BARRIER</code> waits for all labels in any module different from its own.
     * Barrier labels are not directional, and even if there are more than two synchronising
     * barrier labels, they trigger in concert. So, the trigging produces no
     * non--determinism.<br>
     * 
     * <code>FREE</code> is user--defined, not used for synchronisation, merely as a marker.
     * Must be unique within a whole model.
     * 
     * Directional labels are used by Uppaal and Verics, barrier labels are used by Prism.
     */
    public enum Type {
        DIRECTIONAL,
        SENDER,
        RECEIVER,
        BARRIER,
        FREE,
    }
    /**
     * Name of this label.
     */
    final public String name;
    /**
     * Type of this label.
     */
    public Type type;
    /**
     * Use an urgent channel.
     */
    public boolean urgent;
    /**
     * If it is a broadcast label. True only for directional labels.
     */
    final public boolean broadcast;
    
    /**
     * Creates a new instance of PTALabel.
     * 
     * @param name                      name of this label
     * @param type                      type of this label
     * @param broadcast                 if a broadcast label; can be true only
     *                                  for directional labels.
     */
    public PTALabel(String name, Type type, boolean broadcast)
            throws PTAException {
        this.name = name;
        this.type = type;
        this.broadcast = broadcast;
        if(!isDirectional() && this.broadcast)
            throw new PTAException(null, "only a directional label can have the " +
                    "broadcast attribute");
    }
    /**
     * If this is a directional label or a declaration of a directional label.
     * 
     * @return if directional
     */
    public final boolean isDirectional() {
        switch(type) {
            case DIRECTIONAL:
            case SENDER:
            case RECEIVER:
                return true;

            case BARRIER:
            case FREE:
                return false;

            default:
                throw new RuntimeException("unknown type");
        }
    }
    /**
     * Creates a new label opposite to this one, that is, having the same
     * properties except for an opposite value of <code>sender</code>.<br>
     * 
     * If this label is not directional, a runtmie exception is thrown.
     * 
     * @return                          opposite label
     */
    public PTALabel newOpposite() {
        try {
            PTALabel label;
            switch(type) {
                case DIRECTIONAL:
                    throw new RuntimeException("can not be called on label declaration");

                case SENDER:
                    label = new PTALabel(name, Type.RECEIVER, broadcast);
                    break;

                case RECEIVER:
                    label = new PTALabel(name, Type.SENDER, broadcast);
                    break;

                case BARRIER:
                case FREE:
                    throw new RuntimeException("label not directional");

                default:
                    throw new RuntimeException("unknown type");
            }
            return label;
        } catch(PTAException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    /**
     * Returns postfix of this label, that is, empty or <code>!</code> or
     * <code>?</code>, depending on whether this label is, respectively,
     * a barrier, a sender or a receiver. Broadcast flag is not taken into account
     * by this method.
     * 
     * @return                          postfix
     */
    public String getPostfix() {
        switch(type) {
            case DIRECTIONAL:
                throw new RuntimeException("can not be called on label declaration");
                
            case SENDER:
                return "!";
                
            case RECEIVER:
                return "?";
                
            case BARRIER:
            case FREE:
                return "";
                
            default:
                throw new RuntimeException("unknown type");
        }
    }
    /**
     * Returns the name of this label postfixed with what
     * <code>getPostfix()</code> returns.<br>
     * 
     * Broadcast flag is not taken into account by this
     * method.
     * 
     * @return                          postfixed name
     */
    public String getPostfixedName() {
        return name + getPostfix();
    }
    @Override
    public boolean equals(Object o) {
        if(!(o instanceof PTALabel))
            throw new RuntimeException("invalid comparison");
        PTALabel other = (PTALabel)o;
        return name.equals(other.name) &&
                type == other.type &&
                broadcast == other.broadcast;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.name);
        hash = 59 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 59 * hash + (this.broadcast ? 1 : 0);
        return hash;
    }
    @Override
    public PTALabel clone() {
        try {
            PTALabel copy = (PTALabel)super.clone();
            return copy;
        } catch(CloneNotSupportedException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    @Override
    public String toString() {
        return getPostfixedName() + (broadcast ? "/broadcast" : "");
    }
}
