/*
 * PTAUnaryExpression.java
 *
 * Created on May 8, 2008
 *
 * Copyright (c) 2008, 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeConstant;
import pl.gliwice.iitis.hedgeelleth.compiler.code.RangedCodeValue;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.CodeOpUnaryExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.UnaryExpression;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.interpreter.op.CodeArithmetics;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;

/**
 * An unary expression in a PTA transition, with an optional
 * target variable.<br>
 * 
 * The target variable can be null if this expression is a branch
 * condition.<br>
 * 
 * To wrap a constant, use
 * <code>CodeOpUnaryExpression.Op.NONE</code>.
 * 
 * @author Artur Rataj
 */
public class PTAUnaryExpression extends AbstractPTAExpression {
    /**
     * Operand.
     */
    public AbstractPTAValue sub;
    /**
     * Operator, can be NONE.
     */
    public CodeOpUnaryExpression.Op operator;
    
    /**
     * Creates a new instance of PTAUnaryExpression, with
     * null target.
     * 
     * @param sub                       operand
     * @param operator                  operator
     */
    public PTAUnaryExpression(AbstractPTAValue sub,
            CodeOpUnaryExpression.Op  operator) {
        super();
        this.sub = sub;
        this.operator = operator;
    }
    @Override
    public boolean isNestable() {
        return true;
    }
    @Override
    public AbstractPTAExpression newComplementary() {
        UnaryExpression.Op treeOperator = CodeOpUnaryExpression.
                operatorCodeToTree(operator);
        treeOperator = treeOperator.newOppositeBoolean();
        PTAUnaryExpression o = new PTAUnaryExpression(sub,
                CodeOpUnaryExpression.operatorTreeToCode(treeOperator));
        return o;
    }
    @Override
    public boolean isEquivalentCondition(
            AbstractPTAExpression other) {
        if(!isConditional() || !other.isConditional())
            throw new RuntimeException("not conditions");
        if(other instanceof PTAUnaryExpression) {
            PTAUnaryExpression ue = (PTAUnaryExpression)other;
            return operator == ue.operator && sub.equals(ue.sub);
        } else
            return false;
    }
    @Override
    public PTAConstant getConstant() {
        if(operator == CodeOpUnaryExpression.Op.NONE &&
                sub instanceof PTAConstant) {
            PTAConstant constant = (PTAConstant)sub;
            return constant;
        } else
            return null;
    }
    @Override
    public boolean isFloating() {
        if(operator == CodeOpUnaryExpression.Op.UNARY_CAST)
            throw new RuntimeException("casts not supported");
        return sub.floating;
    }
    @Override
    public boolean isConditional() {
        return operator == CodeOpUnaryExpression.Op.NONE ||
                operator == CodeOpUnaryExpression.Op.UNARY_CONDITIONAL_NEGATION;
    }
    @Override
    public AbstractPTAExpression probability() {
        return null;
    }
    @Override
    public SortedSet<AbstractPTAVariable> getSources() {
        SortedSet<AbstractPTAVariable> out = super.getSources();
        // all variables in arguments are sources in this context
        out.addAll(sub.getVariables());
        return out;
    }
    @Override
    public List<PTARange> trueRanges() {
        if(!isConditional())
            throw new ArithmeticException("conditional expression expected");
        try {
            List<PTARange> out = new LinkedList<>();
            Set<AbstractPTAVariable> sources = getSources();
            if(sources.size() != 1)
                throw new ArithmeticException("a source variable expected");
            AbstractPTAVariable variable = sources.iterator().next();
            PTARange sourceRange = variable.range;
            switch(operator) {
                case UNARY_CONDITIONAL_NEGATION:
                    if(sourceRange.range.getMinFloatingPoint() == 0)
                        out.add(new PTARange(null, 0, 0));
                    break;

                case NONE:
                    if(sourceRange.range.getMaxFloatingPoint() == 1)
                        out.add(new PTARange(null, 1, 1));
                    break;

                default:
                    throw new RuntimeException("unexpected operator");
            }
            return out;
        } catch(PTAException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    @Override
    public PTAConstant evaluate(Map<AbstractPTAVariable,
            PTAConstant> knowns) {
        CodeOpUnaryExpression ue = new CodeOpUnaryExpression(
                operator, null, null);
        ue.sub = toConstant(sub, knowns);
        if(ue.sub == null)
            return null;
        else
            try {
                switch(ue.op) {
                    case UNARY_CONDITIONAL_NEGATION:
                        // <code>CodeArithmetics</code> requires a boolean for
                        // a conditional negation, while this backend uses
                        // an arithmetic value instead
                        ue.sub = new RangedCodeValue(new CodeConstant(null,
                            new Literal(
                                ((CodeConstant)ue.sub.value).value.getDouble() != 0.0)));
                        break;
                        
                    case NONE:
                        return fromLiteral(((CodeConstant)ue.sub.value).value);

                    case UNARY_NEGATION:
                    case UNARY_ABS:
                        break;
                        
                    default:
                        throw new RuntimeException("unknown operator");
                }
                return fromLiteral(CodeArithmetics.computeValue(ue,
                        null));
            } catch(InterpreterException e) {
                throw new RuntimeException("unexpected: " + e);
            }
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof PTAUnaryExpression) {
            PTAUnaryExpression other = (PTAUnaryExpression)o;
            return operator == other.operator &&
                     sub.equals(other.sub);
        } else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.sub);
        hash = 29 * hash + (this.operator != null ? this.operator.hashCode() : 0);
        return hash;
    }
    @Override
    public PTAUnaryExpression clone() {
        PTAUnaryExpression copy = (PTAUnaryExpression)super.clone();
        copy.sub = sub;
        copy.operator = operator;
        return copy;
    }
    @Override
    public String toString() {
        String s;
        String t = "";
        if(operator == CodeOpUnaryExpression.Op.UNARY_CAST)
            s = "";
        else if(operator == CodeOpUnaryExpression.Op.UNARY_ABS) {
            s = "abs(";
            t = ")";
        } else
            s = UnaryExpression.getOperatorName(
                CodeOpUnaryExpression.operatorCodeToTree(operator),
                null)[0];
        return super.toString() + s + sub.toString() + t;
    }
}
