/*
 * PTALocationGuard.java
 *
 * Created on Oct 16, 2014
 *
 * Copyright (c) 2014 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.pta.values.*;

/**
 * A guard, representing if all/some of certain locations are reached at the same
 * time. See <code>type</code>
 * for an exact definition. This class can not be replaced by <code>PTAUnaryExpression</code>
 * or <code>PTABinaryExpression</code>, because it is only known in the backend, how to
 * relate state variables to the locations.<br>
 * 
 * The target variable is null, as this is a boolean expression.<br>
 * 
 * @author Artur Rataj
 */
public class PTALocationGuard extends AbstractPTAExpression {
//    /**
//     * <p>Type of this guard. NEGATED is put into loops outside locks.
//     * DIRECT is put into synchronising transitions. NEGATED_COMMON
//     * is put into loops inside locks, as these loops need to serve
//     * several synchronisations at once.</p>
//     * 
//     * <p>For definitions, see the documentation of the field <code>direct</code>.</p>
//     * 
//     */
//    public enum Type {
//        NEGATED,
//        NEGATED_COMMON,
//        DIRECT
//    };
//    /**
//     * Locations, tested for being reached at the same time. The
//     * locations are the sources of transitions in this list.
//     */
//    public List<PTATransition> LOCATIONS;
//    /**
//     * <p>Direct or negated or common negated expression.</p>
//     * 
//     * <p>Let there be a set of automatons, whose locations are listed
//     * in <code>locations</code>.</p>
//     * 
//     * <p>If the field is NEGATED, this expression is true if at least a single of these
//     * automatons is not currently in any of its subset of <code>locations</code>.
//     * If the field is NEGATED_COMMON, this expression is true if all of these
//     * automatons are currently not in any of its subset of <code>locations</code>.
//     * If the field is DIRECT, this expression is true if all of these automatons are currently
//     * within their subsets of <code>locations</code>.</p>
//     */
//    public Type type;
//    
//    /**
//     * Creates a new instance of PTALocationGuard.
//     * 
//     * @param locations list of transitions, whose source locations
//     * are the ones checked for being reached
//     * @param type passed to <code>setType()<code>
//     */
//    public PTALocationGuard(List<PTATransition> locations, Type type) {
//        super();
//if(locations.size() > 1)
//    locations = locations;
//        LOCATIONS = new LinkedList<>();
//        addLocations(locations);
//        this.type = type;
//    }
//    /**
//     * Set, if this expression is direct or negated.
//     * 
//     * @param direct copied to the field <code>direct</code>
//     */
//    public void setType(Type type) {
//        this.type = type;
//    }
//    /**
//     * If this expression is direct or negated.
//     * 
//     * @return the value of the field <code>direct</code>
//     */
//    public Type getType() {
//        return type;
//    }
//    /**
//     * Adds locations.
//     * 
//     * @param locations locations to add
//     */
//    public void addLocations(Collection<PTATransition> locations) {
//        LOCATIONS.addAll(locations);
//                }
//    /**
//     * Extracts the tested locations, that is, source states, of
//     * transitions in <code>LOCATIONS</code>.
//     * 
//     * @return a list of locations
//     */
//    public List<PTAState> getLocations() {
//        List<PTAState> source = new LinkedList<>();
//        for(PTATransition l : LOCATIONS)
//            source.add(l.sourceState);
//        return source;
//    }
//    /**
//     * Returns states sorted by their PTAs.
//     * 
//     * @return states sorted by owners
//     */
//    public Map<PTA, SortedSet<PTAState>> byOwner() {
//        Map<PTA, SortedSet<PTAState>> owners = new HashMap<>();
//        for(PTAState l : getLocations()) {
//                    SortedSet<PTAState> set = owners.get(l.owner);
//                    if(l.owner == null)
//                        throw new RuntimeException("no owner set");
//                    if(set == null) {
//                        set = new TreeSet<>();
//                        owners.put(l.owner, set);
//                    }
//                    set.add(l);
//                }
//        return owners;
//    }
//    @Override
//    public boolean isNestable() {
//        return false;
//    }
//    @Override
//    public AbstractPTAExpression newComplementary() {
//        PTALocationGuard opposite = clone();
//        switch(getType()) {
//            case DIRECT:
//                opposite.setType(Type.NEGATED);
//                break;
//                
//            case NEGATED:
//                opposite.setType(Type.DIRECT);
//                break;
//                
//            case NEGATED_COMMON:
//                throw new RuntimeException("no complementary expression");
//                
//            default:
//                throw new RuntimeException("unknown type");
//        }
//        return opposite;
//    }
//    @Override
//    public boolean isEquivalentCondition(
//            AbstractPTAExpression other) {
//        if(!isConditional() || !other.isConditional())
//            throw new RuntimeException("not conditions");
//        if(other instanceof PTALocationGuard) {
//            PTALocationGuard lg = (PTALocationGuard)other;
//            if(type != lg.type)
//                return false;
//            List<PTAState> source = getLocations();
//            List<PTAState> lgSource = lg.getLocations();
//                    for(PTAState l : source)
//                        if(!lgSource.contains(l))
//                            return false;
//                    for(PTAState l : lgSource)
//                        if(!source.contains(l))
//                            return false;
//            return true;
//        } else
//            return false;
//    }
//    @Override
//    public PTAConstant getConstant() {
//        return null;
//    }
//    @Override
//    public boolean isFloating() {
//        return false;
//    }
//    @Override
//    public boolean isConditional() {
//        return true;
//    }
//    @Override
//    public AbstractPTAExpression probability() {
//        return null;
//    }
//    @Override
//    public SortedSet<AbstractPTAVariable> getSources() {
//        SortedSet<AbstractPTAVariable> out = super.getSources();
//        // // all variables in arguments are sources in this context
//        // out.addAll(sub.getVariables());
//        return out;
//    }
//    @Override
//    public List<PTARange> trueRanges() {
//        List<PTARange> out = new LinkedList<>();
//        return out;
//    }
//    @Override
//    public PTAConstant evaluate(Map<AbstractPTAVariable,
//            PTAConstant> knowns) {
//        return null;
//    }
//    @Override
//    public boolean equals(Object o) {
//        return isEquivalentCondition((AbstractPTAExpression)o);
//    }
//    @Override
//    public PTALocationGuard clone() {
//        PTALocationGuard copy = (PTALocationGuard)super.clone();
//        copy.LOCATIONS = new LinkedList<>(LOCATIONS);
//        copy.type = type;
//        return copy;
//    }
//    @Override
//    public String toString() {
//        StringBuilder s = new StringBuilder();
//        s.append(type.name() + "<<");
//        for(PTAState l : getLocations()) {
//                    s.append(" ");
//                    s.append((l.owner != null ? l.owner.name : "?") + ":" + l.index);
//                }
//        s.append(" >>");
//        return super.toString() + s.toString();
//    }



    /**
     * <p>Type of this guard. NEGATED is put into loops outside locks.
     * DIRECT is put into synchronising transitions. NEGATED_COMMON
     * is put into loops inside locks, as these loops need to serve
     * several synchronisations at once.</p>
     * 
     * <p>For definitions, see the documentation of the field <code>direct</code>.</p>
     * 
     */
    public enum Type {
        NEGATED,
        NEGATED_COMMON,
        DIRECT
    };
    /**
     * Locations, tested for being reached at the same time. The
     * locations are the sources of transitions in this list. Used by
     * DIRECT and NEGATED_COMMON.
     */
    public List<PTATransition> LOCATIONS_COMMON;
    /**
     * Locations, tested for being reached at the same time. The
     * locations are the sources of transitions in this list. Keys with label
     * names.  Used by NEGATED. 
     */
    public SortedMap<String, List<PTATransition>> LOCATIONS;
    /**
     * <p>Direct or negated or common negated expression.</p>
     * 
     * <p>Let there be a set of automatons, whose locations are listed
     * in <code>locations</code>.</p>
     * 
     * <p>If the field is NEGATED, this expression is true if at least a single of
     * automatons in each of <code>LOCATIONS</code> is not currently in any
     * of its sync subsets.
     * If the field is NEGATED_COMMON, this expression is true if all automatons descibed in
     * in <code>LOCATIONS_COMMON</code> are currently not in any of its sync subsets.
     * -- can be used by locks, as these have directional labels.
     * If the field is DIRECT, this expression is true if all of automatons in
     * <code>LOCATIONS_COMMON</code> are currently within their sync subsets.
     */
    public Type type;
    
    /**
     * Creates a new instance of PTALocationGuard.
     * 
     * @param locations list of transitions, whose source locations
     * are the ones checked for being reached; can be completed
     * later
     * @param type passed to <code>setType()<code>
     */
    public PTALocationGuard(List<PTATransition> locations, Type type) {
        super();
//if(locations.size() > 1)
//    locations = locations;
        setType(type);
        switch(getType()) {
            case DIRECT:
            case NEGATED_COMMON:
                LOCATIONS_COMMON = new LinkedList<>();
                break;
                
            case NEGATED:
                LOCATIONS = new TreeMap<>();
                break;
                
            default:
                throw new RuntimeException("unknown type");
        }
        addLocations(locations);
    }
    /**
     * Sets the value of the field <code>direct</code>
     * 
     * @param type
     */
    public final void setType(Type type) {
        this.type = type;
    }
    /**
     * Returns type.
     * 
     * @return the value of the field <code>direct</code>
     */
    public final Type getType() {
        return type;
    }
    /**
     * A single empty string for DIRECT and NEGATED_COMMON, names of each label
     * for NEGATED.
     */
    public Set<String> getLabelAlternatives() {
        SortedSet<String> out = new TreeSet<>();
        switch(getType()) {
            case DIRECT:
            case NEGATED_COMMON:
                out.add("");
                break;
                
            case NEGATED:
                out.addAll(LOCATIONS.keySet());
                break;
                
            default:
                throw new RuntimeException("unknown type");
        }
        return out;
    }
    /**
     * Adds locations.
     * 
     * @param locations locations to add
     */
    public void addLocations(Collection<PTATransition> locations) {
        switch(getType()) {
            case DIRECT:
            case NEGATED_COMMON:
                LOCATIONS_COMMON.addAll(locations);
                break;
                
            case NEGATED:
                String name = locations.iterator().next().label.name;
                List<PTATransition> set = LOCATIONS.get(name);
                if(set == null) {
                    set = new LinkedList<>();
                    LOCATIONS.put(name, set);
                }
                set.addAll(locations);
                break;
                
            default:
                throw new RuntimeException("unknown type");
        }
    }
    /**
     * Extracts the tested locations, that is, source states, of
     * transitions in <code>LOCATIONS_COMMON</code>.
     * 
     * @return a list of locations
     */
    public List<PTAState> getLocationsCommon() {
        List<PTAState> source = new LinkedList<>();
        for(PTATransition l : LOCATIONS_COMMON)
            source.add(l.sourceState);
        return source;
    }
    /**
     * Extracts the tested locations, that is, source states, of
     * transitions in <code>LOCATIONS_COMMON</code>.
     * 
     * @return a list of locations
     */
    public SortedMap<String, List<PTAState>> getLocations() {
        SortedMap<String, List<PTAState>> source = new TreeMap<>();
        for(String name : LOCATIONS.keySet()) {
            List<PTAState> states = new LinkedList<>();
            for(PTATransition x : LOCATIONS.get(name))
                states.add(x.sourceState);
            source.put(name, states);
        }
        return source;
    }
    /**
     * Returns states sorted by their PTAs.
     * 
     * @param labelName empty string for DIRECT and NEGATED_COMMON, label
     * name of an alternative for NEGATED
     * @return states sorted by owners
     */
    public Map<PTA, SortedSet<PTAState>> byOwner(String labelName) {
        Map<PTA, SortedSet<PTAState>> owners = new HashMap<>();
        switch(getType()) {
            case DIRECT:
            case NEGATED_COMMON:
                if(!labelName.isEmpty())
                    throw new RuntimeException("unexpected label name");
                for(PTAState l : getLocationsCommon()) {
                    SortedSet<PTAState> set = owners.get(l.owner);
                    if(l.owner == null)
                        throw new RuntimeException("no owner set");
                    if(set == null) {
                        set = new TreeSet<>();
                        owners.put(l.owner, set);
                    }
                    set.add(l);
                }
                break;
                
            case NEGATED:
                if(labelName.isEmpty())
                    throw new RuntimeException("label name expected");
                for(PTAState l : getLocations().get(labelName)) {
                    SortedSet<PTAState> set = owners.get(l.owner);
                    if(l.owner == null)
                        throw new RuntimeException("no owner set");
                    if(set == null) {
                        set = new TreeSet<>();
                        owners.put(l.owner, set);
                    }
                    set.add(l);
                }
                break;
                
            default:
                throw new RuntimeException("unknown type");
        }
        return owners;
    }
    @Override
    public boolean isNestable() {
        return false;
    }
    @Override
    public AbstractPTAExpression newComplementary() {
        PTALocationGuard opposite = clone();
        switch(getType()) {
            case DIRECT:
                opposite.setType(Type.NEGATED);
                break;
                
            case NEGATED:
                opposite.setType(Type.DIRECT);
                break;
                
            case NEGATED_COMMON:
                throw new RuntimeException("no complementary expression");
                
            default:
                throw new RuntimeException("unknown type");
        }
        return opposite;
    }
    @Override
    public boolean isEquivalentCondition(
            AbstractPTAExpression other) {
        if(!isConditional() || !other.isConditional())
            throw new RuntimeException("not conditions");
        if(other instanceof PTALocationGuard) {
            PTALocationGuard lg = (PTALocationGuard)other;
            if(type != lg.type)
                return false;
            switch(type) {
                case DIRECT:
                case NEGATED_COMMON:
                    List<PTAState> source = getLocationsCommon();
                    List<PTAState> lgSource = lg.getLocationsCommon();
                    for(PTAState l : source)
                        if(!lgSource.contains(l))
                            return false;
                    for(PTAState l : lgSource)
                        if(!source.contains(l))
                            return false;
                    break;
                    
                case NEGATED:
                    throw new UnsupportedOperationException();
                    
                default:
                    throw new RuntimeException("unknown type");
            }
            return true;
        } else
            return false;
    }
    @Override
    public PTAConstant getConstant() {
        return null;
    }
    @Override
    public boolean isFloating() {
        return false;
    }
    @Override
    public boolean isConditional() {
        return true;
    }
    @Override
    public AbstractPTAExpression probability() {
        return null;
    }
    @Override
    public SortedSet<AbstractPTAVariable> getSources() {
        SortedSet<AbstractPTAVariable> out = super.getSources();
        // // all variables in arguments are sources in this context
        // out.addAll(sub.getVariables());
        return out;
    }
    @Override
    public List<PTARange> trueRanges() {
        List<PTARange> out = new LinkedList<>();
        return out;
    }
    @Override
    public PTAConstant evaluate(Map<AbstractPTAVariable,
            PTAConstant> knowns) {
        return null;
    }
    @Override
    public boolean equals(Object o) {
        return isEquivalentCondition((AbstractPTAExpression)o);
    }
    @Override
    public PTALocationGuard clone() {
        PTALocationGuard copy = (PTALocationGuard)super.clone();
        if(LOCATIONS_COMMON != null)
            copy.LOCATIONS_COMMON = new LinkedList<>(LOCATIONS_COMMON);
        if(LOCATIONS != null) {
            copy.LOCATIONS = new TreeMap<>();
            for(Map.Entry<String, List<PTATransition>> entry : LOCATIONS.entrySet())
                copy.LOCATIONS.put(entry.getKey(), new LinkedList<>(entry.getValue()));
        }
        copy.type = type;
        return copy;
    }
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(type.name() + "<<");
        switch(type) {
            case DIRECT:
            case NEGATED_COMMON:
                for(PTAState l : getLocationsCommon()) {
                    s.append(" ");
                    s.append((l.owner != null ? l.owner.name : "?") + ":" + l.index);
                }
                break;

            case NEGATED:
                SortedMap<String,List<PTAState>> locations = getLocations();
                for(String labelName : locations.keySet()) {
                    s.append(" [" + labelName + "]");
                    for(PTAState l : locations.get(labelName)) {
                        s.append(" ");
                        s.append((l.owner != null ? l.owner.name : "?") + ":" + l.index);
                    }
                }
                break;

            default:
                throw new RuntimeException("unknown type");
        }
        s.append(" >>");
        return super.toString() + s.toString();
    }

    


}
