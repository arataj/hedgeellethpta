/*
 * PTARange.java
 *
 * Created on May 12, 2009, 3:41:22 PM
 *
 * Copyright (c) 2009, 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.primitiverange.PrimitiveRangeDescription;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.TypeRangeMap;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * Range of an arithmetic or a boolean variable.<br>
 *
 * Extreme values can be exclusive or inclusive. For variable ranges they are always
 * inclusuive, though.
 *
 * In the case of boolean variables, <code>false</code> is 0 and
 * <code>true</code> is 1.<br>
 *
 * In the case of array variables, this range is defined for
 * the array's elements, just like <code>VariablePrimitiveRange</code> or
 * <code>CodeVariablePrimitiveRange</code>.<br>
 * 
 * @author Artur Rataj
 */
public class PTARange {
    /**
     * Arithmetic value of the boolean value <code>false</code>.
     */
    public static final int FALSE_VALUE = 0;
    /**
     * Arithmetic value of the boolean value <code>true</code>.
     */
    public static final int TRUE_VALUE = 1;

    /**
     * Default range values within the range for a TADD, not Java.
     */
    public Type.TypeRange range;
    /**
     * If the minimum value is exclusive. Never true for ranges of variables.
     */
    public boolean minExclusive = false;
    /**
     * If the maximum value is exclusive. Never true for ranges of variables.
     */
    public boolean maxExclusive = false;
    /**
     * A position, <i>to be used solely by the backend</i>. Outside
     * the backend it is always null.
     */
    public StreamPos backendPos;
    /**
     * Used solely by the backend. Outside it is always false.<br>
     * 
     * If this range is guaranteed not to be exceed. Known to be true for
     * example for Java's boolean values, some internal counters etc.<br>
     * 
     * This information is only needed for the backend, so that it knows
     * it does not need any range checking for the variable.<br>
     * 
     * The default is false for all constructors.
     */
    public boolean backendGuaranteed = false;
    /**
     * Used solely by the backend. Outside it is always false.<br>
     * 
     * If this range originated from a source--level one.<br>
     * 
     * The default is false for all constructors.
     */
    public boolean backendIsSourceLevel = false;

    /**
     * Creates a new instance of PTARange, based on primitive type. A
     * TADD default is used for that type.
     *
     * @param type type or array of types, that determines the range
     * @param reducedLongRange make long fit into 32 bits
     */
    public PTARange(Type type, boolean reducedLongRange) {
        range = getTADDDefault(type, reducedLongRange);
    }
    /**
     * Creates a new instance of PTARange, with specified minimum and
     * maximum integer values.
     *
     * @param pos                       position in the source stream
     * @param min                       minimum value
     * @param max                       maximum value
     */
    public PTARange(StreamPos pos, int min, int max) throws PTAException {
        backendPos = pos;
        range = new Type.TypeRange(min, max);
        checkValidity(null);
    }
    /**
     * Creates a new instance of PTARange, with specified minimum and
     * maximum integer values.
     *
     * @param pos                       position in the source stream
     * @param min                       minimum value
     * @param max                       maximum value
     */
    public PTARange(StreamPos pos, long min, long max) throws PTAException {
        backendPos = pos;
        if(min < Integer.MIN_VALUE || max > Integer.MAX_VALUE)
            throw new RuntimeException("does not fit into 32 bits");
        range = new Type.TypeRange(min, max);
        checkValidity(null);
    }
    /**
     * Creates a new instance of PTARange, with specified minimum and
     * maximum floating point values.
     *
     * @param pos                       position in the source stream
     * @param min                       minimum value
     * @param max                       maximum value
     */
    public PTARange(StreamPos pos, double min, double max) throws PTAException {
        backendPos = pos;
        range = new Type.TypeRange(min, max);
        checkValidity(null);
    }
    /**
     * Creates a new instance of PTARange, with specified minimum and
     * maximum integer values.
     *
     * @param pos                       position in the source stream
     * @param min                       minimum value
     * @param max                       maximum value
     */
    public PTARange(StreamPos pos, Type.TypeRange typeRange) throws PTAException {
        backendPos = pos;
        range = new Type.TypeRange(typeRange);
        checkValidity(null);
    }
//    /**
//     * NOT IMPLEMENTED
//     * 
//     * Creates a new instance of PTARange, covering all values, for
//     * which a given conditional expression is true.
//     * 
//     * @param condition                 condition that determines the range
//     */
//    public PTARange(AbstractPTAExpression condition) {
//        backendPos = null;
//        if(!condition.isConditional())
//            throw new RuntimeException("conditional expression expected");
//    }
    /**
     * Creates a new instance of PTARange. This is a copying constructor.
     *
     * @param range                     range to copy
     */
    public PTARange(PTARange range) {
        maxExclusive = range.maxExclusive;
        minExclusive = range.minExclusive;
        backendPos = range.backendPos;
        backendGuaranteed = range.backendGuaranteed;
        backendIsSourceLevel = range.backendIsSourceLevel;
        this.range = new Type.TypeRange(range.range);
    }
    /**
     * Checks if the range is valid. If not, throws <code>PTAException<code>.
     * 
     * @param name name of a ranged variable, null for none
     */
    public void checkValidity(String name) throws PTAException {
        if(range.getMinFloatingPoint() > range.getMaxFloatingPoint()) {
            String s;
            if(name == null)
                s = "";
            else
                s = "variable " + name + " has ";
            throw new PTAException(backendPos,
                    s + "invalid range " + range.toString());
        }
    }
    /**
     * Returns a new range on basis of a type's default range for TADDS, <i>this
     * is unlike default ranges in Java</i>. If the type is an array, then the
     * array's element type is used to get get the default range.
     *
     * @param type type whose range is used to set this range
     * @param reducedLongRange make long fit into 32 bits
     */
    protected static Type.TypeRange getTADDDefault(Type type,
            boolean reducedLongRange) {
        if(type.isArray(null))
            type = type.getElementType(null, 0);
        Type.TypeRange range = new Type.TypeRange(type.getPrimitive(),
                true);
        int power;
        if(reducedLongRange)
            power = 31;
        else
            power = 48;
        switch(type.getPrimitive()) {
            case BOOLEAN:
                range.setMinI(FALSE_VALUE);
                range.setMaxI(TRUE_VALUE);
                break;

            case BYTE:
                range.setMinI(-(1 << 7));
                range.setMaxI(-range.getMinI() - 1);
                break;

            case CHAR:
                range.setMinI(0);
                range.setMaxI((1 << 16) - 1);
                break;

            case SHORT:
                range.setMinI(-(1 << 15));
                range.setMaxI(-range.getMinI() - 1);
                break;

            case INT:
                range.setMinI(-(1 << 15));
                range.setMaxI(-range.getMinI() - 1);
                break;

            case LONG:
                // must fit into <code>double</code> even if +/-1 is added
                // by the backend for error detection, as is stored in variables
                // of that type
                range.setMinI(-(1L << power) + 1);
                range.setMaxI(-range.getMinI() - 1);
                break;

            case FLOAT:
            case DOUBLE:
                range.setMinF(-(1L << power));
                range.setMaxF(-range.getMinF() - 1);
                break;

            default:
                throw new RuntimeException("invalid type");
        }
        return range;
    }
    /**
     * Returns a map of ranges on basis of type's default range for TADDS,
     * <i>this is unlike default ranges in Java</i>. All TADD primitive
     * types are included.
     * 
     * @param reducedLongRange make long fit into 32 bits
     */
    public static TypeRangeMap getTADDDefaults(boolean reducedLongRange) {
        Set<Type.PrimitiveOrVoid> types = new HashSet<>();
        types.add(Type.PrimitiveOrVoid.BOOLEAN);
        types.add(Type.PrimitiveOrVoid.BYTE);
        types.add(Type.PrimitiveOrVoid.CHAR);
        types.add(Type.PrimitiveOrVoid.SHORT);
        types.add(Type.PrimitiveOrVoid.INT);
        types.add(Type.PrimitiveOrVoid.LONG);
        types.add(Type.PrimitiveOrVoid.FLOAT);
        types.add(Type.PrimitiveOrVoid.DOUBLE);
        TypeRangeMap map = new TypeRangeMap();
        for(Type.PrimitiveOrVoid p : types)
            map.add(p, getTADDDefault(new Type(p), reducedLongRange));
        return map;
    }
    /**
     * Returns the lower limit.
     * 
     * @return minimum value, exclusive or inclusive depending on
     * <code>minExclusive</code>
     * 
     */
    public double getMin() {
        if(range.isFloating())
            return range.getMinF();
        else
            return range.getMinI();
    }
    /**
     * Returns the upper limit.
     * 
     * @return maximum value, exclusive or inclusive depending on
     * <code>maxExclusive</code>
     * 
     */
    public double getMax() {
        if(range.isFloating())
            return range.getMaxF();
        else
            return range.getMaxI();
    }
    /**
     * If this range overlaps another, including a possible exclusion of
     * minimum or maximum value.
     * 
     * @param other the other range
     * @return a range with a common non--zero set or null
     */
    public PTARange overlaps(PTARange other) {
        double min;
        boolean minX;
        double max;
        boolean maxX;
        if(getMin() > other.getMin()) {
            min = getMin();
            minX = minExclusive;
        } else if(getMin() < other.getMin()) {
            min = other.getMin();
            minX = other.minExclusive;
        } else {
            min = getMin();
            minX = minExclusive || other.minExclusive;
        }
        if(getMax() < other.getMax()) {
            max = getMax();
            maxX = maxExclusive;
        } else if(getMax() > other.getMax()) {
            max = other.getMax();
            maxX = other.maxExclusive;
        } else {
            max = getMax();
            maxX = maxExclusive || other.maxExclusive;
        }
        if(min > max)
            return null;
        else if(min == max && (minX || maxX))
            return null;
        else {
            try {
                PTARange common;
                if(!range.isFloating() && !other.range.isFloating() &&
                        max <= Integer.MAX_VALUE)
                    common = new PTARange(null, (int)min, (int)max);
                else
                    common = new PTARange(null, min, max);
                common.minExclusive = minX;
                common.maxExclusive = maxX;
                return common;
            } catch(PTAException e) {
                throw new RuntimeException("unexpected: " + e.toString());
            }
        }
    }
    /**
     * If this range fits into another, including a possible exclusion of
     * minimum or maximum value.
     * 
     * @param other another range
     * @return if this range is equal to or a subset of the other range
     */
    public boolean fitsTo(PTARange other) {
        return (getMin() > other.getMin() ||
                (getMin() == other.getMin() && !(!minExclusive && other.minExclusive))) &&
            (getMax() < other.getMax() ||
                (getMax() == other.getMax() && !(!maxExclusive && other.maxExclusive)));
    }
    @Override
    public String toString() {
        String s;
        if(minExclusive)
            s = "((";
        else
            s = PrimitiveRangeDescription.VARIABLE_LEFT_STRING;
        if(range.isFloating())
            s += getMin() + ", " + getMax();
        else
            s += (long)getMin() + ", " + (long)getMax();
        if(maxExclusive)
            s += "))";
        else
            s += PrimitiveRangeDescription.VARIABLE_RIGHT_STRING;
        if(backendGuaranteed)
            s = "[" + s + "]";
        return s;
    }
}
