/*
 * InterpreterException.java
 *
 * Created on May 3, 2008
 *
 * Copyright (c) 2008, 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;

/**
 * A PTA generation exception.
 * 
 * @author Artur Rataj
 */
public class PTAException extends CompilerException {
    /**
     * Creates a new instance of PTAException, with an empty set of
     * reports.
     */
    public PTAException() {
        super();
    }
    /**
     * Creates a new instance of PTAException.
     * 
     * @param description               description of this exception
     */
    public PTAException(StreamPos pos, String description) {
        super(pos, Code.ILLEGAL, description);
    }
    /**
     * Creates a new instance of PTAException. This constructor
     * copies only position and message, ignores possible other parameters
     * in subclasses.
     * 
     * @param e translator exception
     */
    public PTAException(TranslatorException e) {
        this(e instanceof CompilerException ? null : e.getStreamPos(),
                e instanceof CompilerException ? null : e.getRawMessage());
        if(e instanceof CompilerException) {
            CompilerException c = (CompilerException)e;
            addAllReports(c);
        }
    }
    /**
     * Creates a new instance of PTAException.
     *
     * @param e compiler exception
     */
    public PTAException(CompilerException e) {
        super(null, null, e);
    }
}
