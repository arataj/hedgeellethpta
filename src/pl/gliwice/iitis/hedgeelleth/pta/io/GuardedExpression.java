/*
 * GuardedExpression.java
 *
 * Created on Nov 15, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.io;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.text.StringAnalyze;

/**
 * Textual representation of a parsed value or expression, together
 * with textual representation of error guards, that enable or disable
 * accessing to that value or execution of that expression.
 * 
 * @author Artur Rataj
 */
public class GuardedExpression {
    /**
     * Textual representation of a value or an expression.
     */
    private StringBuilder expr;
    /**
     * True if this expression's return type is floating, false if that type
     * is integer or undefined.
     */
    private boolean floating;
    /**
     * List of textual representations of enabling error guards.
     */
    private List<String> enable;
    /**
     * List of textual representations of disabling error guards.
     */
    private List<String> disable;

    /**
     * Creates a new guarded expression, without error guards.
     * 
     * @param expr textual representation of the expression
     * @param floating true if the expression's return type is floating,
     * false if that type is integer or undefined
     */
    public GuardedExpression(String expr, boolean floating) {
        this.expr = new StringBuilder(expr);
        this.floating = floating;
        enable = new LinkedList<>();
        disable = new LinkedList<>();
    }
    /**
     * A copying constructor. The copy is deep.
     * 
     * @param source guarded expression
     */
    public GuardedExpression(GuardedExpression source) {
        expr = new StringBuilder(source.expr);
        floating = source.floating;
        enable = new LinkedList<>(source.enable);
        disable = new LinkedList<>(source.disable);
    }
    /**
     * Replace the expression string. Guards are untouched by this
     * method.
     * 
     * @param newExpr textual representation of an expression
     */
    public void replaceExpr(String newExpr) {
        expr = new StringBuilder(newExpr);
    }
    /**
     * Prepends a string before this object's expression.
     * 
     * @param s string to prefix
     */
    public void prepend(String s) {
        expr.insert(0, s);
    }
    /**
     * Prepends an expression before this object, including
     * guards.
     * 
     * @param e expression to prefix
     */
    public void prepend(GuardedExpression e) {
        expr.insert(0, e.expr);
        enable.addAll(0, e.enable);
        disable.addAll(0, e.disable);
    }
    /**
     * Appends a string to this object's expression.
     * 
     * @param s string to append
     */
    public void append(String s) {
        expr.append(s);
    }
    /**
     * Appends an expression to this object, including
     * guards.
     * 
     * @param e expression to append
     */
    public void append(GuardedExpression e) {
        expr.append(e.expr);
        enable.addAll(e.enable);
        disable.addAll(e.disable);
    }
    /**
     * If there is no expression or guard within this object.
     * 
     * @return if is empty
     */
    public boolean isEmpty() {
        return expr.length() == 0 && enable.isEmpty() && disable.isEmpty();
    }
    /**
     * Adds an enabling error guard.
     * 
     * @param guard guard to add
     */
    public void addEnable(String guard) {
        enable.add(guard);
    }
    /**
     * Adds a disabling error guard.
     * 
     * @param guard guard to add
     */
    public void addDisable(String guard) {
        disable.add(guard);
    }
    /**
     * Returns the expression contained, without guards.
     * 
     * @param guardsAllowed if this object is allowed to contain
     * any guards when this method is called; if not allowed but
     * contains, then a runtime exception is thrown
     */
    public String getExpr(boolean guardsAllowed) {
        if(!guardsAllowed &&
                (!enable.isEmpty() || !disable.isEmpty()))
            throw new RuntimeException("unexpected guards");
        return expr.toString();
    }
    /**
     * Returns the expression contained, without guards.
     * Guards are allowed. This is a convenience method.
     */
    public String getExpr() {
        return getExpr(true);
    }
    /**
     * Sets, if this expression's return type is floating.
     * 
     * @param floating if floating
     */
    public void setFloating(boolean floating) {
        this.floating = floating;
    }
    /**
     * Returns, if this expression's return type is floating.
     * 
     * @param floating if floating
     */
    public boolean isFloating() {
        return floating;
    }
    /**
     * Returns a textual representation of a set of error guards, in
     * the format accepted by Prism.
     * 
     * @param guards set to guards
     * @return textual representation
     * @param operator boolean operator that combines the guards
     */
    private String get(List<String> guards, String operator) {
        StringBuilder s = new StringBuilder();
        boolean first = true;
        for(String g : guards) {
            if(!first)
                s.append(operator);
            boolean parenthesize = !StringAnalyze.inParentheses(g, false);
            if(parenthesize)
                s.append("(");
            s.append(g);
            if(parenthesize)
                s.append(")");
            first = false;
        }
        return s.toString();
    }
    /**
     * Returns a product of enabling error guards, in the format
     * accepted by Prism.
     * 
     * @return a textual representation of a set of guards
     */
    public String getEnable() {
        return get(enable, " & ");
    }
    /**
     * Returns a sum of disabling error guards, in the format
     * accepted by Prism.
     * 
     * @return a textual representation of a set of guards
     */
    public String getDisable() {
        return get(disable, " | ");
    }
    @Override
    public String toString() {
        if(false)
            throw new RuntimeException("not here please");
        return getExpr() + " ( " + getEnable() + " ) !( " + getDisable() + " )";
    }
}
