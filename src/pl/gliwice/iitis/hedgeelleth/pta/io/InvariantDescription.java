/*
 * InvariantDescription.java
 *
 * Created on Jul 25, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.io;

import pl.gliwice.iitis.hedgeelleth.pta.PTAClockCondition;

/**
 * A description of a single clock invariant.
 * 
 * @author Artur Rataj
 */
public class InvariantDescription {
    /**
     * Index of the affected state.
     */
    public final int stateIndex;
    /**
     * Condition on the clock.
     */
    public final PTAClockCondition clockCondition;
    
    /**
     * Creates a new invariant description.
     * 
     * @param stateIndex copied to the field <code>stateIndex</code>
     * @param maxClock copied to the field <code>maxClock</code>
     * @param maxClockInclusive copied to the field <code>maxClockInclusive</code>
     */
    public InvariantDescription(int stateIndex, PTAClockCondition clockCondition) {
        this.stateIndex = stateIndex;
        this.clockCondition = clockCondition;
    }
}
