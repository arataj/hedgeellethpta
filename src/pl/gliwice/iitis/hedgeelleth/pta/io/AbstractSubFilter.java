/*
 * AbstractSubFilter.java
 *
 * Created on Jul 13, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.io;

/**
 * A backend--specific filter of subs in <code>FileFormatIO.buildNameMap()</code>.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractSubFilter {
    /**
     * If a given sub should be accepted.
     * 
     * @param sub a sub to test
     * @return original or shortened sub if accepted by the backend, null if
     * denied by the backend
     */
    abstract public String accept(String sub);
}
