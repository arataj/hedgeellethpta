/*
 * Simplify.java
 *
 * Created on Oct 27, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.io;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.matheclipse.core.eval.ExprEvaluator;
import org.matheclipse.core.interfaces.*;
import org.matheclipse.core.visit.IVisitorBoolean;

/**
 *
 * @author Artur Rataj
 */
public class Simplify implements IVisitorBoolean {
    ExprEvaluator evaluator;
    
    public Simplify(ExprEvaluator evaluator) {
        this.evaluator = evaluator;
    }
    @Override
    public boolean visit(IInteger ii) {
        return true;
    }
    @Override
    public boolean visit(IFraction i) {
        return true;
    }
    @Override
    public boolean visit(INum inum) {
        double value = inum.doubleValue();
        return value == (int)value || Double.toString(value).length() < 4;
    }
    @Override
    public boolean visit(ISymbol is) {
        return true;
    }
    @Override
    public boolean visit(IComplex ic) {
        throw new RuntimeException("unexpected");
    }
    @Override
    public boolean visit(IComplexNum icn) {
        throw new RuntimeException("unexpected");
    }
    @Override
    public boolean visit(IPattern ip) {
        throw new RuntimeException("unexpected");
    }
    @Override
    public boolean visit(IPatternSequence ips) {
        throw new RuntimeException("unexpected");
    }
    @Override
    public boolean visit(IStringX isx) {
        throw new RuntimeException("unexpected");
    }
    @Override
    public boolean visit(IAST iast) {
        Iterator<IExpr> it = iast.iterator();
        while(it.hasNext()) {
            IExpr expr = it.next();
            if(!expr.accept(this))
                return false;
        }
        return true;
    }
    public String process(String in) {
        // change all real numbers with no fractional part
        // to integer equivalents, as symja works better with
        // them
        StringBuilder out = new StringBuilder();
        Pattern p = Pattern.compile("[\\d]+.0");
        Matcher m = p.matcher(in);
        int pos = 0;
        while(m.find()) {
            int s = m.start();
            int e = m.end();
            String g = m.group();
            out.append(in.substring(pos, s));
            out.append(g.substring(0, g.length() - 2));
            pos = e;
        }
        out.append(in.substring(pos));
        IExpr expr = evaluator.evaluate(out.toString());
        if(expr.accept(this))
            return expr.toString()
                    .replaceAll("\\+", " - ")
                    .replaceAll("\\-", " - ")
                    .trim();
        else
            return in;
    }
    public static void main(String[] args) {
        ExprEvaluator util = new ExprEvaluator(false, 100);
        //Ppiqe * Ppiq * Pp - Ppiqe * Pq * Pp + Pq
        //Ppiqe * Ppiq * Pp - Ppiqe * Pp - Ppiqe * Ppiq + Ppiq + Ppiqe
        //IExpr result = util.evaluate("Solve({x1==a,x2==-a + a*b + 1,x3==c - a*c + a*b}, x3)");
        IExpr result = util.evaluate("Eliminate({x1==a,x2==-a + a*b + 1,x3==c - a*c + a*b}, {a,b,c,d})");
        //util.evaluate("Solve(1 - (j*1)/10 == 0, j)");
        //IExpr result = util.evaluate("Refine({x1==a,x2==-a + a*b + 1,x3==c - a*c + a*b}, x1>0)");
//        util.evaluate("d-d*x1-c/(d*x1)+(c*d)/(d*x1)+(-c*d^2*x1)/(d*x1)+(c*d^2*x1^2)/(d*x1)+x3/(d*x1)+(-d*x3)/(d*x1)+(d*x1*x3)/(d*x1) - " +
//                "( -(c-c*d-d^2*x1+c*d^2*x1+d^2*x1^2-c*d^2*x1^2-x3+d*x3-d*x1*x3)/(d*x1) )");
        //IExpr result = util.evaluate("Eliminate({x1==a,x2==a*b*d - a*d - b*d + b +d,x3==a*b*d - a*c*d + c}, {a,b,c,d})");
        String r = result.toString();
        System.out.println(r);
        for(int i = 1; i <= 3; ++i) {
            result = util.evaluate("Solve(" + r + ", x" + i + ")");
            System.out.println(result.toString());
        }
        String s = result.toString();
        s = s.substring(s.indexOf("->") + 2, s.lastIndexOf("}}"));
        util.evaluate("{x1=0.5,x2=0.5,x3=0.0,d=1,c=0}");
        util.evaluate("x3==" + s);
        util.evaluate("x3=" + s + "");
    }
    
    
}
