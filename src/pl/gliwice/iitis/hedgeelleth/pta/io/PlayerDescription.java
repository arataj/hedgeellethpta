/*
 * PlayerDescription.java
 *
 * Created on Nov 14, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.io;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.pta.*;

/**
 * Description of players, used by the parser.
 * 
 * @author Artur Rataj
 */
public class PlayerDescription {
    /**
     * Position in the parsed stream, that represents the player section.
     */
    public StreamPos pos;
    /**
     * Names of player PTAs.
     */
    public SortedMap<String, SortedSet<String>> playerPTAs =
        new TreeMap<>();
    /**
     * Names of player labels.
     */
    public SortedMap<String, SortedSet<String>> playerLabels =
        new TreeMap<>();
    
    /**
     * Translates this description into players in <code>PTASystem</code>.
     * 
     * @param compilation a compilation to set player structures
     */
    public void setPlayers(PTASystem compilation) throws ParseException {
        for(String player : playerPTAs.keySet()) {
            List<PTA> ptas = compilation.playerPTAs.get(player);
            Set<String> participants = playerPTAs.get(player);
            for(PTA pta : compilation.ptas.values())
                if(participants.contains(pta.name)) {
                    ptas.add(pta);
                    participants.remove(pta.name);
                }
            if(!participants.isEmpty())
                throw new ParseException(pos,
                        ParseException.Code.PARSE,
                        "participant not found: " + participants.iterator().next());
        }
        for(String player : playerLabels.keySet()) {
            List<PTALabel> labels = compilation.playerLabels.get(player);
            Set<String> participants = playerLabels.get(player);
            for(PTALabel label : compilation.labels.values())
                if(participants.contains(label.name)) {
                    labels.add(label);
                    participants.remove(label.name);
                }
            if(!participants.isEmpty())
                throw new ParseException(pos,
                        ParseException.Code.PARSE,
                        "participant not found: [" + participants.iterator().next() + "]");
        }
    }
}
