/*
 * VariableDescription.java
 *
 * Created on Feb 22, 2010, 2:22:51 PM
 *
 * Copyright (c) 2010 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.io;

import pl.gliwice.iitis.hedgeelleth.pta.PTARange;

/**
 * Description of a variable, for PTA IO.
 *
 * @author Artur Rataj
 */
public class VariableDescription {
    /**
     * Index of this variable.
     */
    public int index;
    /**
     * Final range of the variable, as declared in the generated file.<br>
     *
     * This might differ from the original range of the variable, due
     * to range checking properties.<br>
     * 
     * Clocks have no range, and this field should be null for them.
     */
    PTARange finalRange;

    /**
     * Creates a new instance of VariableDescription.
     *
     * @param index                     index of this variable
     * @param finalRange                final range, see the field
     *                                  <code>finalRange</code> for
     *                                  details
     */
    public VariableDescription(int index, PTARange finalRange) {
        this.index = index;
        this.finalRange = finalRange;
    }
}
