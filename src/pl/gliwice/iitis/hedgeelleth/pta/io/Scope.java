/*
 * Scope.java
 *
 * Created on Jul 25, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.io;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAVariable;

/**
 * Defines labels and variables, accessible by <code>PTAExpression</code>.
 * 
 * @author Artur Rataj
 */
public class Scope {
    /**
     * Variables, keyed with their names.<br>
     */
    public SortedMap<String, AbstractPTAVariable> variables;
    /**
     * Labels, keyed with their names.<br>
     */
    public SortedMap<String, PTALabel> labels;
    /**
     * Number of states in the current TADD, -1 for unknown.
     */
    public int statesNum = -1;
    
    /**
     * Creates an empty scope. The collections in this object are created but
     * empty. Number of states is unknown.
     */
    public Scope() {
        variables = new TreeMap<>();
        labels = new TreeMap<>();
        statesNum = -1;
    }
    /**
     * Creates a scope, that makes shallow copies out of variables in another
     * scope. Additions of variables to this scope does not modify the parent scope.
     * The reference to labels and the number of states are copied from the parent.
     * 
     * @param parent parent scope
     */
    public Scope(Scope parent) {
        variables = new TreeMap<>(parent.variables);
        labels = parent.labels;
        statesNum = parent.statesNum;
    }
    /**
     * Creates a scope containing all globals and all labels of a given system of PTAs.
     * The collection references in this object are  copies of equivalents in
     * <code>system</code>, so changes to them are reflected in <code>system</code>.
     * The number of states is set to unknown, as the system is not
     * required to have its states completed.
     * 
     * @param system system to read variables and labels from
     */
    public Scope(PTASystem system) {
        variables = system.variables;
        labels = system.labels;
        statesNum = -1;
    }
    /**
     * Adds a variable to this scope.
     * 
     * @param v variable to add
     */
    public void addVariable(AbstractPTAVariable v) {
        variables.put(v.name, v);
    }
    /**
     * Adds a label to this scope.
     * 
     * @param l label to add
     */
    public void addLabel(PTALabel l) {
        labels.put(l.name, l);
    }
}
