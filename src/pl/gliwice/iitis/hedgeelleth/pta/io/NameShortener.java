/*
 * NameShortener.java
 *
 * Created on Jul 24, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.io;

import java.util.*;
import java.io.*;

/**
 * Translates long names into shorter and cleaner, but still
 * retaining uniqueness.
 * 
 * @author Artur Rataj
 */
public class NameShortener {
    /**
     * Name map, built by <code>buildNameMap()</code> if
     * this shortener is not transparent and <code>verbose</code>
     * is true. Otherwise null. Used by <code>getName()</code>.
     */
    protected SortedMap<String, String> nameMap;
    /**
     * A dictionary of the replacement of some predefined subs
     * in <code>buildNameMap()</code>;
     */
    protected static final SortedMap<String, String> subDictionary = new TreeMap<>();
    {
        subDictionary.put("::", "_in_");
        subDictionary.put("#", "_hash_");
        subDictionary.put("/", "_slash_");
        subDictionary.put(".", "_dot_");
        subDictionary.put("<", "_lt_");
        subDictionary.put(">", "_gt_");
        // there is no attempt to remove this string, but only
        // to replace it, to preserve readability
        subDictionary.put("__", "_");
        subDictionary.put("lockVariables::", "");
        subDictionary.put("statusVariables::", "");
        subDictionary.put("__#retval", "_retval");
    }

    /**
     * True for verbose names, false for enumerated names.
     */
    protected final boolean verbose;
    /**
     * Replace instances with their position in a sorted list.
     */
    protected final boolean compactInstances;
    
    /**
     * Creates a new name shortener, that is transparent, i.e. returns the
     * original names if <code>verbose</code> is true.
     * 
     * @param verbose true for verbose names, false for enumerated names
     * @param compactInstances replace instances with their position in a sorted list
     */
    public NameShortener(boolean verbose, boolean compactInstances) {
        this.verbose = verbose;
        this.compactInstances = compactInstances;
    }
    /**
     * Creates a new name shortener.
     * 
     * @param verbose true for verbose names, false for enumerated names
     * @param compactInstances replace instances with their position in a sorted list
     * @param names names of all variables and optionally all labels in the generated model;
     * meaningless if <code>verbose</code> is false
     * @param subFilter filter of unwanted cut--outs, null for none
     */
    public NameShortener(boolean verbose, boolean compactInstances,
            Collection<String> names, AbstractSubFilter subFilter) throws IOException {
        this(verbose, compactInstances);
        buildNameMap(names, subFilter);
    }
    /**
     * Greedily removes all instances of <code>sub</code> from
     * <code>full</code>.
     * 
     * @param full original string
     * @param sub sub to find
     * @return possibly shortened string
     */
    protected String shorten(String full, String sub) {
        StringBuilder out = new StringBuilder();
        int pos = 0;
        while(pos < full.length()) {
            int begin = full.indexOf(sub, pos);
            if(begin == -1) {
                out.append(full.substring(pos));
                break;
            } else
                out.append(full.substring(pos, begin));
            pos = begin + sub.length();
        }
        return out.toString();
    }
    /**
     * Initializes name map, replacing instances with their positions in a sorted
     * list.
     * 
     * @param all all names
     */
    protected void initNameMap(Collection<String> all) {
        nameMap = new TreeMap<>();
        if(compactInstances) {
            final String BEG_STRING = "#";
            final String END_STRING = "::";
            SortedSet<String> instances = new TreeSet<>();
            SortedMap<String, SortedMap<Integer, Integer>> instanceMap = new TreeMap<>();
            for(String v : all) {
                int beg = v.indexOf(BEG_STRING);
                int end = v.indexOf(END_STRING);
                if(beg != -1 && end != -1 && beg + BEG_STRING.length() < end) {
                    String i = v.substring(beg + BEG_STRING.length(), end);
                    try {
                        int num = Integer.parseInt(i);
                        instances.add(v.substring(0, end));
                    } catch(NumberFormatException e) {
                        /* empty */
                    }
                }
            }
            for(String s : instances) {
                int pos = s.indexOf(BEG_STRING);
                String prefix = s.substring(0, pos);
                String i = s.substring(pos + BEG_STRING.length());
                int num = Integer.parseInt(i);
                SortedMap<Integer, Integer> sequence = instanceMap.get(prefix);
                if(sequence == null) {
                    sequence = new TreeMap<>();
                    instanceMap.put(prefix, sequence);
                }
                // index since 0
                sequence.put(num, sequence.size());
            }
            for(String v : all) {
                int beg = v.indexOf(BEG_STRING);
                int end = v.indexOf(END_STRING);
                String out = v;
                if(beg != -1 && end != -1 && beg + 1 < end) {
                    String i = v.substring(beg + 1, end);
                    try {
                        String prefix = v.substring(0, beg);
                        int num = instanceMap.get(prefix).get(Integer.parseInt(i));
                        out = v.substring(0, beg + BEG_STRING.length()) +
                                num + v.substring(end);
                    } catch(NumberFormatException e) {
                        /* empty */
                    }
                }
                nameMap.put(v, out);
            }
        } else
            for(String v : all)
                nameMap.put(v, v);
    }
    /**
     * Builds a name map of variables and optionally labels.
     * Must be called once after all variables and optionally
     * labels are known, but before calls to <code>getName()<code>.
     * 
     * @param all names of all variables and optionally all labels in the generated model;
     * meaningless if <code>options.verboseTADDVariables</code> is false
     * @param subFilter filter of unwanted cut--outs, null for none
     */
    final protected void buildNameMap(Collection<String> all, AbstractSubFilter subFilter)
            throws IOException {
        if(verbose) {
            final String SEPARATOR = "::";
            initNameMap(all);
            // find all subs
            SortedSet<String> subs = new TreeSet<>();
            for(String s : nameMap.values()) {
                int pos = 0;
                while(pos < s.length()) {
                    int begin = pos;
                    int end = s.indexOf(SEPARATOR, pos);
                    if(end == -1)
                        end = s.length();
                    String sub = s.substring(begin, end);
                    // do not try subs, which are equal to the whole string,
                    // as it might cut the names too much
                    if(end < s.length())
                        subs.add(sub);
                    // leave the likely package prefix only
                    int dot = sub.lastIndexOf('.');
                    if(dot != -1) {
                        sub = sub.substring(0, dot + 1);
                        if(subFilter == null || (sub = subFilter.accept(sub)) != null)
                            subs.add(sub);
                    }
                    pos = end + SEPARATOR.length();
                }
            }
            // add the subs from dictionary in the hope,
            // that they can be rather removed than
            // translated
            for(String s : subDictionary.keySet())
                // see <code>subDictionary</code> for the explanation
                // of the following statement
                if(!s.equals("__"))
                    subs.add(s);
            // accept only the subs that retain uniqueness
            //
            // check since the longest, so that the shorter cuts won't make
            // the longer cuts impossible
            SortedMap<Integer, SortedSet<String>> sortedSubs =
                    new TreeMap<>();
            for(String sub : subs) {
                int key = -sub.length();
                SortedSet<String> set = sortedSubs.get(key);
                if(set == null) {
                    set = new TreeSet<>();
                    sortedSubs.put(key, set);
                }
                set.add(sub);
            }
            for(int key : sortedSubs.keySet())
                for(String sub : sortedSubs.get(key)) {
                    boolean badSub = false;
                    SortedMap<String, String> shortenedNames =
                            new TreeMap<>();
                    Set<String> unique = new HashSet<>();
                    for(String v : nameMap.keySet()) {
                        String shortened = shorten(nameMap.get(v), sub);
                        int i = 0;
                        while(i < shortened.length() && !Character.isJavaIdentifierStart(shortened.charAt(i)))
                            ++i;
                        if(i == shortened.length()) {
                            badSub = true;
                            break;
                        }
                        if(shortened.isEmpty() || !Character.isLetter(shortened.charAt(i)) ||
                                unique.contains(shortened)) {
                            badSub = true;
                            break;
                        }
                        if(shortened.startsWith("s")) {
                            try {
                                Integer.parseInt(shortened.substring(1));
                                // a conflict or too similar to modules' step variables
                                badSub = true;
                                break;
                            } catch(NumberFormatException e) {
                                /* empty */
                            }
                        }
                        unique.add(shortened);
                        shortenedNames.put(v, shortened);
                    }
                    if(!badSub)
                        nameMap = shortenedNames;
                }
            // translate the dictionary subs, if any are left
            for(String sub : subDictionary.keySet()) {
                String translation = subDictionary.get(sub);
                SortedMap<String, String> translatedNames =
                        new TreeMap<>();
                Map<String, String> unique = new HashMap<>();
                for(String v : nameMap.keySet()) {
                    String translated = nameMap.get(v).replace(sub,
                            translation);
                    if(unique.keySet().contains(translated))
                        throw new IOException("clash with " +
                                unique.get(translated) + " because it contains " +
                                "\"" + translation + "\" and verbose names are enabled");
                    unique.put(translated, v);
                    translatedNames.put(v, translated);
                }
                nameMap = translatedNames;
            }
        }
    }
    /**
     * Generates a name of a variable or a label, depending on
     * <code>options.verboseTADDVariables</code>.
     * 
     * @param v original, full name of a variable or a label
     * @param variable true for a variable, false for a label; used only if
     * <code>verbose</code> is false
     * @param index index of the variable or label, used only if
     * <code>verbose</code> is false
     */
    public String getName(String v, boolean variable, int index) {
        String name;
        if(verbose) {
            if(nameMap != null)
                name = nameMap.get(v);
            else
                name = v;
        } else
            name = (variable? "z" : "e") + index;
        return name;
    }
}
