/*
 * PTAIO.java
 *
 * Created on Jul 24, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.io;

import java.io.*;
import java.util.*;
import java.text.NumberFormat;
import org.matheclipse.core.eval.ExprEvaluator;

import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.ModelType;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.StringAnalyze;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.ToAlphanumeric;
import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.io.parser.PTAParser;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;

/**
 * I/O routines for PTAs.
 * 
 * @author Artur Rataj
 */
public class PTAIO {
    /**
     * Mode of generating the output file. <code>PRISM</code> is used
     * only by the backend.
     */
    public enum Flavour {
        PRISM,
        HEDGEELLETH,
    };
    /**
     * A unit of indent.
     */
    final public static String INDENT_STRING = "    ";
    /**
     * Prefix of the name of the step local variable.
     */
    final public static String STEP_NAME = "s";
    /**
     * Prefix of the name of the clock local variable.
     */
    final public static String CLOCK_NAME = "x";
    /**
     * Prefix of verbose modules, used only by the backend.
     */
    final public static String MODULE_PREFIX = "m_";
    /**
     * Prefix of verbose labels, used only by the backend.
     */
    final public static String BACKEND_CHANNEL_PREFIX = "c_";
    /**
     * A string for a formula, that is true if at least a single urgent synchronisation
     * is enabled.
     */
    final public static String FORMULA_ENABLED_URGENT_NAME = "enabled_urgent";

    /**
     * A context for parsing statements.
     */
    public static class Context {
        /**
         * Flavour of the generated file.
         */
        final public Flavour flavour;
        /**
         * Global variables.
         */
        final public Map<AbstractPTAVariable, VariableDescription> variables;
        /**
         * Name shortener for variables.
         */
        final public NameShortener shortener;
        /**
         * Indices of each PTA.
         */
        public Map<PTA, Integer> ptaIndices;
        /**
         * Indices of each state, over all PTAs.
         */
        public Map<PTAState, Integer> stateIndices;
        
        /**
         * Index of the currently generated PTA.
         */
        public int currPTAIndex;
        /**
         * Map of arrays to their elements. Used only by the backend
         * to convert arrays to scalars. Null outside the backend.
         */
        public Map<PTAArrayVariable, PTAScalarVariable[]> backendArrays;
        /**
         * Shortener exclusively for array names. Used only by the backend
         * to get unique textual prefixes for each array. Null outside the backend.
         */
        public NameShortener backendArrayShortener;
        /**
         * Non--state formulas, key is unique name, value is contents.
         * Used only by the backend by the PRISM flavour. Null outside the
         * backend.
         */
        public SortedMap<String, String> backendFormulas;
        /**
         * If arrays are emulated.
         */
        public boolean emulateArrays;
        /**
         * If index range checking guards should be generated.
         */
        public boolean indexRangeCheck;
        /**
         * If at least a single transition has <code>backendWaitForSync</code> true,
         * then this string contains the condition to append to the guard of these
         * transitions. Otherwise, the string is null.
         */
        public String backendWaitForSyncString;
        /**
         * Simplifies equations using symbolic algebra.
         */
        Simplify simplify;
        
        /**
         * Creates a new context. It must be completed by at least
         * <code>currPTAIndex</code>.
         * 
         * @param flavour copied to <code>flavour</code>
         * @param variables copied to <code>variables</code>
         * @param shortener copied to <code>shortener</code>
         * @param emulateArrays copied to <code>emulateArrays</code>
         * @param indexRangeCheck copied to <code>indexRangeCheck</code>
         * @param backendWaitForSyncString not null if at least a single
         * transition has <code>backendWaitForSync</code> true;
         * contains what to append to the guards of such transitions
         */
        public Context(Flavour flavour, Map<AbstractPTAVariable, VariableDescription>
                variables, NameShortener shortener, boolean emulateArrays,
                boolean indexRangeCheck, String backendWaitForSyncString) {
            this.flavour = flavour;
            this.variables = variables;
            this.shortener = shortener;
            this.emulateArrays = emulateArrays;
            this.indexRangeCheck = indexRangeCheck;
            this.backendWaitForSyncString = backendWaitForSyncString;
            currPTAIndex = -1;
            backendArrays = null;
            backendFormulas = null;
            ptaIndices = new HashMap<>();
            stateIndices = new HashMap<>();
            simplify = new Simplify(new ExprEvaluator(false, 100));
        }
    }
    /**
     * Finds a model type.
     * 
     * @param file name of the input file
     * @return a model type
     */
    public static ModelType readType(String fileName) throws ParseException {
        try(FileInputStream in = new FileInputStream(fileName)) {
            PTAParser parser = new PTAParser(in);
            return parser.Type();
        } catch(IOException e) {
            throw new ParseException(null,
                    ParseException.Code.IO,
                    "could not read input file: " + e.getMessage());
        }
    }
    /**
     * Reads a number of PTAs.
     * 
     * @param file name of the input file
     * @return a PTA compilation
     */
    public static PTASystem read(String fileName) throws ParseException {
        try(FileInputStream in = new FileInputStream(fileName)) {
            PTAParser parser = new PTAParser(in);
            PTASystem compilation = parser.CompilationUnit(fileName);
            return compilation;
        } catch(IOException e) {
            throw new ParseException(null,
                    ParseException.Code.IO,
                    "could not read input file: " + e.getMessage());
        }
    }
    /**
     * Completes branch ids of conditional or non--deterministic
     * branches. Used by the parser.<br>
     * 
     * If a branch to be marked is marked already, a runtime exception
     * is thrown.
     * 
     * @param pta pta to modify
     * @param type type of branches to mark
     */
    public static void completeBranchIds(PTA pta,
            BranchId.Type type) {
        for(PTAState state : pta.states.values())
            state.qualifiesForBranchId(type, true, false);
    }
    /**
     * Searches all nested expressions for indexing, and then
     * checks each of the expressions for the need for checking
     * for the index being out of range.<br>
     * 
     * This method is used to check, if a system of PTAs will
     * require special transitions, as defined in
     * <code>writeTransitions()</code>, and thus,
     * will need a reserved value for the error variable.
     * 
     * @return 
     */
    public static boolean isOutOfRangeIndexInNested(Collection<PTA> ptas) {
        List<PTANestedExpression> all = new LinkedList<>();
        for(PTA pta : ptas)
            for(PTATransition x : pta.transitions) {
                if(x.guard instanceof PTANestedExpression)
                    all.add((PTANestedExpression)x.guard);
                if(x.probability instanceof PTANestedExpression)
                    all.add((PTANestedExpression)x.probability);
                for(AbstractPTAExpression u : x.update)
                    if(u instanceof PTANestedExpression)
                        all.add((PTANestedExpression)u);
            }
        for(PTANestedExpression ne : all)
            for(PTANestedExpression ie : ne.getIndexings()) {
//if(ie.toString().equals("#system.#Array#106##c4<<0, 3>> (length = 5 contents = [ 0 ])[(#default.Sort#107::#default.Sort.run/::k<<0, 6>>(0)+1)]"))
//    ie = ie;
//if(ie.toString().equals("#system.#Array#106##c4<<0, 3>> (length = 5 contents = [ 0 ])[#default.Sort#107::#default.Sort.run/::k<<0, 6>>(0)]"))
//    ie = ie;
                PTAArrayVariable a = (PTAArrayVariable)ie.leftValue;
                PTARange i = ie.getBackendKnownRange(false);
                if(i == null || i.getMin() < 0 || i.getMax() >= a.length)
                    return true;
            }
        return false;
    }
    /**
     * Returns a Prism--style string representing an operator.
     * 
     * @param operator operator
     * @return textual form
     */
    public static String getOperatorName(CodeOpBinaryExpression.Op operator) {
        String s = BinaryExpression.getOperatorName(
            CodeOpBinaryExpression.operatorCodeToTree(operator));
        if(s.equals("=="))
            s = "=";
        return s;
    }
    /**
     * Returns a shortened name of a variable.
     * 
     * @param context context
     * @param v variable
     * @return a shortened name
     */
    public static String getName(Context context, AbstractPTAVariable v) {
        return context.shortener.getName(v.name, true,
                context.variables.get(v).index);
    }
    /**
     * Returns a scalar variable, that replaced a given element variable.
     * 
     * @param context context
     * @param element element variable
     * @return replacement scalar variable
     */
    public static PTAScalarVariable getScalarFromElement(Context context,
            PTAElementVariable element) throws IOException {
        PTAScalarVariable[] array = context.backendArrays.get(element.array);
        PTAConstant index = (PTAConstant)element.index;
        if(index.floating)
            throw new IOException("floating point index");
        return array[(int)index.value];
    }
    /**
     * Returns a string representing the value of a given element.
     * 
     * @param a array
     * @param index index
     * @return textual representation
     */
    protected static String getElementValue(PTAArrayVariable a,
            int index) {
        if(a.floating)
            return "" + a.initValue[index];
        else
            return "" + (long)a.initValue[index];
    }
    /**
     * Creates, if not exists yet, a formula for reading a given array.
     * 
     * @param context context
     * @param element element accessed using a variable index
     * @return name of the formula and possibly "//"--separated
     * accept/reject formula guards
     */
    protected static GuardedExpression addReadElementFormula(Context context,
            PTAElementVariable element) throws IOException {
        PTAArrayVariable a = element.array;
        AbstractPTAVariable i = (AbstractPTAVariable)element.index;
        String indexName = getName(context, i);
        GuardedExpression formulaName = new GuardedExpression(
                context.backendArrayShortener.getName(
                        a.name, true, -1) +
                "_index_" + indexName,
                a.floating);
        if(!context.backendFormulas.containsKey(formulaName.getExpr())) {
            String s = "";
            boolean prevEmpty = true;
            long minAccess = (long)i.range.getMin();
            long maxAccess = (long)i.range.getMax();
            long minIndex = Math.max(0, minAccess);
            long maxIndex = Math.min(a.length - 1, maxAccess);
            if(minIndex < Integer.MIN_VALUE || maxIndex > Integer.MAX_VALUE)
                throw new RuntimeException("index does not fit into integer");
            boolean minTest = minAccess < 0;
            boolean maxTest = maxAccess >= a.length;
            for(int index = (int)maxIndex; index >= (int)minIndex; --index) {
                PTAElementVariable e = new PTAElementVariable(
                        a, new PTAConstant(index, false), a.range, a.floating);
                String name;
                PTAScalarVariable v = getScalarFromElement(context, e);
                if(v != null)
                    name = getName(context, v);
                else 
                    // the element is never written to, so it does not have its own
                    // variable
                    name = getElementValue(a, index);
                if(s.isEmpty())
                    s = name;
                else {
                    s = indexName + "=" + index + " ? " + name + " :\n" +
                        INDENT_STRING + (prevEmpty ? s : "(" + s + ")");
                    prevEmpty = false;
                }
            }
            context.backendFormulas.put(formulaName.getExpr(), s);
            if(context.indexRangeCheck) {
                if(minTest) {
                    formulaName.addEnable(indexName + ">=0");
                    formulaName.addDisable(indexName + "<0");
                }
                if(maxTest) {
                    formulaName.addEnable(indexName + "<=" + maxIndex);
                    formulaName.addDisable(indexName + ">" + maxIndex);
                }
            }
        }
        return formulaName;
    }
    /**
     * Creates, if not exists yet, a formula for reading a given array,
     * used instead of a nested expression.
     * 
     * @param context context
     * @param array indexed array
     * @param indexExpr textual representation of the index
     * @param indexreadRange if the range of values possible to
     * the index is known, then represents that range, otherwise
     * null
     * @return name of the formula and possibly "//"--separated
     * accept/reject formula guards
     */
    protected static GuardedExpression addReadElementFormula(Context context,
            PTAArrayVariable array, GuardedExpression indexExpr,
            PTARange indexReadRange) throws IOException {
        GuardedExpression formulaName = new GuardedExpression(
                context.backendArrayShortener.getName(
                        array.name, true, -1) +
                "_index_nested_",
                array.floating);
        GuardedExpression aIndexExpr = new GuardedExpression(indexExpr);
        aIndexExpr.replaceExpr(ToAlphanumeric.get(aIndexExpr.getExpr(),
                // so that a string that begins with a single underscore
                // is a keyword
                false));
        formulaName.append(aIndexExpr);
        int maxIndex = array.length - 1;
        if(!context.backendFormulas.containsKey(formulaName.getExpr())) {
            String s = "";
            boolean prevEmpty = true;
            for(int index = maxIndex; index >= 0; --index) {
                PTAElementVariable e = new PTAElementVariable(
                        array, new PTAConstant(index, false), array.range, array.floating);
                String name;
                PTAScalarVariable v = getScalarFromElement(context, e);
                if(v != null)
                    name = getName(context, v);
                else 
                    // the element is never written to, so it does not have its own
                    // variable
                    name = getElementValue(array, index);
                if(s.isEmpty())
                    s = name;
                else {
                    s = indexExpr.getExpr() + "=" + index + " ? " + name + " :\n" +
                        INDENT_STRING + (prevEmpty ? s : "(" + s + ")");
                    prevEmpty = false;
                }
            }
            context.backendFormulas.put(formulaName.getExpr(), s);
        }
        if(indexReadRange != null && indexReadRange.range.isFloating())
            throw new RuntimeException("index expected to have an integer range");
        long min;
        long max;
        if(indexReadRange != null) {
            min = (int)indexReadRange.getMin();
            max = (int)indexReadRange.getMax();
        } else {
            min = Long.MIN_VALUE;
            max = Long.MAX_VALUE;
        }
        if(context.indexRangeCheck) {
            if(min < 0) {
                formulaName.addEnable(indexExpr.getExpr() + ">=0");
                formulaName.addDisable(indexExpr.getExpr() + "<0");
            }
            if(max > maxIndex) {
                formulaName.addEnable(indexExpr.getExpr() + "<=" + maxIndex);
                formulaName.addDisable(indexExpr.getExpr() + ">" + maxIndex);
            }
        }
        return formulaName;
    }
    
    /**
     * Produces a text representation of a value.
     *
     * @param context                   context
     * @param e                         value
     */
    public static GuardedExpression parse(Context context, AbstractPTAValue e) throws IOException {
        if(e instanceof PTAConstant) {
            PTAConstant c = (PTAConstant)e;
            if(c.floating) {
                NumberFormat nf = NumberFormat.getInstance(Locale.ROOT);
                nf.setMaximumFractionDigits(16);
                String s = nf.format(c.value);
                if(s.indexOf(".") == -1)
                    s += ".0";
                return new GuardedExpression("" + s, true);
            } else
                return new GuardedExpression("" + (int)c.value, false);
        } else if(e instanceof PTAClock) {
            // clock is a local, and a single PTA can have at most a single clock
            return new GuardedExpression(CLOCK_NAME + context.currPTAIndex, false);
        } else if(e instanceof PTADensity) {
            if(context.flavour != Flavour.HEDGEELLETH)
                throw new IOException("Prism format does not support densities");
            PTADensity d = (PTADensity)e;
            GuardedExpression s = new GuardedExpression(
                    d.typeToString() + "(", d.floating);
            // parsing of variables
            boolean first = true;
            try {
                for(AbstractPTAValue p : d.params) {
                    if(!first)
                        s.append(", ");
                    s.append(parse(context, p));
                    first = false;
                }
            } catch(IOException f) {
                throw new IOException("density of type " + d.typeToString() +
                        " not supported: " + f.getMessage());
            }
            s.append(")");
            return s;
        } else if(e instanceof PTAElementVariable) {
            PTAElementVariable c = (PTAElementVariable)e;
            if(context.backendArrays == null) {
                // no manual range checking -- if the model model checker
                // supports arrays, it likely does range checking by itself
                GuardedExpression ge = parse(context, c.index);
                ge.prepend(getName(context, c.array) + "[");
                ge.append("]");
                ge.setFloating(c.array.floating);
                return ge;
            } else {
                if(c.index instanceof PTAConstant) {
                    PTAScalarVariable element = getScalarFromElement(
                            context, c);
                    if(element == null) {
                        // the element is never written to, so it does not have its own
                        // variable
                        return new GuardedExpression(getElementValue(
                                c.array, (int)((PTAConstant)c.index).value),
                                c.array.floating);
                    } else
                        return new GuardedExpression(getName(
                                context, element), element.floating);
                } else {
                    switch(context.flavour) {
                        case PRISM:
                            // prism has formulas for such cases
                            return addReadElementFormula(context, c);
                            
                        case HEDGEELLETH:
                            throw new IOException("indexing with a variable not supported");
                            
                        default:
                            throw new RuntimeException("unknown flavour");
                    }
                }
            }
        } else if(e instanceof PTABufferedConstant) {
            throw new RuntimeException("buffered constant not replaced: " +
                    e.toString());
        } else {
            AbstractPTAVariable v = (AbstractPTAVariable)e;
            if(v instanceof PTAArrayVariable && context.emulateArrays)
                throw new IOException("array parameter can not be emulated: " +
                        v.name);
            return new GuardedExpression(getName(context, v), v.floating);
        }
    }
    /**
     * Wraps what returns <code>PTASystem.getMax</code> into a string.
     * 
     * @param range
     * @return 
     */
    public static String getMax(Type.TypeRange range) {
        double v = PTASystem.getMax(range);
        if(v == (int)v)
            return "" + (int)v;
        else
            return "" + v;
    }
    /**
     * Wraps what returns <code>PTASystem.getMin</code> into a string.
     * 
     * @param range
     * @return 
     */
    public static String getMin(Type.TypeRange range) {
        double v = PTASystem.getMin(range);
        if(v == (int)v)
            return "" + (int)v;
        else
            return "" + v;
    }
    /**
     * Returns, if the some value is known to be always not
     * lesser that 0.
     * 
     * @param value value, whose range is estimated
     * @return if known to be never negative
     */
    private static boolean alwaysNonnegative(AbstractPTAValue value) {
        return (value instanceof PTAConstant && ((PTAConstant)value).value >= 0.0) ||
                (value instanceof AbstractPTAVariable &&
                    ((AbstractPTAVariable)value).range.getMin() >= 0.0);
    }
//    /**
//     * Parses a location guard.
//     * 
//     * @param context context
//     * @param e expression to parse
//     * @param ePTAIndex index of the PTA, to which belongs <code>e</code>
//     * @return parse result
//     */
//    protected static GuardedExpression parseLocationGuard(Context context,
//            AbstractPTAExpression e, int ePTAIndex) {
//        GuardedExpression s = new GuardedExpression("", false);
//        PTALocationGuard lg = (PTALocationGuard)e;
//        Map<PTA, SortedSet<PTAState>> owners = lg.byOwner();
//            boolean firstPTA = true;
//            boolean needsParentheses = false;
//            boolean lastParenthesized = false;
//            for(int ptaNum = 0; ptaNum < context.ptaIndices.size(); ++ptaNum) {
//                PTA pta = null;
//                for(PTA p : context.ptaIndices.keySet())
//                    if(context.ptaIndices.get(p) == ptaNum) {
//                        pta = p;
//                        break;
//                    }
//                if(ptaNum != ePTAIndex && owners.keySet().contains(pta)) {
//                    String t = "";
//                    Set<PTAState> locations = owners.get(pta);
//                    boolean firstLocation = true;
//                    for(PTAState l : locations) {
//                        if(!firstLocation)
//                            t += " | ";
//                        t += PTAIO.STEP_NAME + context.ptaIndices.get(pta) +
//                                "=" + context.stateIndices.get(l);
//                        firstLocation = false;
//                    }
//                    if(lastParenthesized =
//                            (locations.size() > 1 && lg.getType() != PTALocationGuard.Type.NEGATED_COMMON)) {
//                        t = "(" + t + ")";
//                    }
//                    if(!firstPTA) {
//                        if(lg.getType() == PTALocationGuard.Type.NEGATED_COMMON)
//                            s.append(" | ");
//                        else
//                            s.append(" & ");
//                        needsParentheses = true;
//                    }
//                    s.append(t);
//                    firstPTA = false;
//                }
//            }
//            if(needsParentheses || !lastParenthesized) {
//            s.prepend("(");
//                s.append(")");
//            }
//        if(lg.getType() != PTALocationGuard.Type.DIRECT)
//            s.prepend("!");
//        return s;
//    }
    /**
     * Parses a location guard.
     * 
     * @param context context
     * @param e expression to parse
     * @param ePTAIndex index of the PTA, to which belongs <code>e</code>
     * @return parse result
     */
    protected static GuardedExpression parseLocationGuard(Context context,
            AbstractPTAExpression e, int ePTAIndex) {
        GuardedExpression r = new GuardedExpression("", false);
        PTALocationGuard lg = (PTALocationGuard)e;
        boolean firstAlternative = true;
        for(String labelName : lg.getLabelAlternatives()) {
            if(!firstAlternative)
                r.append(" | ");
            StringBuilder s = new StringBuilder();
            Map<PTA, SortedSet<PTAState>> owners = lg.byOwner(labelName);
            boolean firstPTA = true;
            boolean needsParentheses = false;
            boolean lastParenthesized = false;
            for(int ptaNum = 0; ptaNum < context.ptaIndices.size(); ++ptaNum) {
                PTA pta = null;
                for(PTA p : context.ptaIndices.keySet())
                    if(context.ptaIndices.get(p) == ptaNum) {
                        pta = p;
                        break;
                    }
                if(ptaNum != ePTAIndex && owners.keySet().contains(pta)) {
                    String t = "";
                    Set<PTAState> locations = owners.get(pta);
                    boolean firstLocation = true;
                    for(PTAState l : locations) {
                        Integer index = context.stateIndices.get(l);
                        // check if the location was not optimised out
                        if(index != null) {
                            if(!firstLocation)
                                t += " | ";
                            t += PTAIO.STEP_NAME + context.ptaIndices.get(pta) +
                                    "=" + index;
                            firstLocation = false;
                        }
                    }
                    if(lastParenthesized =
                            (locations.size() > 1 && lg.getType() != PTALocationGuard.Type.NEGATED_COMMON)) {
                        t = "(" + t + ")";
                    }
                    if(!firstPTA) {
                        if(lg.getType() == PTALocationGuard.Type.NEGATED_COMMON)
                            s.append(" | ");
                        else
                            s.append(" & ");
                        needsParentheses = true;
                    }
                    s.append(t);
                    firstPTA = false;
                }
            }
            if(needsParentheses || !lastParenthesized) {
                s.insert(0, "(");
                s.append(")");
            }
            r.append(s.toString());
            firstAlternative = false;
        }
        if(lg.getLabelAlternatives().size() > 1) {
            r.prepend("(");
            r.append(")");
        }
        if(lg.getType() != PTALocationGuard.Type.DIRECT)
            r.prepend("!");
        return r;
    }
    /**
     * Extracts a description of an element variable or of an constant. Only range
     * is guaranted to be valid in the returned object.
     * 
     * @param c context
     * @param variable variable
     * @return description containing a range
     */
    private static VariableDescription extractDescription(Context c, AbstractPTAVariable variable) {
        PTAScalarVariable scalar;
        if(variable instanceof  PTAElementVariable) {
            try {
                PTAElementVariable e = (PTAElementVariable)variable;
                if(e.index instanceof PTAConstant)
                    // perhaps the range will be narrowed to a single element
                    scalar = getScalarFromElement(c, (PTAElementVariable)variable);
                else {
                    // get the common range
                    return new VariableDescription(-1, e.array.range);
                }
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage());
            }
        } else
            scalar = (PTAScalarVariable)variable;
        VariableDescription description;
        if(scalar != null)
            description = c.variables.get(scalar);
        else {
            // no variable, must be an element never written to, get its constant value
            PTAElementVariable e = (PTAElementVariable)variable;
            try {
                if(e.floating) {
                    double f = Double.parseDouble(getElementValue(e.array, (int)((PTAConstant)e.index).value));
                    try {
                        description = new VariableDescription(-1, new PTARange(null, f, f));
                    } catch(PTAException g) {
                        throw new RuntimeException("unexpected: " + g.getMessage());
                    }
                } else {
                    int i = Integer.parseInt(getElementValue(e.array, (int)((PTAConstant)e.index).value));
                    try {
                        description = new VariableDescription(-1, new PTARange(null, i, i));
                    } catch(PTAException f) {
                        throw new RuntimeException("unexpected: " + f.getMessage());
                    }
                }
            } catch(NumberFormatException n) {
                throw new RuntimeException("unexpected: " + n.getMessage());
            }
        }
        return description;
    }
    /**
     * If an assignment needs min clipping.
     * 
     * @param vd description of the target
     * @param sub right-hand side
     * @return if clipping of minimum value needed
     */
    private static boolean clipMinNeeded(Context c, VariableDescription vd, AbstractPTAValue sub) {
        if(sub instanceof PTAConstant)
            return false;
        else if(sub instanceof AbstractPTAVariable) {
            VariableDescription rightVd = extractDescription(c, (AbstractPTAVariable)sub);
            if(vd != null && rightVd.finalRange.getMin() >= vd.finalRange.getMin())
                return false;
        } else if(sub instanceof PTADensity) {
            PTADensity d = (PTADensity)sub;
            if(vd != null && d.range != null && d.range.getMin() >= vd.finalRange.getMin())
                return false;
        }
        return true;
    }
    /**
     * If an assignment needs max clipping.
     * 
     * @param vd description of the target
     * @param sub right-hand side
     * @return if clipping of maximum value needed
     */
    private static boolean clipMaxNeeded(Context c, VariableDescription vd, AbstractPTAValue sub) {
        if(sub instanceof PTAConstant)
            return false;
        else if(sub instanceof AbstractPTAVariable) {
            VariableDescription rightVd = extractDescription(c, (AbstractPTAVariable)sub);
            if(vd != null && rightVd.finalRange.getMax() <= vd.finalRange.getMax())
                return false;
        } else if(sub instanceof PTADensity) {
            PTADensity d = (PTADensity)sub;
            if(vd != null && d.range != null && d.range.getMax() <= vd.finalRange.getMax())
                return false;
        }
        return true;
    }
    /**
     * Produces a text representation of an expression.
     *
     * If clock number is -1, only assignments to non--clock variables are produced.
     * Otherwise, only assignments to clock variables are produced.
     *
     *
     * @param context context
     * @param e expression
     * @param booleanResult if true, then this expression is expected to have a boolean
     * result; if false, then this expression can have any result
     * @return text description of the expression
     */
    public static GuardedExpression parse(Context context, AbstractPTAExpression e,
            boolean booleanResult) throws IOException {
        GuardedExpression s = null; ///new GuardedExpression("", false);
        String booleanTarget = "";
        VariableDescription vd;
        if(e.target != null) {
            AbstractPTAVariable v = e.target;
            if(v == PTANestedExpression.SUPER_NESTED)
                vd = null;
            else {
                if(context.emulateArrays && v instanceof PTAElementVariable)
                    v = getScalarFromElement(context, (PTAElementVariable)v);
                vd = context.variables.get(v);
            }
        } else
            vd = null;
        boolean includeMin = true;
        boolean includeMax = true;
        // expressions of the form f( ... ) can be explicitly marked as ones
        // not requring to be parenthesized
        boolean knownNoNeedToParenthesize = false;
        if(e instanceof PTABinaryExpression ||
                (e instanceof PTANestedExpression &&
                    ((PTANestedExpression)e).isTopBinary())) {
            PTABinaryExpression be;
            PTANestedExpression ne;
            BinaryExpression.Op operator;
            if(e instanceof PTABinaryExpression) {
                be = (PTABinaryExpression)e;
                ne = null;
                operator = CodeOpBinaryExpression.operatorCodeToTree(
                        be.operator);
                s = new GuardedExpression(getOperatorName(be.operator),
                        e.isFloating());
            } else {
                ne = (PTANestedExpression)e;
                be = null;
                if(ne.topOperator != AbstractCodeOp.Op.NESTED_INDEX) {
                    operator = CodeOpBinaryExpression.operatorCodeToTree(
                            ne.topOperator);
                    s = new GuardedExpression(getOperatorName(ne.topOperator),
                            ne.isFloating());
                } else
                    operator = null;
            }
            boolean divideShorterForm = false;
            if(operator != null && operator.isRelational()) {
                // no need for checking ranges if the expression is
                // boolean
                includeMin = false;
                includeMax = false;
                if(e.target != null && context.flavour == Flavour.PRISM)
                    booleanTarget = " ? 1 : 0";
            } else {
                if(be != null) {
                    switch(operator) {
                        case PLUS:
                            if(be.target == be.left &&
                                    be.right instanceof PTAConstant) {
                                PTAConstant t = (PTAConstant)be.right;
                                if(t.value <= 0)
                                    includeMax = false;
                                if(t.value >= 0)
                                    includeMin = false;
                            }
                            break;

                        case MINUS:
                            if(be.target == be.left &&
                                    be.right instanceof PTAConstant) {
                                PTAConstant t = (PTAConstant)be.right;
                                if(t.value <= 0)
                                    includeMin = false;
                                if(t.value >= 0)
                                    includeMax = false;
                            }
                            break;

                        case MULTIPLY:
                            break;

                        case DIVIDE:
                            if(be.target == be.left &&
                                    be.right instanceof PTAConstant) {
                                PTAConstant t = (PTAConstant)be.right;
                                if(t.value >= 1) {
                                    includeMin = false;
                                    includeMax = false;
                                }
                            }
                            divideShorterForm =
                                    alwaysNonnegative(be.left) || alwaysNonnegative(be.right);
                            break;

                        case MODULUS:
                            if(be.target == be.left) {
                                includeMin = false;
                                includeMax = false;
                            } else if(be.left instanceof AbstractPTAVariable &&
                                    be.right instanceof PTAConstant) {
                                AbstractPTAVariable l = (AbstractPTAVariable)be.left;
                                PTAConstant r = (PTAConstant)be.right;
                                if(l.range.getMin() >= 0 &&
                                        be.target.range.getMin() <= 0)
                                    includeMin = false;
                                if(be.target.range.getMax() <= r.value - 1)
                                    includeMax = false;
                            }
                            break;

                        case MODULUS_POS:
                            if(be.target == be.left) {
                                includeMin = false;
                                includeMax = false;
                            } else if(be.left instanceof AbstractPTAVariable &&
                                    be.right instanceof PTAConstant) {
                                AbstractPTAVariable l = (AbstractPTAVariable)be.left;
                                PTAConstant r = (PTAConstant)be.right;
                                if(be.target.range.getMin() <= 0)
                                    includeMin = false;
                                if(be.target.range.getMax() <= r.value - 1)
                                    includeMax = false;
                            }
                            break;

                        default:
                            throw new IOException("operator " + s + " not allowed");

                    }
                }
            }
            GuardedExpression left;
            if(be != null)
                left = parse(context, be.left);
            else
                // ensure this is not a nested index expression
                if(operator != null) {
                    if(ne.leftExpr != null)
                        left = parse(context, ne.leftExpr, false);
                    else
                        left = parse(context, ne.leftValue);
                } else
                    left = null;
            GuardedExpression right;
            if(be != null)
                right = parse(context, be.right);
            else if(ne.rightExpr != null)
                right = parse(context, ne.rightExpr, false);
            else
                right = parse(context, ne.rightValue);
            if(ne != null && ne.topOperator == AbstractCodeOp.Op.NESTED_INDEX) {
                if(ne.leftExpr != null)
                    throw new RuntimeException("unexpected expression");
                PTARange indexRange = ne.getBackendKnownRange(false);
                s = addReadElementFormula(context,
                        (PTAArrayVariable)ne.leftValue, right,
                        indexRange);
            } else if(operator == BinaryExpression.Op.DIVIDE &&
                    context.flavour == Flavour.PRISM &&
                    !s.isFloating()) {
                String z = "(" + left.getExpr() + " / " + right.getExpr() + ")";
                if(ne != null && ne.backendReadRange != null &&
                        ne.backendReadRange.getMin() >= 0)
                    divideShorterForm = true;
                if(divideShorterForm)
                    s = new GuardedExpression("floor(", true);
                else
                    s = new GuardedExpression(z + " > 0 ? floor" + z + " : ceil(", true);
                // explicitly add operands once, so that their guards are included
                s.append(left);
                s.append(" / ");
                s.append(right);
                s.append(")");
                // knownNoNeedToParenthesize = true;
            } else if(operator == BinaryExpression.Op.MODULUS &&
                    context.flavour == Flavour.PRISM) {
                s = new GuardedExpression("mod(", s.isFloating());
                s.append(left);
                s.append(", ");
                s.append(right);
                s.append(")");
                // knownNoNeedToParenthesize = true;
            } else {
                s.prepend(" ");
                s.prepend(left);
                s.append(" ");
                s.append(right);
            }
        } else if(e instanceof PTAUnaryExpression ||
                (e instanceof PTANestedExpression &&
                    ((PTANestedExpression)e).isTopUnary())) {
            PTAUnaryExpression ue;
            PTANestedExpression ne;
            AbstractCodeOp.Op operator;
            if(e instanceof PTAUnaryExpression) {
                ue = (PTAUnaryExpression)e;
                ne = null;
                operator = ue.operator;
            } else {
                ne = (PTANestedExpression)e;
                ue = null;
                operator = ne.topOperator;
            }
            if(e.target == null) {
                // a condition
                if(ue != null)
                    s = parse(context, ue.sub);
                else if(ne.leftExpr != null)
                    s = parse(context, ne.leftExpr, false);
                else
                    s = parse(context, ne.leftValue);
                if(operator == AbstractCodeOp.Op.UNARY_CONDITIONAL_NEGATION)
                    booleanTarget = " = 0";
                else if(operator != AbstractCodeOp.Op.NONE) {
                    s = new GuardedExpression(UnaryExpression.getOperatorName(
                            CodeOpUnaryExpression.operatorCodeToTree(
                                operator), null)[0],
                            false);
                    throw new IOException("operator " + s + " not allowed in condition");
                } else if(booleanResult)
                    booleanTarget = " = 1";
            } else {
                if(operator == AbstractCodeOp.Op.UNARY_CONDITIONAL_NEGATION) {
                    s = new GuardedExpression("1 - ", false);
                    if(ue != null)
                        s.append(parse(context, ue.sub));
                    else if(ne.leftExpr != null)
                        s.append(parse(context, ne.leftExpr, false));
                    else
                        s.append(parse(context, ne.leftValue));
                    includeMin = false;
                    includeMax = false;
                } else if(operator == AbstractCodeOp.Op.UNARY_NEGATION) {
                    s = new GuardedExpression("0 - ", false);
                    if(ue != null)
                        s.append(parse(context, ue.sub));
                    else if(ne.leftExpr != null)
                        s.append(parse(context, ne.leftExpr, false));
                    else
                        s.append(parse(context, ne.leftValue));
                } else if(operator == AbstractCodeOp.Op.UNARY_ABS) {
                    s = new GuardedExpression("max(", false);
                    if(ue != null)
                        s.append(parse(context, ue.sub));
                    else if(ne.leftExpr != null)
                        s.append(parse(context, ne.leftExpr, false));
                    else
                        s.append(parse(context, ne.leftValue));
                    s.append(",-(");
                    if(ue != null)
                        s.append(parse(context, ue.sub));
                    else if(ne.leftExpr != null)
                        s.append(parse(context, ne.leftExpr, false));
                    else
                        s.append(parse(context, ne.leftValue));
                    s.append("))");
                } else if(operator == AbstractCodeOp.Op.NONE) {
                    if(ue != null) {
                        if(!clipMaxNeeded(context, vd, ue.sub))
                            includeMax = false;
                        if(!clipMinNeeded(context, vd, ue.sub))
                            includeMin = false;
                        s = parse(context, ue.sub);
                    } else if(ne.leftExpr != null)
                        s = parse(context, ne.leftExpr, false);
                    else {
                        if(!clipMaxNeeded(context, vd, ne.leftValue))
                            includeMax = false;
                        if(!clipMinNeeded(context, vd, ne.leftValue))
                            includeMin = false;
                        s = parse(context, ne.leftValue);
                    }
                } else {
                    s = new GuardedExpression(UnaryExpression.getOperatorName(
                            CodeOpUnaryExpression.operatorCodeToTree(
                                operator), null)[0],
                            false);
                    if(s.isEmpty())
                        s = new GuardedExpression("postfix " + UnaryExpression.getOperatorName(
                                CodeOpUnaryExpression.operatorCodeToTree(
                                    operator), null)[1],
                                false);
                    else
                        s = new GuardedExpression("prefix " + s,
                                false);
                    throw new IOException("operator " + s.getExpr() + " not allowed");
                }
            }
        } else if(e instanceof PTANestedExpression &&
                ((PTANestedExpression)e).topOperator == AbstractCodeOp.Op.BRANCH) {
            PTANestedExpression ne = (PTANestedExpression)e;
            if(ne.leftValue != null)
                s = parse(context, ne.leftValue);
            else
                s = parse(context, ne.leftExpr, true);
            PTANestedExpression diff = ne.rightExpr.clone();
            diff.target = null;
            diff.minMaxRange = null;
            s.prepend("(");
            s.append(" ? (");
            s.append(parse(context, diff, false));
            s.append(") : ");
            s.append(parse(context, ne.target));
            s.append(")");
        } else if(e instanceof PTALocationGuard)
            s = parseLocationGuard(context, e, context.currPTAIndex);
        else
            throw new RuntimeException("unknown expression");
        if(e.target != null && e.target != PTANestedExpression.SUPER_NESTED) {
//if(e.target.name.contains("defiance"))
//    e = e;
            if(vd != null) {
/*if(getMax(vd.finalRange.range).length() > 5)
    vd = vd;*/
                if(includeMax && !Double.isNaN(
                        PTASystem.getMax(vd.finalRange.range))) {
                    s.prepend("min(" + getMax(vd.finalRange.range) + ", ");
                    s.append(")");
                }
                if(includeMin && !Double.isNaN(
                        PTASystem.getMax(vd.finalRange.range))) {
                    s.prepend("max(" + getMin(vd.finalRange.range) + ", ");
                    s.append(")");
                }
            }
            s.prepend("' = ");
            s.prepend(parse(context, e.target));
        }
        if(!booleanTarget.isEmpty()) {
            s.append(booleanTarget);
            knownNoNeedToParenthesize = false;
        }
/*if(s.getExpr().equals("Household119runMicrogrid_getNumJobs_numJobs"))
            System.out.println("P1 " + s.getExpr());*/
        if(e.target == PTANestedExpression.SUPER_NESTED &&
                !knownNoNeedToParenthesize) {
            String t = StringAnalyze.parenthesize(s.getExpr());
            s.replaceExpr(t);
        }
        return s;
    }
    /**
     * Generates a string of the "options" section. Returns an empty string
     * if that section is not needed.
     * 
     * @param system a PTA system
     * @return a string
     */
    public static String getOptions(PTASystem system) throws IOException {
        String s = "";
        boolean sectionNeeded = false;
        if(system.implicitScheduler != null) {
            sectionNeeded = true;
            switch(system.implicitScheduler) {
                case HIDDEN:
                    s += INDENT_STRING + "implicit_scheduler hidden;\n";
                    break;
                    
                default:
                    throw new IOException("only a hidden implicit scheduler is supported, " +
                            "found " + system.implicitScheduler.toString());
            }
        }
        if(sectionNeeded)
            s = "options\n\n" + s + "\nendoptions\n\n";
        return s;
    }
    /**
     * Generates a string for declaration of a global variable.
     * 
     * @param v variable
     * @param index index of the variable
     * @param min minimum bound
     * @param max maximum bound
     * @param shortener name shortener
     * @param allowSingleElementRange if false, ranges with the same
     * <code>min</code> and <code>max</code> are modified by adding 1
     * to <code>max</code>
     * @return a serialized form
     */
    public static String getGlobalVariableDeclaration(AbstractPTAVariable v,
            int index, double min, double max, NameShortener shortener,
            boolean allowSingleElementRange) {
        PTAScalarVariable scalar = v instanceof PTAScalarVariable ?
                (PTAScalarVariable)v : null;
        PTAArrayVariable array = v instanceof PTAArrayVariable ?
                (PTAArrayVariable)v : null;
        StringBuilder s = new StringBuilder();
        s.append("global " + (v.floating ? "float" + (array != null ? "" : " "): ""));
        if(array != null)
            s.append("[" + array.length + "] ");
        double correctedMax;
        if(min != max || allowSingleElementRange)
            correctedMax = max;
        else
            correctedMax = max + 1;
        s.append(shortener.getName(v.name, true, index) + " : " +
            (v.floating ?
                "[" + min + ".." + correctedMax + "]" : "[" + (long)min + ".." + (long)correctedMax + "]") +
            " init ");
        if(array != null) {
            for(int i = 0; i < array.length; ++i) {
                if(i != 0)
                        s.append(", ");
                double d = array.initValue[i];
                s.append(v.floating ? "" + d : "" + (long)d);
            }
        } else
            s.append(v.floating ? "" + scalar.initValue : "" + (int)scalar.initValue);
        s.append(";");
        return s.toString();
    }
    /**
     * Generates a textual representation of players.
     * 
     * @param playerPTAs player PTAs, keyed with names
     * @param playerLabels player labels, keyed with names
     * @param playerList list of player names in the order of declaration
     * @param sortedPTAs determines the order of displaying of participant
     * PTAs within a single player
     * @param sortedLabels determines the order of displaying of participant
     * labels within a single player
     * @param moduleNames names of modules, keyed with PTAs
     * @param labelShortener shortener for names of labels; if not
     * transparent, names of labels (but the free ones) are prefixed with
     * <code>BACKEND_CHANNEL_PREFIX</code>
     * @return a string, empty for no players
     */
    public static String getPlayers(SortedMap<String, List<PTA>> playerPTAs,
            SortedMap<String, List<PTALabel>> playerLabels, List<String> playerList,
            Collection<PTA> sortedPTAs, Collection<PTALabel> sortedLabels,
            Map<PTA, String> moduleNames, NameShortener labelShortener) {
        StringBuilder s = new StringBuilder();
        for(String name : playerList) {
            s.append("\nplayer " + name + "\n");
            List<PTA> ptaParticipants = playerPTAs.get(name);
            boolean first = true;
            for(PTA pta : sortedPTAs)
                if(ptaParticipants.contains(pta)) {
                    if(first)
                        s.append(INDENT_STRING);
                    else
                        s.append(", ");
                    s.append(moduleNames.get(pta));
                    first = false;
                }
            List<PTALabel> labelParticipants = playerLabels.get(name);
            int labelCount = 0;
            for(PTALabel label : sortedLabels)
                if(labelParticipants.contains(label)) {
                    if(first)
                        s.append(INDENT_STRING);
                    else
                        s.append(", ");
                    s.append("[");
                    if(label.type == PTALabel.Type.FREE)
                        s.append(label.name);
                    else
                        // the channel prefix is used only by backend's non--transparent
                        // shorteners
                        s.append(labelShortener.getName(labelShortener.nameMap == null ?
                                label.name : BACKEND_CHANNEL_PREFIX + label.name,
                                false, -1));
                    s.append("]");
                    first = false;
                    ++labelCount;
                }
            if(!first)
                s.append("\n");
            s.append("endplayer\n");
        }
        return s.toString();
    }
    /**
     * Generates a string for declaration of labels.
     * 
     * @param labels labels
     * @param shortener name shortener; if not transparent, names of non--free labels
     * are prefixed with <code>BACKEND_CHANNEL_PREFIX</code>;
     * should be transparent outside the backend
     * @return a serialized form
     */
    public static String getLabels(Flavour flavour, SortedMap<String, PTALabel> labels,
            NameShortener shortener) {
        StringBuilder s = new StringBuilder();
        s.append("\nlabels\n");
        int labelCount = 0;
        for(PTALabel l : labels.values()) {
            if(labelCount != 0)
                s.append(",\n");
            s.append("    ");
            // the channel prefix is used only by backend's non--transparent
            // shorteners
            if(l.type == PTALabel.Type.FREE)
                s.append(l.name);
            else
                s.append(shortener.getName(shortener.nameMap == null ?
                        l.name : BACKEND_CHANNEL_PREFIX + l.name, false, labelCount));
            if(flavour == Flavour.HEDGEELLETH && l.isDirectional())
                s.append(" ->");
            if(l.broadcast)
                s.append(" *");
            ++labelCount;
        }
        s.append(";\n");
        return s.toString();
    }
    /**
     * Writes local declarations of a module.
     * 
     * @param context context
     * @param out writer
     * @param pta a PTA represented by the module
     * @param indexMap custom map of state indices, null for none
     */
    public static void writeModuleDeclarations(Context context, PrintWriter out,
            PTA pta, Map<PTAState, Integer> indexMap) throws IOException {
        Map<Integer, PTAState> states = pta.states;
        int statesNum = states.size();
        boolean hasLocals = false;
        if(context.flavour == PTAIO.Flavour.HEDGEELLETH || statesNum > 1) {
            out.println(INDENT_STRING + STEP_NAME + context.currPTAIndex + " : [0.." +
                    (statesNum - 1) + "] init 0;");
            hasLocals = true;
        }
        if(pta.clock != null) {
            out.println(INDENT_STRING + CLOCK_NAME + context.currPTAIndex + " : clock;");
            hasLocals = true;
        }
        if(hasLocals)
            out.println();
        SortedMap<Integer, String> invariants = new TreeMap<>();
        boolean clockInvariantFound = false;
        for(PTAState tState : states.values())
            if(tState.clockLimitHigh != null) {
                int index = indexMap != null ? indexMap.get(tState) : tState.index;
                // a clock invariant
                PTAClockCondition cc = (PTAClockCondition)tState.clockLimitHigh;
                if(cc.getValue() instanceof PTAConstant) {
                    if(!clockInvariantFound) {
                        out.print(INDENT_STRING + "invariant");
                        clockInvariantFound = true;
                    };
                    invariants.put(index,
                            INDENT_STRING + INDENT_STRING +
                            "(s" + context.currPTAIndex + "=" + index +
                            " => " + parse(context, cc, true).getExpr(false) + ")");
                } else
                    throw new IOException("clock invariant requires a constant limit");
            }
        boolean first = true;
        for(String s : invariants.values()) {
            if(!first)
                out.print(" &");
            out.print("\n" + s);
            first = false;
        }
        if(clockInvariantFound)
            out.println("\n" + INDENT_STRING + "endinvariant\n");
    }
    /**
     * Generates a guarded expression representing: guards but
     * the probabilistic ones; all error guards of a transition.<br>
     * 
     * The probabilistic guards are omitted, as they are handled
     * together with probability branches in
     * <code>printTransitions()</code>.
     * 
     * @param context context
     * @param tt transition
     * @return guarded expression, its <code>getExpr()</code> consists
     * of conditions in Prism format, each parenthesized and preceeded with
     * " & ", and do not contain probability guards
     */
    public static GuardedExpression getNPGuards(Context context, PTATransition tt)
            throws IOException {
        Set<AbstractPTAExpression> ignore = new HashSet<>();
        GuardedExpression s = new GuardedExpression("", false);
        List<AbstractPTAExpression> all = new LinkedList<>();
        if(tt.clockLimitLow != null)
            all.add(tt.clockLimitLow);
        if(tt.probability != null) {
            AbstractPTAExpression expr = tt.probability;
            all.add(expr);
            ignore.add(expr);
        }
        if(tt.guard != null)
            all.add(tt.guard);
        String e = s.getExpr();
        all.addAll(tt.update);
        ignore.addAll(tt.update);
        s.replaceExpr(e);
        for(AbstractPTAExpression expr : all) {
            GuardedExpression ge = parse(context, expr, true);
            if(!ignore.contains(expr)) {
                s.append(" & ");
                ge.replaceExpr(StringAnalyze.parenthesize(ge.getExpr()));
            } else
                ge.replaceExpr("");
            s.append(ge);
        }
        if(tt.backendWaitForSync)
            switch(context.flavour) {
                case PRISM:
                    s.append(" & !" + FORMULA_ENABLED_URGENT_NAME);
                    break;
                    
                case HEDGEELLETH:
                    s.append(" & " + context.backendWaitForSyncString);
                    break;
                    
                default:
                    throw new RuntimeException("unknown flavour");
            }
        return s;
    }
    /**
     * Generates a string for updates.
     * 
     * @param context context
     * @param tt transition
     * @param statesNum number of states in the transition's pta
     * @return a serialized form
     */
    public static GuardedExpression getUpdates(Context context, PTATransition tt,
            int statesNum) throws IOException {
        final int LONG_UPDATE_SIZE  = 40;
        GuardedExpression s = new GuardedExpression("", false);
        boolean expressionWritten = false;
        String prevT = null;
        String prevComment = "";
        for(AbstractPTAExpression expr : tt.update) {
            GuardedExpression t = parse(context, expr, false);
            if(!t.isEmpty()) {
                String comment = expr.backendComment == null ?
                    "" : expr.backendComment;
                if((context.flavour != Flavour.PRISM || statesNum > 1) || expressionWritten)
                    s.append(" & ");
                if(((prevT != null && prevT.length() > LONG_UPDATE_SIZE) ||
                        t.getExpr().length() > LONG_UPDATE_SIZE) && tt.update.size() > 1 ||
                        !prevComment.equals(comment)) {
                    // split series of long updates into separate lines
                    s.append("\n" + INDENT_STRING + INDENT_STRING);
                    if(!prevComment.equals(comment))
                        s.append("//" + (comment.isEmpty() ? "" : " " + comment) +
                                "\n" + INDENT_STRING + INDENT_STRING);
                } 
                s.append("(");
                s.append(t);
                s.append(")");
                expressionWritten = true;
                prevComment = comment;
                prevT = t.getExpr();
            }
        }
        if(context.flavour == Flavour.PRISM && statesNum == 1 && !expressionWritten)
            s.append("true");
        return s;
    }
    /**
     * Checks if a variable's name clashes with a step variable of one of
     * the PTAs.
     * 
     * @param variable variable to check
     * @param ptaCount total number of PTAs
     */
    public static boolean isStepVariable(AbstractPTAVariable variable,
            int ptaCount) {
        for(int i = 0; i < ptaCount; ++i)
            if(variable.name.equals(STEP_NAME + i))
                return true;
        return false;
    }
    /**
     * Checks for name clashes between global variables and local step
     * variables.
     * 
     * @param globals global variables
     * @param ptaCount total number of generated PTAs
     */
    protected static void checkNameClash(Collection<AbstractPTAVariable> globals,
            int ptaCount) throws IOException {
        for(AbstractPTAVariable v : globals) {
            String name = v.name;
            if(isStepVariable(v, ptaCount))
                throw new IOException("name clash of variable " + name +
                        " and a step variable");
        }
    }
    /**
     * Appends comments in a statement prefix <code>source<code>
     * to comments in <code>target</code>, returns the result.
     * Duplicates are trimmed. The statements are assumed to be parts of a
     * probabilistic branch.
     * 
     * @param t target prefix
     * @param s source prefix
     * @param streamPosComments if to join comments that represent full stream
     * position into a single line, and then sort them due the position; move the other
     * comments before that line
     */
    private static String appendComments(String t, String s,
            boolean streamPosComments) {
        // these two must be equal within a probabilistic branch
        String sourcePrefixes = null;
        String targetPrefixes = null;
        List<String> comment = new LinkedList<>();
        Scanner sc = CompilerUtils.newScanner(t);
        while(sc.hasNextLine()) {
            String l = sc.nextLine();
            if(l.trim().startsWith("//"))
                comment.add(l);
            else {
                targetPrefixes = l;
                break;
            }
        }
        sc = CompilerUtils.newScanner(s);
        while(sc.hasNextLine()) {
            String l = sc.nextLine();
            if(l.trim().startsWith("//")) {
                if(!comment.contains(l))
                    comment.add(l);
            } else {
                sourcePrefixes = l;
                break;
            }
        }
        if(!targetPrefixes.equals(sourcePrefixes))
            throw new RuntimeException("prefixes expected to match in a probabilistic " +
                    "branch");
        StringBuilder out = new StringBuilder();
        SortedSet<StreamPos> positions = new TreeSet<>();
        for(String l : comment)
            if(streamPosComments) {
                SortedSet<StreamPos> localPositions = new TreeSet<>();
                Scanner pSc = CompilerUtils.newScanner(l.trim().substring(2).trim());
                while(pSc.hasNext()) {
                    String p = pSc.next();
                    StreamPos pos = StreamPos.parse(p);
                    if(pos == null || pos.line == -1) {
                        // line not parsable; treat it as an unformatted comment
                        localPositions.clear();
                        out.append(l + "\n");
                        break;
                    } else
                        localPositions.add(pos);
                }
                positions.addAll(localPositions);
            } else
                out.append(l + "\n");
        if(streamPosComments) {
            out.append(INDENT_STRING + "//");
            for(StreamPos p : positions)
                out.append(" " + p.toString());
            out.append("\n");
        }
        out.append(sourcePrefixes);
        return out.toString();
    }
    /**
     * Searches for redundant guards in a string of updates,
     * removes repeating ones.
     * 
     * @param guards a set of guards in Prism's format
     * @return a set of guards
     */
    private static String removeRedundantGuards(String guards) {
        String operator;
        if(guards.indexOf("&") != -1)
            operator = " & ";
        else
            operator = " | ";
        String[] tokens = guards.split( "\\&|\\|" );
        List<String> l = new LinkedList<>();
        for(String t : tokens) {
            t = t.trim();
            if(!l.contains(t))
                l.add(t);
        }
        StringBuilder out = new StringBuilder();
        boolean first = true;
        for(String g : l) {
            if(!first)
                out.append(operator);
            out.append(g);
            first = false;
        }
        return out.toString();
    }
    /**
     * Simplifies an equation using symbolic algebra.
     * 
     * @param context context
     * @param expr input expression
     * @return input expression possibly simplified
     */
    private static String simplify(Context context, String expr) {
        // this does not work well
        //return context.simplify.process(expr);
        return expr;
    }
    /**
     * Prints transitions of a module, joining probabilistic branches into
     * a single statement. If a transiton has error guards, a special error
     * transition is also created.<br>
     * 
     * @param context context
     * @param out writer
     * @param states subsequent states
     * @param streamPosComments if to join comments that represent full stream
     * position into a single line, and then sort them due the position; move the other
     * comments before that line
     * @param prefixes textual representations of prefixes; these include comments,
     * synchronisation, step guard
     * @param npGuards non--probabilistic guards; probabilistic
     * guards are handles by this method instead; error guards are passed in another
     * parameter
     * @param updates textual representations of the part of each transition
     * after `->'
     * @param errorGuards all error guards, including these of probabilistic guards
     * @param taddCount number of the generated PTA
     * @param indexMap custom map of state indices, null for none
     * @param sorted if not null, specifies the order of transitions
     * @param modelType model type
     * @param specialTransitionUpdate an update to put into special
     * transitions, parenthesized; null if there are no special transitions
     */
    public static void printTransitions(Context context,
            PrintWriter out, Collection<PTAState> states,
            boolean streamPosComments,
            Map<PTATransition, String> prefixes,
            Map<PTATransition, String> npGuards,
            Map<PTATransition, String> updates,
            Map<PTATransition, GuardedExpression> errorGuards,
            int taddCount, Map<PTAState, Integer> indexMap,
            List<PTATransition> sorted, ModelType modelType,
            String specialTransitionUpdate) throws IOException {
//sorted = null;
        Set<PTATransition> printed = new HashSet<>();
        // strings of transitions, keyed with one of the represented transitions
        Map<PTATransition, List<String>> xs = new HashMap<>();
        // strings of states without output transitions
        List<String> es = new LinkedList<>();
        for(PTAState state : states) {
            // state.output can contain empty sets
            boolean noOutTransitions = true;
            for(Set<PTATransition> set : state.output.values()) {
                for(PTATransition tt : set)
                    if(!printed.contains(tt)) {
                        noOutTransitions = false;
                        List<PTATransition> statement = new LinkedList<>();
                        statement.add(tt);
                        if(tt.branchId != null && tt.branchId.type ==
                                BranchId.Type.PROBABILISTIC) {
                            // groups transitions of a single probabilistic branch
                            for(Set<PTATransition> otherSet : state.output.values())
                                for(PTATransition other : otherSet)
                                    if(tt != other &&
                                            tt.branchId == other.branchId) {
                                        if((tt.label == null ? other.label != null : !tt.label.equals(other.label)) ||
                                                (tt.clockLimitLow == null ? other.clockLimitLow != null : !tt.clockLimitLow.equals(other.clockLimitLow)))
                                            throw new RuntimeException("probabilistic branch " +
                                                    "has different labels or clock conditions");
                                        statement.add(other);
                                    }
                            for(PTATransition p : statement)
                                if(p.guard != null)
                                    throw new RuntimeException("non--probabilistic guard not allowed " +
                                            "in a probabilistic branch");
                        }
                        StringBuilder u = new StringBuilder();
                        GuardedExpression allError = new GuardedExpression("", false);
                        for(PTATransition p : statement) {
                            // join comments; also merge comments from different branches
                            prefixes.put(tt, appendComments(prefixes.get(tt),
                                    prefixes.get(p), streamPosComments));
                            if(p != tt)
                                u.append(" +");
                            u.append(" ");
                            if(p.probability != null) {
                                GuardedExpression ge = parse(context, p.probability, false);
                                u.append(StringAnalyze.parenthesize(simplify(
                                        context, ge.getExpr())));
                                u.append(":");
                            }
                            u.append(updates.get(p));
                            printed.add(p);
                            allError.append(errorGuards.get(p));
                        }
                        u.append(";");
                        if(!allError.getExpr().isEmpty())
                            throw new RuntimeException("unexpected contents in " +
                                    "error guards");
                        String probEnable = removeRedundantGuards(allError.
                                getEnable());
                        if(!probEnable.isEmpty())
                            probEnable = " & " + probEnable;
                        String z = prefixes.get(tt) + probEnable +
                                // but guards after enabling error guards
                                npGuards.get(tt) + " ->" + u.toString();
                        if(sorted == null)
                            out.println(z);
                        else {
                            List<String> l = new LinkedList<>();
                            l.add(z);
                            xs.put(tt, l);
                        }
                        if(!allError.isEmpty()) {
                            if(specialTransitionUpdate == null)
                                throw new RuntimeException("unexpected special transition");
                            // parenthesizing is needed because disable guards are ORed,
                            // and OR has a lower probability in comparison to AND
                            String probDisable = StringAnalyze.parenthesize(
                                    removeRedundantGuards(allError.getDisable()));
                            z =  prefixes.get(tt) + " & " + probDisable + " -> " +
                                    specialTransitionUpdate + ";";
                            if(sorted == null)
                                out.println(z);
                            else
                                xs.get(tt).add(z);
                        }
                    }
            }
            if(noOutTransitions &&
                    (context.flavour == Flavour.HEDGEELLETH ||
                        // self--loops not allowed in a PTA
                        (modelType != ModelType.PTA))) {
                StringBuilder s = new StringBuilder();
                // without this, states without any output transitions would be
                // treated as deadlocks by Prism
                s.append("[] ");
                if(context.flavour == Flavour.PRISM && states.size() == 1)
                    s.append("true");
                else {
                    int index;
                    if(indexMap != null)
                        index = indexMap.get(state);
                    else
                        index = state.index;
                    s.append("(" + PTAIO.STEP_NAME + taddCount + "=" + index + ")");
                }
                s.append(" -> ");
                switch(context.flavour) {
                    case PRISM:
                        s.append("true");
                        break;

                    case HEDGEELLETH:
                        s.append("stop");
                        break;

                    default:
                        throw new RuntimeException("unknown flavour");
                }
                String z = PTAIO.INDENT_STRING  + s.toString() + ";";
                if(sorted == null)
                    out.println(z);
                else
                    es.add(z);
            }
        }
        if(sorted != null) {
            for(PTATransition tt : sorted) {
                List<String> l = xs.get(tt);
                if(l != null)
                    for(String s : l)
                        out.println(s);
            }
            for(String s : es)
                out.println(s);
        }
    }
    /**
     * Generates the representation of state formulas of some PTA.
     * 
     * @param out print writes
     * @param ptas all PTAs in the model, in the order of generations
     * @param indexMap map of states to their target indices, null if to
     * use current state indices also as the target ones
     * @return a textual representation
     */
    public static void generateStateFormulas(PrintWriter out, Collection<PTA> ptas,
            Map<PTAState, Integer> indexMap) {
        SortedSet<String> names = new TreeSet<>();
        for(PTA pta : ptas)
            if(pta.stateFormulas != null)
                for(StateFormula f : pta.stateFormulas)
                    names.add(f.name);
        if(!names.isEmpty())
            out.println();
        for(String name : names) {
            StringBuilder b = new StringBuilder();
            b.append("formula " + name + " = ");
            boolean first = true;
            int ptaCount = 0;
            for(PTA pta : ptas) {
                String step = STEP_NAME + ptaCount;
                for(StateFormula f : pta.stateFormulas)
                    if(f.name.equals(name)) {
                        for(PTAState s : f.states) {
                            int index;
                            if(indexMap == null)
                                index = s.index;
                            else
                                index = indexMap.get(s);
                            if(!first) {
                                char c;
                                if(f.or)
                                    c = '|';
                                else
                                    c = '&';
                                b.append(c);
                            }
                            b.append("(" + step + "=" + index + ")");
                            first = false;
                        }
                    }
                ++ptaCount;
            }
            b.append(";");
            out.println(b.toString());
        }
    }
    /**
     * Returns the proper value of <code>Context</code>'s
     * <code>backendWaitForSyncString</code>.
     * 
     * @param context context, only for <code>parseLocationGuard()</code>
     * @param ptas a set of PTAs
     * @param ptaIndices pta indices
     * @param stateIndices state indices
     * @return a string or null
     */
    public static String getWaitForSyncString(Context context,
            Collection<PTA> ptas,
            Map<PTA, Integer> ptaIndices,
            Map<PTAState, Integer> stateIndices) {
        boolean found = false;
        Map<String, PTA> ptaNames = new HashMap<>();
        for(PTA p : ptas) {
            if(!found)
                for(PTATransition x : p.transitions)
                    if(x.backendWaitForSync) {
                        found = true;
                        break;
                    }
            ptaNames.put(p.name, p);
        }
        if(found) {
            String NEW_LINE;
            switch(context.flavour) {
                case PRISM:
                    NEW_LINE = "\n" + INDENT_STRING;
                    break;
                    
                case HEDGEELLETH:
                    NEW_LINE = " ";
                    break;
                    
                default:
                    throw new RuntimeException("unknown flavour");
            }
            SortedMap<String, SortedSet<PTATransition>> sets = new TreeMap<>();
            for(PTA p : ptas)
                for(PTATransition x : p.transitions) {
                    if(x.label != null) {
                        SortedSet<PTATransition> set = sets.get(x.label.name);
                        if(set == null) {
                            set = new TreeSet<>();
                            sets.put(x.label.name, set);
                        }
                        set.add(x);
                    }
                }
            StringBuilder t = new StringBuilder();
            boolean needsExternalParentheses = false;
            boolean firstS = true;
            for(SortedSet<PTATransition> set : sets.values()) {
                StringBuilder u = new StringBuilder();
                boolean firstX = true;
                u.append("(");
                boolean locationsFound = false;
                for(PTA p : ptas) {
                    SortedSet<PTATransition> local = new TreeSet<>();
                    for(PTATransition x : set)
                        if(p.name.equals(x.ownerName))
                            local.add(x);
                    if(local.isEmpty())
                        continue;
                    if(!firstX)
                        u.append(" & ");
                    boolean localNeedsExternalParentheses = false;
                    boolean firstL = true;
                    String v = "";
                    for(PTATransition xL : local) {
                        int ptaIndex = ptaIndices.get(ptaNames.get(xL.ownerName));
                        int stateIndex = stateIndices.get(xL.sourceState);
                        if(!firstL) {
                            v += " | ";
                            localNeedsExternalParentheses = true;
                        }
                        String w = PTAIO.STEP_NAME + ptaIndex + "=" + stateIndex;
                        if(xL.guard != null) {
                            if(xL.guard instanceof PTALocationGuard) {
                                if(false) {
                                    //
                                    // the location guard's sole purpose is to check for
                                    // synchronisation availability, so it is redundant in
                                    // this context
                                    //
                                    PTALocationGuard lg = (PTALocationGuard) xL.guard;
                                    String x = parseLocationGuard(context, lg, ptaIndex).getExpr(false);
                                    if(!x.isEmpty()) {
                                        x = StringAnalyze.parenthesize(x);
                                        w = "(" + w + " & " + x + ")";
                                    }
                                }
                            } else
                                throw new RuntimeException("unexpected guard type on a " +
                                        "synchronised transition");
                        }
                        v += w;
                        firstL = false;
                    }
                    if(localNeedsExternalParentheses)
                        v = "(" + v + ")";
                    if(!v.isEmpty()) {
                        u.append(v);
                        locationsFound = true;
                    }
                    firstX = false;
                }
                u.append(")");
                if(locationsFound) {
                    if(!firstS) {
                        t.append(" |" + NEW_LINE);
                        needsExternalParentheses = true;
                    }
                    t.append(u);
                    firstS = false;
                }
            }
            if(needsExternalParentheses) {
                t.insert(0, "(");
                t.append(")");
            }
            return t.toString();
        } else
            return null;
    }
    /**
     * Writes a pure runtime PTA compilation to a file.<br>
     * 
     * There is a similar method <code>PrismIO.write</code>, which
     * writes a backend PTA compilation.
     * 
     * @param fileName name od the file to create
     * @param c a PTA compilation
     * @param compactInstances replace instances with their position in a sorted list
     */
    public static void write(String fileName, PTASystem c, boolean compactInstances) throws IOException {
        try {
            File file = new File(fileName);
            PrintWriter out = new PrintWriter(new FileOutputStream(
                    file));
            boolean clocksFound = false;
            for(AbstractPTAVariable v : c.variables.values())
                if(v instanceof PTAClock)
                    clocksFound = true;
            if(clocksFound && !c.type.allowsClocks())
                throw new IOException("clocks not allowed for " +
                        c.type.getHeader());
            out.println(c.type.getHeader() + "\n");
            NameShortener shortener = new NameShortener(true, compactInstances);
            StringBuilder s = new StringBuilder();
            s.append(getOptions(c));
            Map<AbstractPTAVariable, VariableDescription> variables =
                    new HashMap<>();
            for(AbstractPTAVariable tv : c.variables.values()) {
                if(tv instanceof PTAClock) {
                    /* ignore -- clock variables are declared within modules */
                } else {
                    if(tv instanceof PTAScalarVariable ||
                            tv instanceof PTAArrayVariable) {
                        // PTAScalarVariable v = (PTAScalarVariable)tv;
                        double min = PTASystem.getMin(tv.range.range);
                        double max = PTASystem.getMax(tv.range.range);
                        if(!tv.floating &&
                                (min < Integer.MIN_VALUE || max > Integer.MAX_VALUE))
                            throw new RuntimeException("value does not fit into integer");
                        if(tv.comment != null) {
                            s.append("// ");
                            s.append(tv.comment);
                            s.append("\n");
                        }
                        s.append(getGlobalVariableDeclaration(tv, -1, min, max,
                                shortener, true) + "\n");
                        try {
                            VariableDescription vd = new VariableDescription(
                                    -1, new PTARange(null, min, max));
                            variables.put(tv, vd);
                        } catch(PTAException e) {
                           throw new RuntimeException("unexpected exception: " +
                                e.toString());
                        }
                    } else
                        throw new RuntimeException("unknown variable type");
                }
            }
            Map<PTA, String> moduleNames = new HashMap<>();
            for(PTA pta : c.ptas.values())
                moduleNames.put(pta, pta.name);
            s.append(getPlayers(c.playerPTAs, c.playerLabels, c.playerList,
                    c.ptas.values(), c.labels.values(), moduleNames,
                    shortener));
            if(!c.playerPTAs.isEmpty() && !c.type.allowsPlayers())
                throw new IOException("players not allowed for " +
                        c.type.getHeader());
            if(!c.labels.isEmpty())
                s.append(getLabels(Flavour.HEDGEELLETH, c.labels, shortener));
            out.print(s);
            int ptaCount = 0;
            Context context = new Context(Flavour.HEDGEELLETH, variables,
                    shortener, false, false, null);
            for(PTA pta : c.ptas.values()) {
                context.currPTAIndex = ptaCount;
                String moduleName = shortener.getName(pta.name, false, -1);
                out.println();
                if(pta.comment != null)
                    out.println("// " + pta.comment);
                out.println("module " + moduleName + "\n");
                writeModuleDeclarations(context, out, pta, null);
                Map<PTATransition, String> prefixes = new HashMap<>();
                Map<PTATransition, String> npGuards = new HashMap<>();
                Map<PTATransition, String> updates = new HashMap<>();
                Map<PTATransition, GuardedExpression> errorGuards = new HashMap<>();
                for(PTAState tState : pta.states.values())
                    for(Set<PTATransition> set : tState.output.values())
                        for(PTATransition tt : new TreeSet<>(set)) {
                            s = new StringBuilder();
                            if(tt.comments != null) {
                                for(String comment : tt.comments) {
                                    s.append(INDENT_STRING + "// ");
                                    s.append(comment);
                                    s.append("\n");
                                }
                            }
                            s.append(INDENT_STRING);
                            s.append("[");
                            if(tt.label != null) {
                                s.append(shortener.getName(tt.label.name, false,
                                    -1));
                                s.append(tt.label.getPostfix());
                            }
                            s.append("] ");
                            s.append("(" + STEP_NAME + ptaCount + "=" + tt.sourceState.index + ")");
                            prefixes.put(tt, s.toString());
                            npGuards.put(tt, getNPGuards(context, tt).getExpr(false));
                            s = new StringBuilder();
                            s.append("(" + STEP_NAME + ptaCount + "'=" + tt.targetState.index + ")");
                            GuardedExpression u = getUpdates(context, tt, pta.states.size());
                            s.append(u.getExpr(false));
                            updates.put(tt, s.toString());
                            errorGuards.put(tt, new GuardedExpression("", false));
                        }
                List<PTATransition> sorted = new LinkedList<>(pta.transitions);
                PTAIO.printTransitions(context, out, pta.states.values(),
                        false, prefixes, npGuards, updates, errorGuards,
                        ptaCount, null, sorted, c.type, null);
                out.println("\nendmodule");
                ++ptaCount;
            }
            generateStateFormulas(out, c.ptas.values(), null);
            checkNameClash(c.variables.values(), ptaCount);
            if(out.checkError())
                throw new IOException("write error to " + file.getPath());
            out.close();
        } catch(IOException e) {
            throw new IOException("backend PTA: " + e.getMessage());
        }
    }
    /**
     * A test class.
     * 
     * @param args command--line arguments
     */
    public static void main(String[] args) throws ParseException, IOException {
        String[] args_ = {
            //"../HedgeellethCompiler/regression/ct/BranchVariable.pta",
            //"BranchVariable_c.pta",
            //"../HedgeellethCompiler/regression/microgrid/Microgrid.pta",
            //"Microgrid_c.pta",
            //"../HedgeellethCompiler/regression/microgrid/MicrogridParticipants.pta",
            //"MicrogridParticipants_c.pta",
            //"../HedgeellethCompiler/regression/stateclass/Stateless.pta",
            //"Stateless_c.pta",
            //"../HedgeellethCompiler/regression/microgrid/MicrogridFast.pta",
            //"MicrogridFast_c.pta",
            //"../HedgeellethCompiler/example/ctmc/MM1.nh",
            //"MM1.nh",
            "../HedgeellethCompiler/regression/mirela/BarrierAsymmetricComplex.nh",
        };
        args = args_;
        PTASystem c = read(args[0]);
        // PTA pta = c.ptas.get("m_Server_0");
        // Trace trace = new Trace(pta);
        write(args[1], c, true);
    }
}
