/*
 * PTABinaryExpression.java
 *
 * Created on May 8, 2008
 *
 * Copyright (c) 2008, 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.pta.values.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.Generator;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.BinaryExpression;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.interpreter.op.CodeArithmetics;

/**
 * A binary expression in a PTA transition, with target variable.
 * The target variable can be null if this expression is a branch
 * condition.
 * 
 * @author Artur Rataj
 */
public class PTABinaryExpression extends AbstractPTAExpression {
    /**
     * Left operand.
     */
    public AbstractPTAValue left;
    /**
     * Right operand.
     */
    public AbstractPTAValue right;
    /**
     * Operator.
     */
    public CodeOpBinaryExpression.Op operator;
    
    /**
     * Creates a new instance of PTABinaryExpression, with
     * null target.
     * 
     * @param left                      left operand
     * @param right                     right operand
     * @param operator                  operator
     */
    public PTABinaryExpression(AbstractPTAValue left, AbstractPTAValue right,
            CodeOpBinaryExpression.Op  operator) {
        super();
        this.left = left;
        this.right = right;
        this.operator = operator;
    }
    @Override
    public boolean isNestable() {
        return true;
    }
    @Override
    public AbstractPTAExpression newComplementary() {
        BinaryExpression.Op treeOperator = CodeOpBinaryExpression.
                operatorCodeToTree(operator);
        treeOperator = treeOperator.newOpposite();
        if(!treeOperator.isRelational())
            throw new RuntimeException("no relational operator");
        BinaryExpression.Op swappedOperator =
                Generator.treeToCodeSwapOperator(treeOperator);
        boolean swapOperands =
                Generator.treeToCodeSwap(treeOperator);
        CodeOpBinaryExpression.Op swappedCodeOperator =
                CodeOpBinaryExpression.operatorTreeToCode(swappedOperator);
        PTABinaryExpression o = new PTABinaryExpression(
                swapOperands ? right : left,
                swapOperands ? left : right,
                swappedCodeOperator);
        return o;
    }
    @Override
    public boolean isEquivalentCondition(
            AbstractPTAExpression other) {
        if(!isConditional() || !other.isConditional())
            throw new RuntimeException("not conditions");
        if(other instanceof PTABinaryExpression) {
            PTABinaryExpression be = (PTABinaryExpression)other;
            if(operator == be.operator) {
                if(left.equals(be.left) && right.equals(be.right))
                    return true;
                // this class only allows &lt; and &lt;= inequality
                // operators, and thus, conditions equivalent but with swapped
                // operands are impossible, except for <code>==</code>
                // and <code>!=</code>
                else if(
                        // these two are commutative
                        operator == CodeOpBinaryExpression.Op.BINARY_EQUAL ||
                        operator == CodeOpBinaryExpression.Op.BINARY_INEQUAL) {
                    return left.equals(be.right) && right.equals(be.left);
                }
            }
        }
        return false;
    }
    @Override
    public PTAConstant getConstant() {
        if(left instanceof PTAConstant && right instanceof PTAConstant)
            return evaluate(new HashMap<AbstractPTAVariable,PTAConstant>());
        else
            return null;
    }
    @Override
    public boolean isFloating() {
        return left.floating || right.floating;
    }
    @Override
    public boolean isConditional() {
        return CodeOpBinaryExpression.operatorCodeToTree(operator).isRelational();
    }
    @Override
    public AbstractPTAExpression probability() {
        PTADensity d = null;
        PTAConstant c = null;
        AbstractPTAVariable v = null;
        boolean reverse = false;
        if(left instanceof PTADensity) {
            d = (PTADensity)left;
            if(right instanceof PTAConstant)
                c = (PTAConstant)right;
            else if(right instanceof AbstractPTAVariable)
                v = (AbstractPTAVariable)right;
        } else if(right instanceof PTADensity) {
            d = (PTADensity)right;
            if(left instanceof PTAConstant)
                c = (PTAConstant)left;
            else if(left instanceof AbstractPTAVariable)
                v = (AbstractPTAVariable)left;
            reverse = true;
        }
        if(d != null && d.type == PTADensity.ParameterType.UNI) {
            if(operator != CodeOpBinaryExpression.Op.BINARY_LESS &&
                    operator != CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL)
                throw new RuntimeException("uni() and value can only " +
                        "form an inequality");
            if(c != null) {
                double p = c.value;
                if(reverse)
                    p = 1.0 - p;
                return new PTAUnaryExpression(new PTAConstant(p, true),
                        CodeOpUnaryExpression.Op.NONE);
            } else if(v != null) {
                if(reverse)
                    return new PTABinaryExpression(new PTAConstant(1.0, true),
                            v, CodeOpBinaryExpression.Op.BINARY_MINUS);
                else
                    return new PTAUnaryExpression(v,
                            CodeOpUnaryExpression.Op.NONE);
            } else
                throw new RuntimeException("uni() compared to an unknown value");
        }
        return null;
    }
    @Override
    public SortedSet<AbstractPTAVariable> getSources() {
        SortedSet<AbstractPTAVariable> out = super.getSources();
        // all variables in arguments are sources in this context
// if(left == null)
//     left = left;
        out.addAll(left.getVariables());
        out.addAll(right.getVariables());
        return out;
    }
    @Override
    public List<PTARange> trueRanges() {
        if(!isConditional())
            throw new ArithmeticException("conditional expression expected");
        try {
            List<PTARange> out = new LinkedList<>();
            Set<AbstractPTAVariable> sources = getSources();
            if(sources.size() != 1)
                throw new ArithmeticException("exactly one source variable expected");
            AbstractPTAVariable variable = sources.iterator().next();
            PTARange sourceRange = variable.range;
            List<PTARange> tR = new LinkedList<>();
            PTAConstant c;
            if(left instanceof PTAConstant)
                c = (PTAConstant)left;
            else if(right instanceof PTAConstant)
                c = (PTAConstant)right;
            else
                throw new ArithmeticException("no comparison to constant");
            int strictDiff = variable.floating ? 0 : 1;
            switch(operator) {
                case BINARY_LESS_OR_EQUAL:
                    strictDiff = 0;
                    // fall through
                case BINARY_LESS:
                    if(left instanceof PTAConstant) {
                        PTARange r = new PTARange(null,
                                strictDiff == 0 ? (variable.floating ? c.value : Math.ceil(c.value)) :
                                    Math.floor(c.value) + strictDiff,
                                Double.POSITIVE_INFINITY);
                        if(variable.floating && operator == CodeOpBinaryExpression.Op.BINARY_LESS)
                            r.minExclusive = true;
                        tR.add(r);
                    } else {
                        PTARange r = new PTARange(null,
                                Double.NEGATIVE_INFINITY,
                                strictDiff == 0 ? (variable.floating ? c.value : Math.floor(c.value)) :
                                    Math.ceil(c.value) - strictDiff);
                        if(variable.floating && operator == CodeOpBinaryExpression.Op.BINARY_LESS)
                            r.maxExclusive = true;
                        tR.add(r);
                    }
                    break;
                    
                case BINARY_EQUAL:
                    if(variable.floating || c.value == Math.floor(c.value))
                        tR.add(new PTARange(null,
                            c.value, c.value));
                    break;
                    
                case BINARY_INEQUAL:
                    PTARange rLow = new PTARange(null,
                            Double.NEGATIVE_INFINITY,
                            strictDiff == 0 ? c.value : Math.ceil(c.value) - strictDiff);
                    PTARange rHigh = new PTARange(null,
                            strictDiff == 0 ? c.value : Math.floor(c.value) + strictDiff,
                            Double.POSITIVE_INFINITY);
                    if(variable.floating) {
                        rLow.maxExclusive = true;
                        rHigh.minExclusive = true;
                    } else {
                        if(rLow.getMax() + 1.0 == rHigh.getMin()) {
                            rLow = new PTARange(null,
                                    rLow.getMin(), rHigh.getMax());
                            rHigh = null;
                        }
                    }
                    tR.add(rLow);
                    if(rHigh != null)
                        tR.add(rHigh);
                    break;

                default:
                    throw new RuntimeException("unexpected operator");
            }
            for(PTARange r : tR) {
                PTARange common = r.overlaps(sourceRange);
                if(common != null)
                    out.add(common);
            }
            return out;
        } catch(PTAException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    @Override
    public PTAConstant evaluate(Map<AbstractPTAVariable,
            PTAConstant> knowns) {
        CodeOpBinaryExpression be = new CodeOpBinaryExpression(
                operator, null);
        be.left = toConstant(left, knowns);
        if(be.left == null)
            return null;
        else {
            be.right = toConstant(right, knowns);
            if(be.right == null)
                return null;
            else {
                try {
                    return fromLiteral(CodeArithmetics.computeValue(be));
                } catch(InterpreterException e) {
                    throw new RuntimeException("unexpected: " + e);
                }
            }
        }
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof PTABinaryExpression) {
            PTABinaryExpression other = (PTABinaryExpression)o;
            return left.equals(other.left) &&
                    operator == other.operator &&
                    right.equals(other.right);
        } else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.left);
        hash = 83 * hash + Objects.hashCode(this.right);
        hash = 83 * hash + (this.operator != null ? this.operator.hashCode() : 0);
        return hash;
    }
    @Override
    public PTABinaryExpression clone() {
        PTABinaryExpression copy = (PTABinaryExpression)super.clone();
        copy.left = left;
        copy.right = right;
        copy.operator = operator;
        return copy;
    }
    @Override
    public String toString() {
        return super.toString() +
                left.toString() + BinaryExpression.getOperatorName(
                    CodeOpBinaryExpression.operatorCodeToTree(operator)) +
                right.toString();
    }
}
