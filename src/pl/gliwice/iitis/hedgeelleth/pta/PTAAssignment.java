/*
 * PTAAssignment.java
 *
 * Created on May 8, 2008
 *
 * Copyright (c) 2008, 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeConstant;
import pl.gliwice.iitis.hedgeelleth.compiler.code.RangedCodeValue;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAVariable;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAValue;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.AbstractCodeOp.Op;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAConstant;

/**
 * <p>A pure assignment in a PTA transition. It is an unary expression
 * with operator Op.NONE.</p>
 * 
 * <p>Allows for <code>PTABufferedConstant</code>.</p>
 * 
 * @author Artur Rataj
 */
public class PTAAssignment extends PTAUnaryExpression {
    /**
     * Creates a new instance of PTAAssignment.
     * 
     * @param lvalue                    variable that is written to,
     *                                  can be null for a simple
     *                                  variable expression
     * @param rvalue                    value assigned to the variable
     */
    public PTAAssignment(AbstractPTAVariable lvalue, AbstractPTAValue rvalue) {
        super(rvalue, Op.NONE);
        target = lvalue;
    }
    @Override
    public PTAConstant evaluate(Map<AbstractPTAVariable,
            PTAConstant> knowns) {
        RangedCodeValue r = toConstant(sub, knowns);
        if(r != null)
            return fromLiteral(((CodeConstant)r.value).value);
        else
            return null;
    }
    @Override
    public PTAAssignment clone() {
        PTAAssignment copy = (PTAAssignment)super.clone();
        return copy;
    }
    @Override
    public String toString() {
        String s;
        if(target != null)
            s = target.toString() + ":=";
        else
            s = "";
        return s + sub.toString();
    }
}
