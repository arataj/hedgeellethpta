/*
 * ClockOptimizer.java
 *
 * Created on Nov 12, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.optimize;

import java.util.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.AbstractCodeOp;

import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;

/**
 * Optimizes clock contraints.
 * 
 * @author Artur Rataj
 */
public class ClockOptimizer {
    /**
     * Defines a time range.
     */
    static protected class Range {
        /**
         * Minimum value, 0 ... finite value.
         */
        public double min;
        /**
         * Maximum value, 0 ... infinity.
         */
        public double max;
        
        /**
         * Writes this range, as a state invariant and a clock condition,
         * to a given location and a transition, which is, or is expected to
         * become, that location's output transition
         * 
         * @param clock clock to use
         * @param loc a location
         * @param x a transition
         * 
         */
        protected void write(PTAClock clock, PTAState loc, PTATransition x) {
            if(!Double.isInfinite(max)) {
                loc.clockLimitHigh = new PTAClockCondition(
                        clock,
                        new PTAConstant(max, max % 1 != 0),
                        AbstractCodeOp.Op.BINARY_LESS_OR_EQUAL);
                loc.clockLimitHigh.backendEnableStop();
            } else
                loc.clockLimitHigh = null;
            if(min > 0) {
                x.clockLimitLow = new PTAClockCondition(
                        new PTAConstant(min, min % 1 != 0),
                        clock,
                        AbstractCodeOp.Op.BINARY_LESS_OR_EQUAL);
                x.clockLimitLow.backendEnableStop();
            } else
                x.clockLimitLow = null;
        }
        @Override
        public String toString() {
            return "<" + min + "; " + max + ">";
        }
    }
    /**
     * Combines two time ranges.
     * 
     * @param r1 first range
     * @param r2 second range
     * @return new, resulting range
     */
    protected static Range sum(Range r1, Range r2) {
        Range out = new Range();
        out.min = r1.min + r2.min;
        out.max = r1.max + r2.max;
        return out;
    }
    /**
     * Returns the maximum time possible of staying within a location.
     * Clock reset is expected before the location, as is always the case in
     * PTAs generated with <code>TADDFactory.newTADD()</code>.
     * 
     * @param inv a clock invariant on a location
     * @return maximum time or <code>Double.NaN</code> for a
     * hidden time
     */
    protected static double invariantMax(PTAClockCondition inv) {
        double max = Double.POSITIVE_INFINITY;
        if(inv != null) {
            if(inv.left instanceof PTAClock && inv.right instanceof PTAConstant) {
                if (inv.operator == AbstractCodeOp.Op.BINARY_LESS_OR_EQUAL) {
                    PTAConstant c = (PTAConstant)inv.right;
                    max = c.value;
                } else if(inv.operator == AbstractCodeOp.Op.BINARY_LESS
                        && ((PTAConstant) inv.right).value == 0.0)
                    throw new RuntimeException("unexpected hidden location");
                else
                    throw new RuntimeException("unsupported invariant format");
            }
        }
        return max;
    }
    /**
     * Returns the effective range of a given location/outcoming transition.
     * 
     * @param loc location with a single outcoming transition; clock constraints
     * can be defined as specified in <code>optimize()</code>.
     * @return time range
     */
    protected static Range getRange(PTAState loc) {
        Range out = new Range();
        PTATransition x;
        if(loc.getOutputsNum() > 1) {
            throw new RuntimeException("single output expected");
            /*
            // more output transitions means no clock constrains
            // on the output transitions possible, test it
            for(Set<PTATransition> set : loc.output.values())
                for(PTATransition t : set)
                    if(t.clockLimitLow != null)
                        throw new RuntimeException("unexpected clock constraint");
            x = null;
            */
        } else
            x = loc.getSingleOutput();
        PTAClockCondition high = loc.clockLimitHigh;
        PTAClockCondition low = x.clockLimitLow;
        if(low == null) {
            out.min = 0;
            out.max = invariantMax(high);
        } else if(low.operator ==  AbstractCodeOp.Op.BINARY_EQUAL) {
            if(low.left instanceof PTAClock) {
                out.max = out.min = ((PTAConstant)low.right).value;
            } else if(low.right instanceof PTAClock) {
                out.max = out.min = ((PTAConstant)low.left).value;
            } else
                throw new RuntimeException("no clock found");
            if(high != null &&
                    (high.operator != AbstractCodeOp.Op.BINARY_EQUAL ||
                        ((PTAConstant)high.right).value != out.max))
                throw new RuntimeException("clock equalities incompatible");
        } else if(low.operator ==  AbstractCodeOp.Op.BINARY_LESS_OR_EQUAL) {
            if(low.left instanceof PTAClock) {
                out.max =  ((PTAConstant)low.right).value;
                out.min = 0;
            } else if(low.right instanceof PTAClock) {
                out.max = invariantMax(high);
                out.min = ((PTAConstant)low.left).value;
            } else
                throw new RuntimeException("no clock found");
        } else
            throw new RuntimeException("operator " + low.operator.toString() + " not supported");
        return out;
    }
    /**
     * Optimizes a series of locations with clock constraints,
     * also in the no----invariant simplified form as produced by
     * <code>TADDFactory.newTADD()</code>.
     * 
     * @param series a series of locations, no outcoming and incoming
     * transitions, except for possibly multiplie transitions incoming
     * to the fist location; can contain only a single element
     * @return resulting time range
     */
    protected static Range optimize(List<PTAState> series) {
        boolean optimized = false;
        Range out = new Range();
        out.min = 0.0;
        out.max = 0.0;
        for(PTAState loc : series) {
            Range r = getRange(loc);
            out = sum(out, r);
        }
        return out;
    }
    /**
     * <p>Converts clock conditions on transitions, which
     * represent an upper bound or equality, into proper
     * location invariant/clock condition pairs.</p>
     * 
     * <p>Needed for any PTA created with
     * <code>TADDFactory.newTADD()</code>,
     * because when transitions are made witht that method, locations
     * are not yet existing, and clock constrains are stored in
     * a simplified form only in transitions, no invariants,
     * what might be incompatible with model checkers.</p>
     * 
     * <p>Resets should not be added yet, as this method confuses
     * them with general updates, refusing in effect to form proper
     * series.</p>
     * 
     * @param pta pta to modify
     */
    public static void transformClockConstraints(PTA pta) {
        for(PTAState start : new LinkedList<>(pta.states.values())) {
            if(!pta.states.values().contains(start))
                continue;
            List<PTAState> series = new LinkedList<>();
            PTAState curr = start;
            PTAClock clock = null;
            SCAN:
            while(curr.output.values().size() == 1) {
                if(     // start state allowed only as the first in series
                        (curr == pta.startState && !series.isEmpty())||
                        // the first  in a series can be within 
                        // formulas, as it is the one in the series not being
                        // lost
                        (!series.isEmpty() && pta.inStateFormula(curr)))
                    break SCAN;
                SortedSet<PTATransition> o = curr.output.values().iterator().next();
                if(o.size() != 1)
                    break SCAN;
                // a sole output transition of <code>o</code>
                PTATransition x = o.iterator().next();
                if(curr.clockLimitHigh != null || x.clockLimitLow != null) {
                    PTAClock c;
                    if(curr.clockLimitHigh != null)
                        c = curr.clockLimitHigh.getClock();
                    else
                        c = x.clockLimitLow.getClock();
                    if(clock == null)
                        clock = c;
                    else if(clock != c)
                        throw new RuntimeException("different clocks");
                    if(curr.clockLimitHigh != null &&
                            curr.clockLimitHigh.operator == AbstractCodeOp.Op.BINARY_LESS &&
                            ((PTAConstant)curr.clockLimitHigh.right).value == 0.0)                    
                        // hidden locations omitted by this optimizer
                        break SCAN;
                    if((curr.clockLimitHigh != null && curr.clockLimitHigh.containsDensity()) ||
                            (x.clockLimitLow != null && x.clockLimitLow.containsDensity()))
                        // probability densities not supported by this optimizer
                        break SCAN;
                    if(x.label == null && x.guard == null && x.probability == null &&
                            !x.isSpecial()) {
                        if(curr != start && curr.getInputsNum() != 1)
                            // an external jump is not allowed within the series
                            break SCAN;
                        series.add(curr);
                        curr = x.targetState;
                        if(     // a cycle -- do not loop
                                series.contains(curr) ||
                                // an update allowed only at the end
                                !x.update.isEmpty()) {
                            break SCAN;
                        }
                    } else {
                        // only a single--element series, the only case
                        // which allows multiple outputs; only to convert
                        // from the simplified form
                        if(series.isEmpty())
                            series.add(curr);
                        break SCAN;
                    }
                } else
                    break SCAN;
            }
            if(series != null && series.size() >= 1) {
                Range range = optimize(series);
// if(range.min > 0)
//     range = range;
                PTAState source = series.get(0);
                if(series.size() == 1) {
                    PTATransition tt = source.getSingleOutput();
                    PTA.TransitionBag xbag = pta.keyBefore(tt);
                    range.write(clock, source, tt);
                    pta.keyAfter(xbag);
                    /*
                    // the only case which allows multiple outputs
                    for(Set<PTATransition> set : source.output.values())
                        for(PTATransition t : set)
                            range.write(clock, source, t);
                      */
                } else {
                    PTATransition lastTransition = series.get(series.size() - 1).getSingleOutput();
                    PTAState target = lastTransition.targetState;
                    PTATransition compact = new PTATransition(null);
                    compact.update = lastTransition.update;
                    range.write(clock, source, compact);
                    compact.backendOpIndex = source.getSingleOutput().backendOpIndex;
                    List<AbstractCodeOp> ops = new LinkedList<>();
                    List<String> opStrings = new LinkedList<>();
                    for(PTAState s : series) {
                        PTATransition tt = s.getSingleOutput();
                        for(AbstractCodeOp op : tt.backendOps) {
                            // series of updates may come from e. g. an unrolled loop --
                            // prevent duplicate comments, as they might become too
                            // long
                            String t = op.toString();
                            if(!opStrings.contains(t)) {
                                ops.add(op);
                                opStrings.add(t);
                            }
                        }
                    }
                    PTAOptimizer.removeSeries(pta, source.getSingleOutput(), target);
                    compact.sourceState = source;
                    compact.targetState = target;
                    compact.backendOps = ops;
                    pta.addTransition(compact);
                }
            }
        }
    }
}
