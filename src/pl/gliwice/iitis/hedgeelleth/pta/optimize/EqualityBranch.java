/*
 * EqualityBranch.java
 *
 * Created on Sep 5, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.optimize;

import java.util.Set;
import pl.gliwice.iitis.hedgeelleth.pta.BranchId;
import pl.gliwice.iitis.hedgeelleth.pta.PTABinaryExpression;
import pl.gliwice.iitis.hedgeelleth.pta.PTAState;
import pl.gliwice.iitis.hedgeelleth.pta.PTATransition;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAConstant;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAScalarVariable;

/**
 * Describes a conditional branch of the form
 * <code>if(x == c) then a else n</code>.
 * If transitions within the branch can not contain any
 * other decorations, like e. g. updates.<br>
 * 
 * Conditions within the branch must have a common
 * <code>AbstractPTAExpression.singleEvaluationId</code>.
 * 
 * @author Artur Rataj
 */
public class EqualityBranch {
    /**
     * The transition, whose output branches possibly form an
     * equality branch. Null if no equality branch of the desired form
     * has been detected.
     */
    PTAState root;
    /**
     * Variable tested. Null if no equality branch of the desired form
     * has been detected.
     */
    public PTAScalarVariable x = null;
    /**
     * A constant, to which the variable is compared.
     */
    public Integer c = null;
    /**
     * A transition along the equality branch.
     */
    public PTATransition target = null;
    /**
     * A transition along the inequality branch. It does not lead to
     * the state, to which leads <code>target</code>
     */
    public PTATransition next = null;
    
    /**
     * Describes an equality branch, that begins at <code>state</code>.
     * If the state's output transitions do not form the desired branch, then
     * the fields <code>root</code> and <code>x</code> are null.
     * 
     * @param root state, whose outputs are to be analyzed
     */
    public EqualityBranch(PTAState root) {
        boolean invalid = false;
        int outCount = 0;
        BranchId branchId = null;
        long singleEvaluationId = -1;
        // check if of the form <code>if(x == c) then a</code>
        // and find <code>x</code>, <code>c</code>
        SCAN_OUTPUTS:
        for(Set<PTATransition> set : root.output.values()) {
            boolean conditional = false;
            for(PTATransition tt : set) {
                if(tt.branchId != null && tt.branchId.type == BranchId.Type.CONDITIONAL) {
                    conditional = true;
                    if(branchId == null)
                        branchId = tt.branchId;
                    else if(branchId != tt.branchId)
                        throw new RuntimeException("conditional branch not shared amongst " +
                                "all output transitions");
                } else if(conditional)
                    throw new RuntimeException("conditional branch not shared amongst " +
                            "all output transitions");
                if(!conditional ||
                        !tt.update.isEmpty() ||
                        tt.probability != null ||
                        tt.clockLimitLow != null ||
                        tt.label != null ||
                        !(tt.guard instanceof PTABinaryExpression))
                    invalid = true;
                else {
                    PTABinaryExpression condition = (PTABinaryExpression)tt.guard;
                    switch(condition.operator) {
                        case BINARY_EQUAL:
                            if(condition.left instanceof PTAConstant) {
                                double d = ((PTAConstant)condition.left).value;
                                if(d > Integer.MAX_VALUE || d < Integer.MIN_VALUE)
                                    invalid = true;
                                else
                                    c = (int)d;
                            } else if(condition.left instanceof PTAScalarVariable)
                                x = (PTAScalarVariable)condition.left;
                            if(condition.right instanceof PTAConstant) {
                                if(c != null)
                                    invalid = true;
                                else {
                                    double d = ((PTAConstant)condition.right).value;
                                    if(d > Integer.MAX_VALUE || d < Integer.MIN_VALUE)
                                        invalid = true;
                                    else
                                        c = (int)d;
                                }
                            } else if(condition.right instanceof PTAScalarVariable) {
                                if(x != null)
                                    invalid = true;
                                else
                                    x = (PTAScalarVariable)condition.right;
                            }
                            if(x == null || c == null)
                                invalid = true;
                            else
                                target = tt;
                            if(!invalid && !x.backendLocal) {
                                // this variable could be modified by a concurrent thread,
                                // check if single evaluation is required
                                if(singleEvaluationId == -1) {
                                    singleEvaluationId = condition.singleEvaluationId;
                                    if(singleEvaluationId == -1)
                                        invalid = true;
                                } else if(singleEvaluationId != condition.singleEvaluationId)
                                    invalid = true;
                            }
                            break;

                        case BINARY_INEQUAL:
                            next = tt;
                            break;

                        default:
                            invalid = true;
                            break;
                    }
// if(x != null && x.backendLocal)
//     x = x;
                }
                if(invalid)
                    break SCAN_OUTPUTS;
                ++outCount;
            }
        }
        if(outCount != 2 ||
                target.targetState == next.targetState)
            invalid = true;
        if(invalid) {
            root = null;
            x = null;
        }
        this.root = root;
    }
}
