/*
 * Trace.java
 *
 * Created on Jul 30, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.optimize;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;

/**
 * A trace within a PTA. UNIMPLEMENTED, UNTESTED and UNUSED.
 * 
 * @author Artur Rataj
 */
public class Trace {
    /**
     * The traced PTA.
     */
    PTA pta;
    /**
     * Values of each variable, keyed with transitions.
     * 
     * A value can be either a constant or unknown, the latter denoted by
     * null.
     */
    protected Map<AbstractPTAVariable, Map<PTATransition,
            PTAConstant>> values;
    /**
     * If a given variable has already been propagated through a given
     * transition, when having a given value.
     */
    protected Map<AbstractPTAVariable, Map<PTATransition,
            Set<PTAConstant>>> visited;
    
    /**
     * Creates a trace for a given PTA. No clock variables are traced.
     * 
     * @param pta pta to trace
     */
    public Trace(PTA pta) {
        this.pta = pta;
        values = new HashMap<>();
        visited = new HashMap<>();
        findTraces();
    }
    /**
     * Puts a value of a variable at some transition, before that transition's
     * updates. If more than a single value is specified for a single variable in a single
     * transition, and these values are not all the same constant, the resulting
     * value is unknown.
     * 
     * @param v variable
     * @param tt transition
     * @param c constant or null for unknown
     */
    protected void putValue(AbstractPTAVariable v, PTATransition tt,
            PTAConstant c) {
        Map<PTATransition, PTAConstant> map = values.get(v);
        if(map == null) {
            map = new HashMap<>();
            values.put(v, map);
        }
        if(map.containsKey(tt)) {
            PTAConstant original = map.get(tt);
            if(original == null || !original.equals(c))
                c = null;
        }
        map.put(tt, c);
    }
    /**
     * Returns a value of a given variable, at a given transition
     * but before the transition's updates.
     * 
     * @param v variable
     * @param tt transition
     * @return constant value or null for unknown
     */
    public PTAConstant getValue(AbstractPTAVariable v, PTATransition tt) {
        Map<PTATransition, PTAConstant> set = values.get(v);
        if(set != null)
            return set.get(tt);
        else
            return null;
    }
    /**
     * If a given variable has already been propagated through
     * a given transition, when having a given value. If not,
     * mark as traced and return false. If yes, return true.
     * 
     * @param tt transiton
     * @param v variable
     * @param c constant, null for unknown
     * @return if traced
     */
    protected boolean traced(PTATransition tt, AbstractPTAVariable v,
            PTAConstant c) {
        if(v instanceof PTAClock)
            throw new RuntimeException("clock not allowed");
        Map<PTATransition, Set<PTAConstant>> transitions = visited.get(v);
        if(transitions == null) {
            transitions = new HashMap<>();
            visited.put(v, transitions);
        }
        Set<PTAConstant> set = transitions.get(tt);
        if(set == null) {
            set = new HashSet<>();
            transitions.put(tt, set);
        }
        if(set.contains(c))
            return true;
        else {
            set.add(c);
            return false;
        }
    }
    /**
     * If a guard allows to fire a given transition, given
     * that <code>v</code> has a value of <code>c</code>.
     * 
     * @param tt transiton
     * @param v variable
     * @param c constant, null for unknown
     * @return true for guard always allows, false for guard nevel
     * allows, null for indeterminate
     */
    protected Boolean firable(PTATransition tt, AbstractPTAVariable v,
            PTAConstant c) {
        if(v instanceof PTAClock)
            throw new RuntimeException("clock not allowed");
        if(tt.probability != null)
            throw new RuntimeException("probability not expected");
        AbstractPTAExpression guard = tt.guard;
        Set<AbstractPTAVariable> sources = guard.getSources();
        if(sources.size() != 1)
            // more than a single variable matters
            return null;
        if(!v.equals(sources.iterator().next()))
            // some other variable matters
            return null;
        Map<AbstractPTAVariable, PTAConstant> knowns =
                new HashMap<>();
        knowns.put(v, c);
        PTAConstant result = guard.evaluate(knowns);
        if(result == null)
            return null;
        else if(result.value == 1.0)
            return true;
        else
            return false;
    }
    /**
     * Propagates a value along all accessible transitions.
     * 
     * @param start from here start the propagation
     * @param v value of that variable are propagated
     * @param c propagated constant, null for unknown
     */
    protected void propagate(PTAState start,
            AbstractPTAVariable v, PTAConstant c) {
        for(Set<PTATransition> set : start.output.values())
            for(PTATransition out : set)
                if(!traced(out, v, c)) {
                    // mark a zone
                    putValue(v, out, c);
                    boolean stop = false;
                    for(AbstractPTAExpression u : out.update)
                        if(u.target.equals(v)) {
                            // end of any zone
                            stop = true;
                            break;
                        }
                    if(!stop) {
                        Boolean firable = firable(out, v, c);
                        PTAConstant after;
                        if(firable == null)
                            // the beginning of a gray zone
                            after = null;
                        else
                            // continuation of the white zone
                            after = c;
                        if(firable == null || firable != false) // ???
                            propagate(out.targetState, v, after);
                    }
                }
    }
    protected final void findTraces() {
        for(PTATransition tt : pta.transitions) {
            for(AbstractPTAExpression expr : tt.update)
                if(!(expr.target instanceof PTAClock)) {
                    PTAConstant result = null;
                    if(expr instanceof PTAAssignment) {
                        PTAAssignment a = (PTAAssignment)expr;
                        if(a.sub instanceof PTAConstant)
                            // the only case, an update's result
                            // is considered known
                            result = (PTAConstant)a.sub;
                    }
                    propagate(tt.targetState, expr.target, result);
                }
        }
    }
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for(PTAState state : pta.states.values()) {
            s.append(state.index + ":\n");
            for(Set<PTATransition> set : state.output.values())
                for(PTATransition tt : set) {
                    s.append("    " + tt.sourceState.index + "->" +
                            tt.targetState.index + ": ");
                    for(AbstractPTAVariable v : values.keySet()) {
                        PTAConstant c = getValue(v, tt);
                        s.append(v + "=" + c + " ");
                    }
                    s.append("\n");
                }
        }
        return s.toString();
    }
}
