/*
 * TraceOptimizer.java
 *
 * Created on Jul 30, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.optimize;

import java.util.*;
import pl.gliwice.iitis.hedgeelleth.Hedgeelleth;

import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.UnaryExpression;
import pl.gliwice.iitis.hedgeelleth.interpreter.op.CodeArithmetics;
import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;

/**
 * Optimizes a PTA by tracing effects of updates. To be used by the backend.<br>
 * 
 * The methods may invalidate <code>PTATransition.connectSourceIndex</code>
 * and <code>PTATransition.connectTargetIndex</code> and thus
 * <code>TADDFactory.checkConnectivity()</code> could be run later to
 * correct these value. These fields are not required to be valid by any
 * method in this class.
 * 
 * @author Artur Rataj
 */
public class PTAOptimizer {
    /**
     * Up to such divergence from 0 or 1 treated as respectively 0 or 1.
     */
    final static double PROB_EPSILON = 1e-15;
    
    /**
     * A branch description. See <code>analyzeBranch()</code> for details.
     * 
     * @see PTAOptimizer.analyzeBranch
     */
    protected static class BranchDescription {
        /**
         * Start of this branch.
         */
        public PTATransition start;
        /**
         * Update in this branch, null for none.
         */
        public AbstractPTAExpression update;
        /**
         * Target start of this branch.
         */
        public PTAState target;
        /**
         * List of subsequent internal states. not including
         * the start and the end start. These states would be removed
         * if <code>remove()</code> would be called.
         */
        public List<PTAState> inside;
        
        /**
         * Creates a new branch description.
         * 
         * @param start first transition in this branch
         * @param update update, null for none
         * @param target target start
         * @param inside list of subsequent states within
         * the branch, without the start and the end start
         */
        public BranchDescription(PTATransition start,
                AbstractPTAExpression update, PTAState target,
                List<PTAState> inside) {
            this.start = start;
            this.update = update;
            this.target = target;
            this.inside = inside;
        }
        /**
         * Removes the described branch from a pta.
         * 
         * @param pta pta to modify
         */
        public void remove(PTA pta) {
            removeSeries(pta, start, target);
        }
    }
    static class ExpressionWithTransition {
        public AbstractPTAExpression expr;
        public PTATransition x;
        
        public ExpressionWithTransition(AbstractPTAExpression expr,
                        PTATransition x) {
             this.expr = expr;
             this.x = x;
        }
    };
    /**
     * Removes a series of transitions that contains no forks
     * within.
     * 
     * @param pta pta to modify
     * @param start first transition in the series to remove
     * @param stop a target state of the series; it is not removed
     */
    protected static void removeSeries(PTA pta, PTATransition start,
            PTAState stop) {
        PTATransition curr = start;
        while(true) {
            if(!curr.sourceState.output.get(curr.targetState.index).
                        remove(curr) || // both evaluated if no exception is thrown
                    !curr.targetState.input.get(curr.sourceState.index).
                        remove(curr) || // both evaluated if no exception is thrown
                    !pta.removeFromLists(curr))
                throw new RuntimeException("unsuccessful removal");
            if(curr.sourceState.output.get(curr.targetState.index).isEmpty())
                curr.sourceState.output.remove(curr.targetState.index);
            if(curr.targetState.input.get(curr.sourceState.index).isEmpty()) {
                // that state might be preserved if the last in the series, but
                // change of input does not change the key
                curr.targetState.input.remove(curr.sourceState.index);
            } if(curr.targetState != stop) {
                pta.states.remove(curr.targetState.index);
                curr = curr.targetState.getSingleOutput();
            } else
                break;
        }
    }
    /**
     * Simplifies non--deterministic branches.
     * 
     * @param pta pta to simplify
     * @return if the PTA has been modified
     */
    public static boolean nonDeterministic_(PTA pta) {
        boolean stable = true;
        for(PTAState state : new LinkedList<>(pta.states.values())) {
            if(!pta.states.values().contains(state))
                continue;
            int count = 0;
            boolean invalid = false;
            for(Set<PTATransition> set : state.output.values()) {
                for(PTATransition tt : set) {
                    if(tt.guard != null ||
                            tt.probability != null ||
                            tt.clockLimitLow != null ||
                            tt.label != null)
                        invalid = true;
                    ++count;
                }
            }
            if(count > 1 && !invalid) {
                // it is a non--deterministic branch
                Map<PTATransition, PTATransition> destination =
                        new HashMap<>();
                //int destinationCount;
                boolean canonical = true;
                AbstractPTAVariable v = null;
                PTAState cross = null;
                PTAState target = null;
                SCAN_TRANSITIONS:
                for(Set<PTATransition> set : state.output.values()) {
                    for(PTATransition tt : set) {
                        if(tt.update.size() != 1) {
                            canonical = false;
                            break SCAN_TRANSITIONS;
                        }   
                        AbstractPTAExpression update = tt.update.iterator().next();
                        // all need to be assignments to the same variable
                        if(update instanceof PTAAssignment &&
                                (v == null || update.target.equals(v))) {
                            v = update.target;
                            // all need to point to the same cross
                            if(cross != null && cross != tt.targetState) {
                                canonical = false;
                                break SCAN_TRANSITIONS;
                            }
                            cross = tt.targetState;
                            // the cross should have inputs only from the
                            // tested branch
                            if(cross.getInputsNum() != count) {
                                canonical = false;
                                break SCAN_TRANSITIONS;
                            }
                            for(Set<PTATransition> crossSet : cross.output.values()) {
                                for(PTATransition crossTt : crossSet) {
                                    Map<AbstractPTAVariable, PTAConstant> knowns =
                                            new HashMap<>();
                                    knowns.put(v, update.getConstant());
                                    PTAConstant result;
                                    if(crossTt.guard == null ||
                                            crossTt.probability != null ||
                                            crossTt.clockLimitLow != null ||
                                            crossTt.label != null ||
                                            !crossTt.update.isEmpty()) {
                                        canonical = false;
                                        break SCAN_TRANSITIONS;
                                    } else {
                                        result = crossTt.guard.evaluate(knowns);
                                        // the guard should be related to the
                                        // tested branch
                                        if(result == null) {
                                            canonical = false;
                                            break SCAN_TRANSITIONS;
                                        }
                                        if(result.value == 1) {
                                            // there should be a single destination transition
                                            // to the node after the cross
                                            PTAState afterCross = crossTt.targetState;
                                            if(afterCross.getInputsNum() != 1) {
                                                canonical = false;
                                                break SCAN_TRANSITIONS;
                                            }
                                            Set<Integer> s = afterCross.output.keySet();
                                            // there should be a single destination transition
                                            // after the node after the cross
                                            if(s.size() != 1) {
                                                canonical = false;
                                                break SCAN_TRANSITIONS;
                                            }
                                            Set<PTATransition> s2 =
                                                    crossTt.targetState.output.get(s.iterator().next());
                                            if(s2.size() != 1) {
                                                canonical = false;
                                                break SCAN_TRANSITIONS;
                                            }
                                            PTATransition destinationTt = s2.iterator().next();
                                            if(destinationTt.guard != null ||
                                                    destinationTt.probability != null ||
                                                    destinationTt.clockLimitLow != null ||
                                                    destinationTt.label != null) {
                                                canonical = false;
                                                break SCAN_TRANSITIONS;
                                            }
                                            // all need to point to the same target
                                            if(target != null && target != destinationTt.targetState) {
                                                canonical = false;
                                                break SCAN_TRANSITIONS;
                                            }
                                            target = destinationTt.targetState;
                                            destination.put(tt, destinationTt);
                                        }
                                    }
                                }
                            }
                        } else {
                            canonical = false;
                            break SCAN_TRANSITIONS;
                        }
                    }
                }
                // all n branches of a non--deterministic fork should
                // have destinations
                if(destination.size() != count)
                    canonical = false;
                if(canonical) {
                    for(PTATransition from : new HashSet<>(destination.keySet())) {
                        PTATransition to = destination.get(from);
                        PTA.TransitionBag xbag = pta.keyBefore(from);
                        from.update.addAll(to.update);
                        pta.keyAfter(xbag);
                        SortedSet<PTATransition> set = from.sourceState.output.get(from.targetState.index);
                        if(!set.remove(from))
                            throw new RuntimeException("transition expected");
                        if(set.isEmpty()) {
                            PTA.StateBag bag = pta.keyBefore(from.sourceState);
                            from.sourceState.output.remove(from.targetState.index);                            
                            pta.keyAfter(bag);
                        }
                        xbag = pta.keyBefore(from);
                        from.targetState = target;
                        pta.keyAfter(xbag);
                        PTA.StateBag sbag = pta.keyBefore(from.sourceState);
                        from.sourceState.addOutputTransition(from);
                        pta.keyAfter(sbag);
                        /*
                        SortedSet<PTATransition> inputSet = target.input.get(
                                from.sourceState.index);
                        if(inputSet == null) {
                            inputSet = new TreeSet<>();
                            target.input.put(from.sourceState.index, inputSet);
                        }
                        inputSet.add(from);
                        */
                        pta.removeFromLists(to.sourceState.getSingleInput());
                        pta.states.remove(to.sourceState.index);
                        // SortedSet<PTATransition> inputSet = target.input.get(to.sourceState.index);
                        // inputSet.clear();
                        target.input.remove(to.sourceState.index);
                        pta.removeFromLists(to);
                    }
                    pta.states.remove(cross.index);
                }
            }
        }
        return !stable;
    }
//    /**
//     * <p>Removes transitions, that differ only in <code>PTATransition.comment</code>.
//     * Such transitions connect the same locations, and are decorated exactly the same,
//     * excluding the comment.</p>
//     * 
//     * <p>The comment is guaranteed to be different in such a case, as
//     * <code>PTATransition</code> requires a uniqueness.</p>
//     * 
//     * @param pta the pta to optimise
//     * @return if anything changed
//     */
//    public static boolean removeRepetitiveTransitions(PTA pta) {
//        boolean modified = false;
//        for(PTAState state : pta.states.values())
//            for(Set<PTATransition> set : state.output.values()) {
//                // as repetitive transitions have a common output location,
//                // they are guaranteed to exist within a single set
//                List<PTATransition> list = new LinkedList<>(set);
//                set.clear();
//                List<List<String>> comments = new LinkedList<>();
//                for(PTATransition x : list) {
//                    List<String> c = x.comments;
//                    comments.add(c);
//                    x.comments = null;
//                }
//                set.addAll(list);
//                if(set.size() != list.size())
//                    modified = true;
//                List<PTATransition> listUnique = new LinkedList<>(set);
//                set.clear();
//                List<String> joined = new LinkedList<>();
//                for(PTATransition x : listUnique) {
//                    Iterator<PTATransition> xIt = list.iterator();
//                    Iterator<List<String>> cIt = comments.iterator();
//                    while(xIt.hasNext()) {
//                        PTATransition y = xIt.next();
//                        List<String> c = cIt.next();
//                        if(x.equals(y) && c != null && !c.isEmpty())
//                            joined.addAll(c);
//                    }
//                    if(!joined.isEmpty())
//                        x.comments = joined;
//                }
//                set.addAll(listUnique);
//            }
//if(modified)
//    modified = modified;
//        return modified;
//    }
    /**
     * Simplifies non--deterministic branches, and also labelled branches.
     * The branches must split into a number of updates, then meet at a common
     * node, then split again using a number of exclusive guards. This optimisation,
     * if succesfull, removes the common node and also guards which are always
     * false.
     * 
     * @param pta pta to simplify
     * @return if the PTA has been modified
     */
    public static boolean nonDeterministic(PTA pta) {
        boolean stable = true;
        List<PTATransition> removeUpdate = new LinkedList<>();
        for(PTAState state : new LinkedList<>(pta.states.values())) {
            if(!pta.states.values().contains(state))
                continue;
            int count = 0;
            boolean invalid = false;
            for(Set<PTATransition> set : state.output.values()) {
                for(PTATransition tt : set) {
                    if(tt.guard != null ||
                            tt.probability != null ||
                            tt.clockLimitLow != null
                            // labels are inherited now
                            /*|| tt.label != null*/)
                        invalid = true;
                    ++count;
                }
            }
            if(count > 1 && !invalid) {
                // it is a non--deterministic branch
                // true if there are two crosses: after nondeterministic assignment
                // and after conditional updates
                boolean canonical = true;
                // true if there is a single cross, and then only a conditional branch
                boolean partial = true;
                // destination for canonical
                Map<PTATransition, PTATransition> destination =
                        new HashMap<>();
                // destination for partial
                Map<PTATransition, PTATransition> partialDestination =
                        new HashMap<>();
                AbstractPTAVariable v = null;
                PTAState cross = null;
                PTAState target = null;
                SCAN_TRANSITIONS:
                for(Set<PTATransition> set : state.output.values()) {
                    for(PTATransition tt : set) {
                        if(tt.update.size() != 1) {
                            canonical = false;
                            partial = false;
                            break SCAN_TRANSITIONS;
                        }   
                        AbstractPTAExpression update = tt.update.iterator().next();
                        // all need to be assignments to the same variable
                        if(update instanceof PTAAssignment &&
                                (v == null || update.target.equals(v))) {
                            v = update.target;
                            // all need to point to the same cross
                            if(cross != null && cross != tt.targetState) {
                                canonical = false;
                                partial = false;
                                break SCAN_TRANSITIONS;
                            }
                            cross = tt.targetState;
                            // the cross should have inputs only from the
                            // tested branch
                            if(cross.getInputsNum() != count) {
                                canonical = false;
                                partial = false;
                                break SCAN_TRANSITIONS;
                            }
                            for(Set<PTATransition> crossSet : cross.output.values()) {
                                for(PTATransition crossTt : crossSet) {
                                    Map<AbstractPTAVariable, PTAConstant> knowns =
                                            new HashMap<>();
                                    knowns.put(v, update.getConstant());
                                    PTAConstant result;
                                    if(crossTt.guard == null ||
                                            crossTt.probability != null ||
                                            crossTt.clockLimitLow != null ||
                                            crossTt.label != null ||
                                            !crossTt.update.isEmpty()) {
                                        canonical = false;
                                        partial = false;
                                        break SCAN_TRANSITIONS;
                                    } else {
                                        result = crossTt.guard.evaluate(knowns);
                                        // the guard should be related to the
                                        // tested branch
                                        if(result == null) {
                                            canonical = false;
                                            partial = false;
                                            break SCAN_TRANSITIONS;
                                        }
                                        if(result.value == 1) {
                                            // after each assignment, there should be only a single
                                            // alternative
                                            if(partialDestination.keySet().contains(tt)) {
                                                canonical = false;
                                                partial = false;
                                                break SCAN_TRANSITIONS;
                                            }
                                            // conditions sufficient for partial topology, save the
                                            // transition to circumwent
                                            partialDestination.put(tt, crossTt);
                                            // is the updated variables stopped by the respective
                                            // guard?
                                            if(crossTt.guard.backendHasStop(v))
                                                removeUpdate.add(tt);
                                            // there should be a single destination transition
                                            // to the node after the cross
                                            PTAState afterCross = crossTt.targetState;
                                            if(afterCross.getInputsNum() != 1)
                                                canonical = false;
                                            else if(canonical) {
                                                // further checks have sense only for canonical
                                                // topology
                                                //
                                                // in the canonical topology, all transitions after the cross
                                                // should point to one input, one output transitions,
                                                // which in turn output to a common node
                                                Set<Integer> s = afterCross.output.keySet();
                                                // there should be a single destination transition
                                                // after the node after the cross
                                                if(s.size() != 1)
                                                    canonical = false;
                                                else {
                                                    Set<PTATransition> s2 =
                                                            crossTt.targetState.output.get(s.iterator().next());
                                                    if(s2.size() != 1)
                                                        canonical = false;
                                                    else {
                                                        PTATransition destinationTt = s2.iterator().next();
                                                        if(tt.label != null ||
                                                                destinationTt.guard != null ||
                                                                destinationTt.probability != null ||
                                                                destinationTt.clockLimitLow != null ||
                                                                destinationTt.label != null)
                                                            canonical = false;
                                                        else {
                                                            // all need to point to the same target
                                                            if(target != null && target != destinationTt.targetState)
                                                                canonical = false;
                                                            else {
                                                                target = destinationTt.targetState;
                                                                destination.put(tt, destinationTt);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            canonical = false;
                            break SCAN_TRANSITIONS;
                        }
                    }
                }
                // all n branches of a non--deterministic fork should
                // have destinations
                if(destination.size() != count)
                    canonical = false;
                if(partialDestination.size() != count)
                    partial = false;
                if(canonical || partial) {
                    stable = false;
                    int uniquenessCount = 0;
                    for(PTATransition tt : removeUpdate) {
                        // to fullfill <code>PTATransition</code>'s uniqueness
                        // condition
                        PTA.TransitionBag xbag = pta.keyBefore(tt);
                        PTATransition destinationRefresh = destination.remove(tt);
                        PTATransition partialDestinationRefresh = partialDestination.remove(tt);
                        tt.backendUniquenessId = uniquenessCount;
                        if(tt.comments == null)
                            tt.comments = new LinkedList<>();
                        tt.comments.add("removed "  + tt.update.toString());
                        tt.update.clear();
                        if(destinationRefresh != null)
                            destination.put(tt, destinationRefresh);
                        if(partialDestinationRefresh != null)
                            partialDestination.put(tt, partialDestinationRefresh);
                        pta.keyAfter(xbag);
                        ++uniquenessCount;
                    }
                }
                if(canonical) {
                    for(PTATransition from : new LinkedList<>(destination.keySet())) {
                        PTATransition to = destination.get(from);
                        PTA.TransitionBag xbag = pta.keyBefore(from);
                        from.update.addAll(to.update);
                        pta.keyAfter(xbag);
                        SortedSet<PTATransition> set = from.sourceState.output.get(from.targetState.index);
                        if(!set.remove(from))
                            throw new RuntimeException("transition expected");
                        if(set.isEmpty())
                            from.sourceState.output.remove(from.targetState.index);                            
                        xbag = pta.keyBefore(from);
                        from.targetState = target;
                        pta.keyAfter(xbag);
                        PTA.StateBag sbag = pta.keyBefore(from.sourceState);
                        from.sourceState.addOutputTransition(from);
                        pta.keyAfter(sbag);
                        // this cross' branch is used,  mark it by removal
                        cross.output.get(to.sourceState.index).remove(to.sourceState.getSingleInput());
                        pta.removeFromLists(to.sourceState.getSingleInput());
                        pta.states.remove(to.sourceState.index);
                        target.input.remove(to.sourceState.index);
                        pta.removeFromLists(to);
                    }
                    // remove cross' unused output branches
                    for(Set<PTATransition> set : cross.output.values())
                        for(PTATransition t : set) {
                            Set<PTATransition> s = t.targetState.input.get(cross.index);
                            s.remove(t);
                            pta.removeFromLists(t);
                            if(s.isEmpty())
                                t.targetState.input.remove(cross.index);
                        }
                    pta.states.remove(cross.index);
                } else if(partial) {
                    for(PTATransition from : new LinkedList<>(partialDestination.keySet())) {
                        SortedSet<PTATransition> set = from.sourceState.output.get(from.targetState.index);
//if(set == null)
//    set = set;
                        if(!set.remove(from))
                            throw new RuntimeException("transition expected");
                        if(set.isEmpty())
                            from.sourceState.output.remove(from.targetState.index);
                        PTATransition condition = partialDestination.get(from);
                        PTA.TransitionBag xbag = pta.keyBefore(from);
//if(condition == null)
//    set = set;
                        from.targetState = condition.targetState;
                        pta.keyAfter(xbag);
                        // the following two removals have any effect only at the first
                        // encounter of each of the available conditions
                        condition.targetState.input.remove(cross.index);
                        pta.removeFromLists(condition);
                        from.sourceState.addOutputTransition(from);
                    }
                    for(Set<PTATransition> set : cross.output.values())
                        for(PTATransition t : set) {
                            SortedSet in = t.targetState.input.get(cross.index);
                            if(in != null && in.contains(t)) {
                                in.remove(t);
                                if(in.isEmpty())
                                    t.targetState.input.remove(cross.index);
                            }
                            /*if(!partialDestination.containsValue(t))*/
                            pta.removeFromLists(t);
                        }
                    pta.states.remove(cross.index);
                }
                //
                // remove repetitive transitions and then clear the uniqueness ids,
                // if any
                //
                List<PTATransition> list = new LinkedList<>(removeUpdate);
                Set<PTATransition> existing = new HashSet<>();
                PTA.StateBag sbag = pta.keyBefore(state);
                for(int nextStateIndex : new TreeSet<>(state.output.keySet())) {
                    PTAState next = pta.states.get(nextStateIndex);
                    for(int prevStateIndex : next.input.keySet())
                        if(state.index == prevStateIndex) {
                            Set<PTATransition> outSet = state.output.get(nextStateIndex);
                            Set<PTATransition> inSet = next.input.get(prevStateIndex);
//if(!inSet.equals(outSet))////all
//    state = state;
                            List<PTATransition> unique = new LinkedList<>(outSet);
                            for(PTATransition tt : unique) {
                                PTA.TransitionBag xbag = pta.keyBefore(tt);
                                tt.backendUniquenessId = -1;
                                // even if the transition is repetitive, the keyed sets
                                // will have a single copy anyway
                                pta.keyAfter(xbag);
                            }
                            //PTA.StateBag sbagState = pta.keyBefore(state);
                            outSet.clear();
                            Map<PTATransition, PTATransition> outMap = new HashMap<>();
                            inSet.clear();
                            for(PTATransition tt : unique)
                                if(outSet.contains(tt)) {
                                    PTATransition kept = outMap.get(tt);
                                    kept.ndInt.addAll(tt.ndInt);
                                    // the removed transition is repetitive,
                                    // so it should be still left in the labels
                                    int index = 0;
                                    for(PTATransition x : pta.transitions) {
                                        if(x == tt) {
                                            pta.transitions.remove(index);
                                            break;
                                        }
                                        ++index;
                                    }
                                } else {
                                    outSet.add(tt);
                                    outMap.put(tt, tt);
                                }
                            //pta.keyAfter(sbagState);
                            inSet.addAll(outSet);
                            existing.addAll(outSet);
//                            for(PTATransition tt : unique)
//                                if(!outSet.contains(tt))
//                                    pta.transitions.remove(tt);
                            //outSet.clear();
                            //outSet.addAll(inSet);
                        }
                }
                for(PTATransition tt : removeUpdate) {
                    if((existing.contains(tt) || pta.transitions.contains(tt)) &&
                            tt.backendUniquenessId != -1)
                        throw new RuntimeException("unique id not deleted");
//                    if(!existing.contains(tt) && pta.transitions.contains(tt))
//                        pta.transitions.remove(tt);
                }
                // end for this set of branches
                removeUpdate.clear();
//                state.output.clear();
//                for(PTATransition tt : list) {// tt.targetState.input
//                    tt.backendUniquenessId = -1;
//                    if(removeUpdate.contains(tt))
//                        pta.transitions.remove(tt);
//                    else
//                        removeUpdate.add(tt);
//                }
//                for(PTATransition tt : pta.transitions)
//                    if(tt.sourceState == state) {
//                        SortedSet<PTATransition> set = state.output.get(tt.targetState.index);
//                        if(set == null) {
//                            set = new TreeSet<>();
//                            state.output.put(tt.targetState.index, set);
//                        }
//                        set.add(tt);
//                    }
                pta.keyAfter(sbag);
            }
        }
        return !stable;
    }
    /**
     * Simplifies non--deterministic branches, which contain assignments
     * a1, a2, ... to a temporary variable, and then join to a common transition,
     * which contains an assignment b, which passes the value of the
     * temporary variable further, in a variable f. In such a case, the
     * temporary and b are both removed, and the lvalues of a1, a2, ... become
     * p.
     * 
     * @param pta pta to simplify
     * @return if the PTA has been modified
     */
    public static boolean moveBackAssignment(PTA pta) {
        boolean stable = true;
        List<PTATransition> removeUpdate = new LinkedList<>();
        for(PTAState crossTt : new LinkedList<>(pta.states.values())) {
            if(!pta.states.values().contains(crossTt))
                continue;
            int count = 0;
            boolean invalid = false;
            PTAAssignment collectorUpdate = null;
            PTATransition collector = null;
            for(Set<PTATransition> set : crossTt.input.values()) {
                for(PTATransition tt : set) {
                    if(tt.guard != null ||
                            tt.probability != null ||
                            tt.clockLimitLow != null ||
                            tt.label != null)
                        invalid = true;
                    ++count;
                }
            }
            if(count != 0 && !invalid) {
                AbstractPTAVariable v = null;
                PTAState cross = null;
                PTAState target = null;
                SCAN_TRANSITIONS:
                for(Set<PTATransition> set : crossTt.input.values()) {
                    for(PTATransition tt : set) {
                        if(tt.update.size() != 1) {
                            invalid = true;
                            break SCAN_TRANSITIONS;
                        }
                        AbstractPTAExpression update = tt.update.iterator().next();
                        // all need to be assignments to the same variable
                        if((v == null || update.target.equals(v))) {
                            v = update.target;
                            if(v instanceof PTAScalarVariable &&
                                    (((PTAScalarVariable)v).backendWriteRange == null ||
                                        !((PTAScalarVariable)v).backendWriteRange.backendIsSourceLevel) &&
                                    ((PTAScalarVariable)v).backendLocal) {
                                // all need to point to the same cross
                                if(cross != null && cross != tt.targetState) {
                                    invalid = true;
                                    break SCAN_TRANSITIONS;
                                }
                                if(crossTt.getOutputsNum() != 1) {
                                    invalid = true;
                                    break SCAN_TRANSITIONS;
                                }
                                // check the collector only once per cross
                                if(collector == null) {
                                    collector = crossTt.getSingleOutput();
                                    if(collector.guard != null ||
                                            collector.probability != null ||
                                            collector.clockLimitLow != null ||
                                            collector.label != null ||
                                            collector.update.size() != 1 ||
                                            !(collector.update.get(0) instanceof PTAAssignment)) {
                                        invalid = true;
                                        break SCAN_TRANSITIONS;
                                    } else {
                                        collectorUpdate = (PTAAssignment)collector.update.get(0);
                                        if(collectorUpdate.sub != v ||
                                                !collectorUpdate.backendHasStop(v)) {
                                            invalid = true;
                                            break SCAN_TRANSITIONS;
                                        }
                                    }
                                }
                            } else
                                invalid = true;
                        } else
                            invalid = true;
                    }
                }
            } else
                invalid = true;
            if(!invalid) {
                for(Set<PTATransition> set : new LinkedList<>(crossTt.input.values()))
                    for(PTATransition tt : new LinkedList<>(set)) {
                        PTA.TransitionBag xbag = pta.keyBefore(tt);
                        tt.update.get(0).target = collectorUpdate.target;
                        tt.update.get(0).backendAddStops(collector.update.get(0));
                        pta.keyAfter(xbag);
                    }
                PTA.TransitionBag xbag = pta.keyBefore(collector);
                collector.update.clear();
                pta.keyAfter(xbag);
                stable = false;
            }
        }
        return !stable;
    }
    /**
     * Finds out the domain of values, asked for in a set of branch conditions.
     */
    protected static class ConditionSet {
        protected int minConstant = Integer.MAX_VALUE;
        protected int maxConstant = Integer.MIN_VALUE;
        
        /**
         * The PTA analysed.
         */
        PTA pta;
        /**
         * The root location of the whole conditional tree.
         */
        PTAState root;
        /**
         * The compared variable.
         */
        PTAScalarVariable variable;
        /**
         * Values, for which some single condition is true.
         */
        SortedSet<Integer> values;
        
        /**
         * Creates a new <code>ConditionSet</code>.
         * 
         * @param pta the pta analysed
         * @param b root condition
         */
        protected ConditionSet(PTA pta, EqualityBranch b) {
            this.pta = pta;
            root = b.root;
            variable = b.x;
            values = new TreeSet<>();
            if(!accept(b))
                throw new RuntimeException("unexpected");
        }
        /**
         * Adds a new condition to this set.
         * 
         * @param b a condition
         * @return if compatible with the other conditions
         */
        protected boolean accept(EqualityBranch b) {
            boolean accepted = false;
            if(b.x != null && b.x == variable) {
                if(minConstant > b.c)
                    minConstant = b.c;
                if(maxConstant < b.c)
                    maxConstant = b.c;
                values.add(b.c);
                accepted = true;
            }
            return accepted;
        }
        /**
         * <p>If there are holes in between <code>values</code>,
         * then this method creates transitions, through which
         * the `hole' values may pass.</p>
         * 
         * <p>The caller shuld provide itself transitions, that
         * transit values outside <code>minConstant</code> ...
         * <code>maxConstant</code>, if <code>variable</code>
         * requires that.</p>
         * 
         * @param outside the location, to which should transit out of
         * range values
         * @param lastOutside the last transition in the sequence, whose
         * guard which leads to <code>outside</code>, used in this
         * method only as a pattern to copy properties
         */
        protected void fillHoles(PTAState outside, PTATransition lastOutside) {
            //PTAVariable 
            //PTABinaryExpression lastGuard = (PTABinaryExpression)lastOutside.guard;
            //if(lastGuard.left instanceof PTAConstant)
            int holeBeg = -1;
            int holeEnd = -1;
            for(int i = minConstant; i <= maxConstant; ++i)
                if(!values.contains(i)) {
                    if(holeBeg == -1)
                        holeBeg = i;
                    holeEnd = i;
                } else if(holeBeg != -1) {
                    int holeSize = holeEnd - holeBeg + 1;
                    if(holeSize == 1) {
                        PTATransition holeOut = lastOutside.clone();
                        holeOut.sourceState = root;
                        holeOut.guard = new PTABinaryExpression(
                                variable, new PTAConstant(holeBeg, false),
                                CodeOpBinaryExpression.Op.BINARY_EQUAL);
                        holeOut.guard.backendCopyStop(lastOutside.guard);
                        pta.addTransition(holeOut);
                    } else {
                        // a temporary solution, as some backends do not support
                        // nested expressions
                        for(int j = holeBeg; j <= holeEnd; ++j) {
                            PTATransition holeOut = lastOutside.clone();
                            holeOut.sourceState = root;
                            holeOut.guard = new PTABinaryExpression(
                                    variable, new PTAConstant(j, false),
                                    CodeOpBinaryExpression.Op.BINARY_EQUAL);
                            holeOut.guard.backendCopyStop(lastOutside.guard);
                            pta.addTransition(holeOut);
                        }
                    }
                    holeBeg = -1;
                }
        }
    }
    /**
     * Simplifies conditional trees of the form
     * <code>if(x == n1) then a else if (x==n2) then b ...</code>.
     * 
     * @param pta pta to simplify
     * @return if the PTA has been modified
     */
    public static boolean conditionalTrees(PTA pta) throws PTAException {
        boolean modified = false;
        for(PTAState root : new LinkedList<>(pta.states.values())) {
            if(!pta.states.values().contains(root))
                continue;
            int count = 0;
            EqualityBranch ebRoot = new EqualityBranch(root);
            if(ebRoot.x != null) {
                // move through the tree along the "inequality" branch
                List<EqualityBranch> ebInequal = new LinkedList<>();
                PTAState curr = ebRoot.next.targetState;
                ConditionSet cs = new ConditionSet(pta, ebRoot);
                cs.accept(ebRoot);
                boolean invalid = false;
                do {
                    EqualityBranch eb = new EqualityBranch(curr);
                    if(cs.accept(eb) &&
                            /*eb.c.intValue() == ebRoot.c.intValue() + 1 + ebInequal.size() && */
                            // check, if the start can be safely deleted
                            curr.getInputsNum() == 1 &&
                            curr.clockLimitHigh == null) {
                        if(ebInequal.isEmpty()) {
                            // target start of the removed transition will eventually
                            // be removed as well
                            root.output.remove(eb.root.index);
                            pta.removeFromLists(ebRoot.next);
                            modified = true;
                        }
                        ebInequal.add(eb);
                        curr = eb.next.targetState;
                    } else
                        invalid = true;
                } while(!invalid);
                if(!ebInequal.isEmpty()) {
                    PTATransition lastEqual = null;
                    PTATransition lastInequal = null;
                    for(EqualityBranch next : ebInequal) {
                        if(lastInequal != null)
                            // source start of the removed transition has already been
                            // removed, and the respective target start will eventually
                            // be removed as well
                            pta.removeFromLists(lastInequal);
                        next.target.targetState.input.remove(next.target.sourceState.index);
                        PTA.TransitionBag xbag = pta.keyBefore(next.target);
                        next.target.sourceState = root;
                        pta.keyAfter(xbag);
                        PTA.StateBag sbag = pta.keyBefore(root);
                        root.addOutputTransition(next.target);
                        pta.keyAfter(sbag);
                        pta.states.remove(next.root.index);
                        lastEqual = next.target;
                        lastInequal = next.next;
                    }
                    cs.fillHoles(lastInequal.targetState, lastInequal);
                    for(Set<PTATransition> set : root.output.values())
                        for(PTATransition tt : set)
                            if(tt != lastEqual)
                                // once the transitions are parallel, stops can
                                // be copied from the last guard in the former
                                // series
                                tt.guard.backendAddStops(lastEqual.guard);
                    if(!lastInequal.targetState.input.get(lastInequal.sourceState.index).
                            remove(lastInequal))
                        throw new RuntimeException("input not found");
                    lastInequal.targetState.input.remove(lastInequal.sourceState.index);
                    PTA.TransitionBag xbag = pta.keyBefore(lastInequal);
                    lastInequal.sourceState = root;
                    pta.keyAfter(xbag);
                    PTA.StateBag sbag = pta.keyBefore(root);
                    root.addOutputTransition(lastInequal);
                    pta.keyAfter(sbag);
                    PTABinaryExpression condition = ((PTABinaryExpression)lastInequal.guard);
                    if(ebRoot.x.range.getMax() > cs.maxConstant) {
                        // transform the inequality condition to <code>x &gt; max_constant</code>
                        xbag = pta.keyBefore(lastInequal);
                        condition.operator = CodeOpBinaryExpression.Op.BINARY_LESS;
                        if(condition.right instanceof PTAConstant) {
                            AbstractPTAValue t = condition.left;
                            condition.left = condition.right;
                            condition.right = t;
                        }
                        condition.left = new PTAConstant(cs.maxConstant, false);
                        pta.keyAfter(xbag);
                        if(ebRoot.x.range.getMin() < cs.minConstant) {
                            // we need yet a parallel transition with <code>x &lt; min_constant</code>
                            PTATransition lastInequal2 = lastInequal.clone();
                            lastInequal2.guard = new PTABinaryExpression(
                                    condition.right, new PTAConstant(cs.minConstant, false),
                                    CodeOpBinaryExpression.Op.BINARY_LESS);
                            lastInequal2.guard.backendCopyStop(condition);
                            sbag = pta.keyBefore(lastInequal2.sourceState);
                            lastInequal2.sourceState.addOutputTransition(lastInequal2);
                            pta.keyAfter(sbag);
                            pta.addToLists(lastInequal2);
                        }
                    } else if(ebRoot.x.range.getMin() < cs.minConstant) {
                        // transform the inequality condition to <code>x &lt; min_constant</code>
                        xbag = pta.keyBefore(lastInequal);
                        condition.operator = CodeOpBinaryExpression.Op.BINARY_LESS;
                        if(condition.left instanceof PTAConstant)
                            condition.left = condition.right;
                        condition.right = new PTAConstant(cs.minConstant, false);
                        pta.keyAfter(xbag);
                    } else {
                          xbag = pta.keyBefore(lastInequal);
                          // no uncovered values left
                          lastInequal.guard = new PTAUnaryExpression(
                                  new PTAConstant(0, false), CodeOpUnaryExpression.Op.NONE);
                          lastInequal.guard.backendCopyStop(condition);
//                        throw new PTAException(
//                                lastInequal.backendOps.iterator().next().getStreamPos(),
//                                "condition branches to dead code: " +
//                                "variable " + ebRoot.x.name + " has never values outside " +
//                                ebRoot.x.range.toString());
                          pta.keyAfter(xbag);
                    }
                    BranchId common = null;
                    for(Set<PTATransition> set : root.output.values())
                        for(PTATransition tt : set) {
                            if(common == null) {
                                common = tt.branchId;
                                if(tt.ndInt != null)
                                    throw new RuntimeException("unexpected nd ids");
                            } else
                                tt.branchId = common;
                            if(tt.branchId.type != BranchId.Type.CONDITIONAL)
                                throw new RuntimeException("unexpected branch id type");
                        }
                    // root.addOutputTransition(lastInequal);
                    modified = true;
                }
            }
        }
        return modified;
    }
    /**
     * Removes updates of all locals, which are never read.<br>
     * 
     * This PTA modification is state formula--sensitive, see
     * <code>PTA.testFormulas()</code> for details.
     * 
     * @param pta pta to modify
     * @param local local variables of this PTA
     * @return if the pta has been modified
     */
    public static boolean removeUnusedLocalUpdates(PTA pta, Set<PTAScalarVariable> local) {
        boolean stable = true;
        Set<PTAScalarVariable> localsRead = new HashSet<>();
        for(PTATransition tt : new LinkedList<>(pta.transitions)) {
            Set<AbstractPTAVariable> sources = tt.getSources();
            for(PTAScalarVariable v : local)
                if(sources.contains(v))
                    localsRead.add(v);
        }
        for(PTATransition tt : new LinkedList<>(pta.transitions))
            for(AbstractPTAExpression u : new LinkedList<>(tt.update))
                if(u.target instanceof PTAScalarVariable &&
                        local.contains((PTAScalarVariable)u.target) &&
                        !localsRead.contains((PTAScalarVariable)u.target)) {
                    PTA.TransitionBag xbag = pta.keyBefore(tt);
                    tt.update.remove(u);
                    pta.keyAfter(xbag);
                    stable = false;
                }
        return !stable;
    }
    /**
     * If transport of a given local ends at some transition, add an update
     * that resets that local to its initial value. Removes updates of all locals,
     * which are never read.<br>
     * 
     * The modification does not change the model's logic, but can be
     * important for the performance of some analytic engines.<br>
     * 
     * The updates added by this method do not have stops enabled,
     * as this method should be the last one that modifies a pta.<br>
     * 
     * This PTA modification is state formula--sensitive, see
     * <code>PTA.testFormulas()</code> for details.
     * 
     * @param pta pta to modify
     * @param local local variables of this PTA
     * @return if the pta has been modified
     */
    public static boolean closeStops(PTA pta, Set<PTAScalarVariable> local) {
        boolean stable = true;
        for(PTATransition tt : new LinkedList<>(pta.transitions)) {
            Set<AbstractPTAVariable> sources = tt.getSources();
            Set<AbstractPTAExpression> closeUpdates = new HashSet<>();
/*if(tt.toString().indexOf("protocolBit") != -1 && tt.toString().indexOf("getAckBit") != -1)
    tt = tt;*/
            for(PTAScalarVariable v : local)
                if(sources.contains(v)) {
                    boolean stop = true;
                    // update is later within a transition
                    if(!tt.update.isEmpty()) {
                        for(AbstractPTAExpression u : tt.update)
                            if(!u.backendHasStop(v)) {
                                stop = false;
                                break;
                            }
                    } else if((tt.guard != null && !tt.guard.backendHasStop(v)) ||
                            (tt.probability != null && !tt.probability.backendHasStop(v)) ||
                            (tt.clockLimitLow != null && !tt.clockLimitLow.backendHasStop(v)))
                        // no update, so look for the preceeding guards
                        stop = false;
                    if(stop) {
                        PTAAssignment reset = new PTAAssignment(v,
                                new PTAConstant(v.initValue, v.floating));
                        reset.backendComment = "reset";
                        closeUpdates.add(reset);
                    }
                }
            if(!closeUpdates.isEmpty()) {
                PTA.TransitionBag xbag = pta.keyBefore(tt);
                tt.update.addAll(closeUpdates);
                pta.keyAfter(xbag);
                stable = false;
            }
        }
        return !stable;
    }
    /**
     * Returns, if it is not true, that: (1) an optional guard contains
     * a non--internal variable which is written to in the update and
     * the guard is not an equality, or (2) the guard alternately
     * contains a clock or refers to a runtime object.
     * 
     * The rationale behind is, that otherwise (1) a non--internal variable might
     * be immediately written to, thus its value after the guard is
     * taken possibly hidden, or (2) the variable is not a plain scalar,
     * and thus might possibly be sensitive in another way, what is not checked
     * by this method.
     * 
     * @param guard a guard, null for none
     * @param update a collection of updates
     * @return if the merge of the guard and of the update should be allowed
     */
    protected static boolean allowMerge(AbstractPTAExpression guard,
            List<AbstractPTAExpression> update){
        if(guard == null)
            return true;
        else {
            for(AbstractPTAVariable v : guard.getSources())
                if(v instanceof PTAScalarVariable) {
                    if(!((PTAScalarVariable)v).backendInternal &&
                            !(guard instanceof PTABinaryExpression &&
                                ((PTABinaryExpression)guard).operator ==
                                CodeOpBinaryExpression.Op.BINARY_EQUAL)) {
                        for(AbstractPTAExpression u : update)
                            if(u.target == v)
                                return false;
                    }
                } else
                    return false;
            return true;
        }
    }
    /**
     * Chooses only non--locals from a set of variables. Excludes clock
     * variables from the selection, as they can be accessed only by a single
     * PTA, like a local.
     * 
     * @param in an input set
     * @param local local variables for a given PTA
     * @return a set with non--locals only
     */
    protected static Set<AbstractPTAVariable> selectNonLocal(
            Set<AbstractPTAVariable> sources, Set<PTAScalarVariable> local) {
        Set<AbstractPTAVariable> out = new HashSet<>();
        for(AbstractPTAVariable v : sources)
            if((!(v instanceof PTAScalarVariable) || !local.contains((PTAScalarVariable)v)) &&
                    !(v instanceof PTAClock))
                out.add(v);
        return out;
    }
//    /**
//     * Compacts series of transitions containing only updates.<br>
//     * 
//     * This PTA modification is state formula--sensitive, see
//     * <code>PTA.testFormulas()</code> for details.
//     * 
//     * @param pta pta to simplify
//     * @param local local variables for this PTA, to test for field access conflicts
//     * @param implicitHidden passed to <code>PTAState.isHidden()</code>
//     * @return if the PTA has been modified
//     */
//    public static boolean compactUpdate_(PTA pta,
//            Set<PTAScalarVariable> local, boolean implicitHidden) {
//        boolean stable = true;
//        for(PTAState start : new LinkedList<>(pta.states.values())) {
//            if(!pta.states.values().contains(start))
//                continue;
//            List<PTAState> series = new LinkedList<>();
//            Set<AbstractPTAVariable> targets = new HashSet<>();
///*if(start.toString().indexOf("job1") != -1 && start.toString().indexOf("numJobs") == -1 &&
//         pta.name.indexOf("Sch") != -1 )
//    start = start;*/
//            PTAState curr = start;
//            SCAN:
//            while(curr.clockLimitHigh == null &&
//                    curr.output.values().size() == 1) {
//                if(curr == pta.startState ||
//                        // the first state in a series can be within state
//                        // formulas, as it is the one in the series not being
//                        // lost
//                        (!series.isEmpty() && pta.inStateFormula(curr)))
//                    break SCAN;
//                SortedSet<PTATransition> o = curr.output.values().iterator().next();
//                if(o.size() != 1)
//                    break SCAN;
//                // a sole output transition of <code>o</code>
//                PTATransition x = o.iterator().next();
//                if(x.label == null && x.clockLimitLow == null &&
//                        x.guard == null && x.probability == null &&
//                        !x.isSpecial()) {
//                    if(!curr.isHidden(implicitHidden)) {
//                        // this start can be visible to other PTAs -- check if
//                        // field access is not merged
//                        Set<AbstractPTAVariable> nonLocals = new HashSet<>();
//                        for(AbstractPTAExpression u : x.update) {
//                            Set<AbstractPTAVariable> l = u.getVariables();
//                            nonLocals.addAll(selectNonLocal(l, local));
//                        }
//                        if(!nonLocals.isEmpty())
//                            break SCAN;
//                    }
//                    // check for dependencies between the subsequent updates
//                    for(AbstractPTAExpression u : x.update)
//                        for(AbstractPTAVariable  t : targets)
//                            if(u.getVariables().contains(t))
//                                break SCAN;
//                    for(AbstractPTAExpression u : x.update)
//                        targets.add(u.target);
//                    if(curr != start && curr.getInputsNum() != 1)
//                        // an external jump is not allowed within the series
//                        break SCAN;
//                    series.add(curr);
//                    curr = x.targetState;
//                    if(series.contains(curr)) {
//                        // a cycle -- invalidate the whole series
//                        series = null;
//                        break SCAN;
//                    }
//                } else
//                    break SCAN;
//            }
//            if(series != null && series.size() > 1) {
//                PTAState source = series.get(0);
//                PTAState target = series.get(series.size() - 1).getSingleOutput().
//                        targetState;
//                PTATransition compact = new PTATransition(null);
//                compact.backendOpIndex = source.getSingleOutput().backendOpIndex;
//                List<AbstractCodeOp> ops = new LinkedList<>();
//                List<String> opStrings = new LinkedList<>();
//                List<AbstractPTAExpression> updates = new LinkedList<>();
//                for(PTAState s : series) {
//                    PTATransition tt = s.getSingleOutput();
//                    for(AbstractCodeOp op : tt.backendOps) {
//                        // series of updates may come from e. g. an unrolled loop --
//                        // prevent duplicate comments, as they might become too
//                        // long
//                        String t = op.toString();
//                        if(!opStrings.contains(t)) {
//                            ops.add(op);
//                            opStrings.add(t);
//                        }
//                    }
//                    updates.addAll(tt.update);
//                }
//                removeSeries(pta, source.getSingleOutput(), target);
//                compact.sourceState = source;
//                compact.targetState = target;
//                compact.backendOps = ops;
//                compact.update = updates;
//                pta.addTransition(compact);
//                stable = false;
//            }
//        }
//        return !stable;
//    }
    /**
     * Compacts series of transitions containing only updates.<br>
     * 
     * This PTA modification is state formula--sensitive, see
     * <code>PTA.testFormulas()</code> for details.
     * 
     * @param pta pta to simplify
     * @param local local variables for this PTA, to test for field access conflicts,
     * if unknown can be empty but decreases efficacy
     * @param implicitHidden passed to <code>PTAState.isHidden()</code>
     * @param integrateEmpty if it is known that no transition is going to be optimised
     * out, the special empty transitions in a series with a following transition can be
     * safely integrated with that following transition; thus this parameter should be true
     * only after all other optimisations which can delete transitions
     * @return if the PTA has been modified
     */
    public static boolean compactUpdate(PTA pta,
            Set<PTAScalarVariable> local, boolean implicitHidden,
            boolean integrateEmpty) {
        boolean stable = true;
        for(PTAState start : new LinkedList<>(pta.states.values())) {
            if(!pta.states.values().contains(start))
                continue;
            List<PTATransition> startOut = new LinkedList<>();
            for(int outputIndices : start.output.keySet())
                for(PTATransition startX : start.output.get(outputIndices))
                    startOut.add(startX);
            for(PTATransition startX : startOut) {
                List<PTAState> series = new LinkedList<>();
                Set<AbstractPTAVariable> targets = new HashSet<>();
                int nonLocalUpdateStages = 0;
                PTAState curr = start;
                SCAN:
                while(curr.clockLimitHigh == null &&
                        (curr == start || curr.output.values().size() == 1)) {
                    if((!series.isEmpty() && curr == pta.startState) ||
                            // the first state in a series can be within state
                            // formulas, as it is the one in the series not being
                            // lost
                            (!series.isEmpty() && pta.inStateFormula(curr)))
                        break SCAN;
                    SortedSet<PTATransition> o;
                    if(curr == start) {
                        o = new TreeSet<>();
                        o.add(startX);
                    } else
                        o = curr.output.values().iterator().next();
                    if(o.size() != 1)
                        break SCAN;
                    // a sole output transition of <code>o</code>
                    PTATransition x = o.iterator().next();
                    if(x.label == null && x.clockLimitLow == null &&
                            x.guard == null && (series.isEmpty() || x.probability == null) &&
                            ((integrateEmpty && series.isEmpty()) || !x.isSpecial())) {
                        if(!curr.isHidden(implicitHidden)) {
                            // this start can be visible to other PTAs -- check if
                            // field access is not merged
                            Set<AbstractPTAVariable> nonLocals = new HashSet<>();
                            for(AbstractPTAExpression u : x.update) {
                                Set<AbstractPTAVariable> l = u.getVariables();
                                nonLocals.addAll(selectNonLocal(l, local));
                            }
                            if(!nonLocals.isEmpty()) {
                                ++nonLocalUpdateStages;
                                if(nonLocalUpdateStages > 1)
                                    break SCAN;
                            }
                        }
                        // check for dependencies between the subsequent updates
                        for(AbstractPTAExpression u : x.update)
                            for(AbstractPTAVariable  t : targets)
                                if(u.getVariables().contains(t))
                                    break SCAN;
                        for(AbstractPTAExpression u : x.update)
                            targets.add(u.target);
                        if(curr != start && curr.getInputsNum() != 1)
                            // an external jump is not allowed within the series
                            break SCAN;
                        series.add(curr);
                        curr = x.targetState;
                        if(series.contains(curr)) {
                            // a cycle -- invalidate the whole series
                            series = null;
                            break SCAN;
                        }
                    } else
                        break SCAN;
                }
                if(series != null && series.size() > 1) {
                    PTAState source = series.get(0);
                    PTAState target = series.get(series.size() - 1).getSingleOutput().
                            targetState;
                    PTATransition compact = new PTATransition(null);
                    compact.backendOpIndex = startX.backendOpIndex;
                    List<AbstractCodeOp> ops = new LinkedList<>();
                    List<String> opStrings = new LinkedList<>();
                    List<AbstractPTAExpression> updates = new LinkedList<>();
                    for(PTAState s : series) {
                        PTATransition tt;
                        if(s == start)
                            tt = startX;
                        else
                            tt = s.getSingleOutput();
                        for(AbstractCodeOp op : tt.backendOps) {
                            // series of updates may come from e. g. an unrolled loop --
                            // prevent duplicate comments, as they might become too
                            // long
                            String t = op.toString();
                            if(!opStrings.contains(t)) {
                                ops.add(op);
                                opStrings.add(t);
                            }
                        }
                        updates.addAll(tt.update);
                    }
                    removeSeries(pta, startX, target);
                    compact.probability = startX.probability;
                    compact.ndInt = startX.ndInt;
                    compact.branchId = startX.branchId;
                    compact.sourceState = source;
                    compact.targetState = target;
                    compact.backendOps = ops;
                    compact.update = updates;
                    pta.addTransition(compact);
                    stable = false;
                }
            }
        }
        return !stable;
    }
    /**
     * If a state <i>s</i> is not the start, has no invariants, a
     * single incoming transition <i>i</i>, a single outcoming transition
     * <i>o</i>, <i>i</i> has only possible guard/label/clock limit,
     * <code>allowMerge(i.guard)</code> is true or the update is to a
     * different variable than the one in the guard, and <i>o</i> has
     * at most updates, then remove <i>s</i> and merge <i>i</i> and
     * <i>o</i>.
     * 
     * @param pta pta to simplify
     * @param local local variables of this PTA; if unknown then can be empty,
     * but it decrease the efficacy
     * @param implicitHidden passed to <code>PTAState.isHidden()</code>
     * @param integrateEmpty if it is known that no transition is going to be optimised
     * out, the special empty transitions in a series with a following transition can be
     * safely integrated with that following transition; thus this parameter should be true
     * only after all other optimisations which can delete transitions
     * @return if the PTA has been modified
     */
    public static boolean compactGuardUpdate(PTA pta,
            Set<PTAScalarVariable> local, boolean implicitHidden,
            boolean integrateEmpty) {
        if(integrateEmpty)
            throw new RuntimeException("does not work");
        boolean stable = true;
        for(PTAState state : new LinkedList<>(pta.states.values())) {
            if(state == pta.startState)
                continue;
            if(!pta.states.values().contains(state))
                continue;
            if(state.clockLimitHigh == null &&
                    state.input.values().size() == 1 &&
                    state.output.values().size() == 1) {
                SortedSet<PTATransition> i = state.input.values().iterator().next();
                SortedSet<PTATransition> o = state.output.values().iterator().next();
                if(i.size() == 1 && o.size() == 1) {
                    PTATransition xI = i.iterator().next();
                    PTATransition xO = o.iterator().next();
/*if(xI.toString().indexOf("nxp") != -1)
    xI = xI;*/
                    if(xI.label == null &&
                            xI.update.isEmpty() &&
                            xO.clockLimitLow == null && xO.label == null &&
                            xO.guard == null && xO.probability == null &&
                            (integrateEmpty || !xO.isSpecial()) && allowMerge(xI.guard, xO.update)) {
                        boolean fieldAccessConflict = false;
                        if((!xI.sourceState.isHidden(implicitHidden) ||
                                !state.isHidden(implicitHidden))) {
                            // this start can be visible to other PTAs -- check if
                            // field access is not merged
                            Set<AbstractPTAVariable> inputNonLocals =
                                    selectNonLocal(xI.getSources(), local);
                            Set<AbstractPTAVariable> outputNonLocals =
                                    new HashSet<>();
                            for(AbstractPTAExpression u : xO.update) {
                                Set<AbstractPTAVariable> l = u.getVariables();
                                outputNonLocals.addAll(selectNonLocal(l, local));
                            }
                            if(!inputNonLocals.isEmpty() &&
                                    !outputNonLocals.isEmpty())
                                fieldAccessConflict = true;
                        }
                        if(!fieldAccessConflict) {
                            for(AbstractCodeOp op : xO.backendOps)
                                if(!xI.backendOps.contains(op))
                                    xI.backendOps.add(op);
                            PTA.TransitionBag xbag = pta.keyBefore(xI);
                            xI.update = xO.update;
                            SortedSet<PTATransition> xISet = xI.sourceState.output.remove(xI.targetState.index);
                            // no more used
                            xbag.outputSet = null;
                            // the single transition has been removed by <code>PTA.keyBefore()</code>
                            if(xISet.size() != 0)
                                throw new RuntimeException("only a single transition expected");
                            xI.targetState = xO.targetState;
                            pta.keyAfter(xbag);
                            PTA.StateBag sbag = pta.keyBefore(xI.sourceState);
                            xI.sourceState.addOutputTransition(xI);
                            pta.keyAfter(sbag);
                            xO.targetState.input.remove(state.index);
                            SortedSet<PTATransition> set = xO.targetState.input.get(xI.sourceState.index);
                            if(set == null) {
                                set = new TreeSet<>();
                                xO.targetState.input.put(xI.sourceState.index, set);
                            }
                            set.add(xI);
                            pta.states.remove(state.index);
                            pta.removeFromLists(xO);
                            stable = false;
                        }
                    }
                }
            }
        }
        return !stable;
    }
    /**
     * Removes all connections, whose guards have always
     * false or always true values. May produce dead states.
     * If a guard is always false, the underlying transition may be
     * removed as well.<br>
     * 
     * This method can be called by PTA modification that are formula--sensitive,
     * see <code>PTA.testFormulas()</code> for details.
     * 
     * @param pta pta to optimize
     * @param waitVariables waitVariables, if these do not have their
     * ranges specified yet, otherwise null; these ranges are set within
     * <code>AbstractTADDGenerator.setLockVariablesRanges</code>
     * @param removeConstGuardsUsingRanges if false, removes
     * only transitions with guards, that are constants; if true,
     * also checks guards with ranged variables, and if constant
     * in that context, removes a respective guard as well; should normally
     * be always true, except for compiler tests
     * @return if the pta has been modified.
     */
    public static boolean removeConstGuards(PTA pta,
            List<AbstractPTAVariable> waitVariables,
            boolean removeConstGuardsUsingRanges) {
        boolean stable = true;
        for(PTATransition tt : new LinkedList<>(pta.transitions))
            if(tt.guard != null) {
                // if this is a conditional branch, then always at least
                // a single of the paths survives, and thus the source
                // state survives as well
                if((tt.branchId == null || tt.branchId.type !=
                            BranchId.Type.CONDITIONAL) &&
                        pta.inStateFormula(tt.sourceState))
                    break;
                boolean alwaysFalse = false;
                boolean alwaysTrue = false;
                PTAConstant c = tt.guard.getConstant();
                if(c != null && c.value == 0.0)
                    alwaysFalse = true;
                else if(c != null && c.value == 1.0)
                    alwaysTrue = true;
                else if(removeConstGuardsUsingRanges &&
                        tt.guard.isConditional() &&
                        tt.guard.getSources().size() == 1) {
                    // lock wait variables have not yet set their ranges, it happens later
                    // at <code>AbstractTADDGenerator.setLockVariablesRanges</code>
                    AbstractPTAVariable source = tt.guard.getSources().iterator().next();
                    boolean lockWaitFound = waitVariables != null &&
                            waitVariables.contains(source);
                    if(!lockWaitFound) {
                        List<PTARange> trueRanges;
                        try {
                            trueRanges = tt.guard.trueRanges();
                            if(trueRanges.isEmpty())
                                alwaysFalse = true;
                            else if(trueRanges.size() == 1 &&
                                    source.range.fitsTo(trueRanges.iterator().next()))
                                alwaysTrue = true;
                        } catch(ArithmeticException e) {
                            /* can not evaluate true ranges */
                        }
                    }
                }
                if(alwaysFalse) {
                    PTA.StateBag sbag = pta.keyBefore(tt.sourceState);
                    if(!tt.sourceState.output.get(tt.targetState.index).remove(tt)
                            || // if no exception, always evaluated both
                            !tt.targetState.input.get(tt.sourceState.index).remove(tt))
                        throw new RuntimeException("transition expected");
                    if(tt.sourceState.output.get(tt.targetState.index).isEmpty())
                        tt.sourceState.output.remove(tt.targetState.index);
                    pta.keyAfter(sbag);
                    if(tt.targetState.input.get(tt.sourceState.index).isEmpty())
                        tt.targetState.input.remove(tt.sourceState.index);
                    pta.removeFromLists(tt);
                    stable = false;
                } else if(alwaysTrue) {
                    PTA.TransitionBag xbag = pta.keyBefore(tt);
                    tt.guard = null;
                    pta.keyAfter(xbag);
                    stable = false;
                }
        }
        return !stable;
    }
    /**
     * Attemps to replace <code>update</code>'s target with
     * <code>update</code>'s right hand side in sources of
     * <code>expr</code>.
     * 
     * @param expr expression to create its copy and then to modify
     * @param update expression to propagate its right hand side
     * @return a new nested expression
     */
    protected static AbstractPTAExpression replace(
            AbstractPTAExpression expr, AbstractPTAExpression update) {
        if(!update.isNestable())
            throw new RuntimeException("a nestable expression expected");
        PTANestedExpression n = PTANestedExpression.newTopExpression(
                expr);
        n.propagate(update);
        return n.simplify();
    }
    /**
     * Checks recursively, if it is possible to propagate an update
     * into expressions, starting at a given root start.
     * 
     * @param update an update, whose result is tested for insertion
     * @param root root start
     * @param start start state, treated as an unsafe side jump
     * @param visited transitions visited before; they can not
     * be entered again
     * @param expressions list of candidates for propagation of
     * <code>update</code>, mapped to the owner transitions;
     * they finally define the propagation region; this method may append new
     * expressions to the list
     * @param safeJumps a list of transitions, which are safe as side
     * jumps, as after them, no variable in <code>update</code> is
     * altered; they also define the propagation region, exluding its
     * leaves
     * @param sideJumps a list to add side jumps into <code>root</code>;
     * they are tested later for belonging to the final region of propagation
     * @param onlyImmediate check root's out transitions if (i) root
     * is hidden or (ii) <code>strictFieldAccess</code> is false; do not
     * recurse further, unless other hidden states are found along
     * should be true if <code>update</code> contains non--locals, to
     * avoid their multiple evaluations
     * @param traced variables traced for stops
     * @param strictFieldAccess defined as in
     * <code>TADDOptions.strictFieldAccess</code>
     * @param implicitHidden passed to <code>PTAState.isHidden()</code>
     * @return false if at least a single start has been found, where
     * transport is unknown or does not end, but the following
     * start can not be entered; or  the transport is intercepted by assigning
     * a new value to the update's target; or some component
     * of <code>update</code> is found to be assigned; in any of these cases,
     * <code>update</code> can not be deleted from its current transition
     */
    protected static boolean nest(AbstractPTAExpression update, PTAState root, PTAState start,
            Set<PTATransition> visited, List<ExpressionWithTransition> expressions,
            Set<PTATransition> safeJumps, Set<PTATransition> sideJumps, boolean onlyImmediate,
            Set<PTAScalarVariable> traced, boolean strictFieldAccess,
            boolean implicitHidden) {
        boolean openEnd = false;
        // add later to side jumps, except for the case
        // where current root contains only propagation region's
        // border transitions, and none of them contains update's
        // target as source, as then they are known to be not really
        // the propagation region in the sense of actual modification
        Set<PTATransition> localSideJumps = new HashSet<>();
        if(root == start)
            return false;
        for(int inputIndices : root.input.keySet())
            for(PTATransition tt : root.input.get(inputIndices))
                if(!safeJumps.contains(tt))
                    localSideJumps.add(tt);
        List<PTATransition> out = new LinkedList<>();
        for(int outputIndices : root.output.keySet())
            for(PTATransition tt : root.output.get(outputIndices))
                out.add(tt);
        if((!strictFieldAccess || !onlyImmediate || root.isHidden(implicitHidden)) &&
                update.isNestable() && update.target instanceof PTAScalarVariable) {
            PTAScalarVariable r = (PTAScalarVariable)update.target;
            if(traced.contains(r)) {
                // none of the following variables can be modified in the
                // propagation region
                Set<AbstractPTAVariable> moved = update.getVariables();
                if(root.clockLimitHigh != null) {
                    Set<AbstractPTAVariable> s = root.clockLimitHigh.getSources();
                    for(AbstractPTAVariable t : moved)
                        if(s.contains(t)) {
                            // interfering clock invariants are not supported
                            openEnd = true;
                            break;
                        }
                }
                if(!openEnd)
                    for(PTATransition tt : out)
                        if(!visited.contains(tt)) {
                            visited.add(tt);
                            if(tt.clockLimitLow != null) {
                                Set<AbstractPTAVariable> s = tt.clockLimitLow.getSources();
                                for(AbstractPTAVariable t : moved)
                                    if(s.contains(t)) {
                                        // interfering clock invariants are not supported
                                        openEnd = true;
                                        break;
                                    }
                            }
                            // update is always later, give it priority in determining the stop
                            boolean checkFurther = false;
                            if(!tt.update.isEmpty()) {
                                boolean stops = false;
                                for(AbstractPTAExpression u : tt.update)
                                    if(u.backendHasStop(r) ||
                                            // check if this is not a transport by the same variable,
                                            // but already of a new value
                                            u.target.equals(r)) {
                                        stops = true;
                                        break;
                                    }
                                if(!stops)
                                    checkFurther = true;
                                if(checkFurther) {
                                    for(AbstractPTAExpression u : tt.update)
                                        if(moved.contains(u.target)) {
                                            openEnd = true;
                                            break;
                                        }
                                    if(!openEnd)
                                        safeJumps.add(tt);
                                }
                            } else if((tt.guard != null && !tt.guard.backendHasStop(r)) ||
                                    (tt.probability != null && !tt.probability.backendHasStop(r))) {
                                safeJumps.add(tt);
                                checkFurther = true;
                            } else if(tt.guard == null && tt.probability == null) {
                                // no witness at this transition of what is really happening
                                safeJumps.add(tt);
                                checkFurther = true;
                            }
                            if(!openEnd) {
                                if(checkFurther)
                                    if(onlyImmediate && !tt.targetState.isHidden(implicitHidden))
                                        openEnd = true;
                                    else {
                                        if(!nest(update, tt.targetState, start, visited, expressions,
                                                safeJumps, sideJumps, onlyImmediate, traced,
                                                strictFieldAccess, implicitHidden))
                                            openEnd = true;
                                    }
                                if(!openEnd) {
                                    if(tt.guard != null)
                                        expressions.add(new ExpressionWithTransition(tt.guard, tt));
                                    if(tt.probability != null)
                                        expressions.add(new ExpressionWithTransition(tt.probability, tt));
                                    for(AbstractPTAExpression u : tt.update) {
                                        expressions.add(new ExpressionWithTransition(u, tt));
                                        if(u.backendHasStop(r) ||
                                                // check if this is not a transport by the same variable,
                                                // but already of a new value
                                                u.target.equals(r))
                                            break;
                                    }
                                    if(checkFurther || tt.getSources().contains(update.target))
                                        sideJumps.addAll(localSideJumps);
                                } else
                                    break;
                            } else
                                break;
                        }
            } else
                openEnd = true;
        } else
            openEnd = true;
        return !openEnd;
    }
    /**
     * If fields would be accessed using a given set of variables.
     * 
     * @param pta pta to optimize
     * @param traced variables traced for stops
     * @param writtenElements element variables that are written to at
     * least a single time within al PTAs; if known, and if the backend uses
     * element variables instead of arrays, otherwise null; used to
     * determine, if array read is a non--local access or only a read of
     * a constant
     * @return if any field would be accessed
     */
    private static boolean fieldsAccessed(Set<AbstractPTAVariable> used,
            Set<PTAScalarVariable> traced, Set<PTAElementVariable> writtenElements) {
        if(writtenElements != null) {
            // screen for read--only arrays, as they are effectively
            // a set of constants
            for(AbstractPTAVariable v : new HashSet<>(used))
                if(v instanceof PTAArrayVariable ||
                        v instanceof PTAElementVariable) {
                    PTAArrayVariable a;
                    if(v instanceof PTAArrayVariable)
                        a = (PTAArrayVariable)v;
                    else
                        a = ((PTAElementVariable)v).array;
                    boolean isVariable = false;
                    for(PTAElementVariable w : writtenElements)
                        if(w.array.equals(a)) {
                            isVariable = true;
                            break;
                        }
                    if(!isVariable)
                        // not a field access, but only read of a constant
                        used.remove(v);
                }
        }
        return !traced.containsAll(used);
    }
    /**
     * Moves along transitions, beginning with a given one, and
     * until (i) an already visited start is reached, (ii) a stop or
     * a fork is found, or (iii) a start <i>i</i> with more than a single input
     * is found. If (iii) occured before (i) or (ii), and only a single update <i>u</i>
     * has been found along the whole pathway, including mid--start properties,
     * and nothing other but possibly a guard at <code>start</code>, then branch
     * description containing <i>i</i>  and <i>u</i> is returned. If any other case,
     * null is returned.
     * 
     * @param start a transition to begin the move
     * @return branch description or null
     */
    protected static BranchDescription analyzeBranch(PTATransition start) {
        if(start.guard == null)
            throw new RuntimeException("guard expected");
        PTATransition curr = start;
        Collection<AbstractPTAExpression> updates = new LinkedList<>();
        Set<PTAState> visited = new HashSet<>();
        visited.add(start.sourceState);
        List<PTAState> inside = new LinkedList<>();
        while(true) {
            if(curr != start && curr.guard != null)
                return null;
            if(curr.label != null || curr.clockLimitLow != null ||
                    curr.probability != null)
                return null;
            updates.addAll(curr.update);
            if(updates.size() > 1)
                return null;
            if(visited.contains(curr.targetState))
                return null;
            if(curr.targetState.getInputsNum() > 1)
                return new BranchDescription(start,
                        updates.isEmpty() ? null : updates.iterator().next(),
                        curr.targetState, inside);
            else if(curr.targetState.getOutputsNum() != 1)
                return null;
            else {
                if(curr != start &&
                        curr.sourceState.clockLimitHigh != null)
                    return null;
                visited.add(curr.targetState);
                inside.add(curr.targetState);
                curr = curr.targetState.getSingleOutput();
            }
        }
    }
    /**
     * Attempt to reduce conditional branches into nested conditional
     * expressions. This is a part of optimizations made by
     * <code>nestExpressions()</code>.<br>
     * 
     * This method can be called by PTA modification that are formula--sensitive,
     * see <code>PTA.testFormulas()</code> for details.
     * 
     * @param pta pta to optimize
     * @param traced variables traced for stops
     * @param writtenElements passed to <code>fieldsAccessed()</code>,
     * see that method for details
     * @param strictFieldAccess defined as in
     * <code>TADDOptions.strictFieldAccess</code>
     * @param implicitHidden passed to <code>PTAState.isHidden()</code>
     * @return if <code>pta</code> has been modified by this
     * method
     */
    protected static boolean nestConditionalBranches(PTA pta,
            Set<PTAScalarVariable> traced, Set<PTAElementVariable> writtenElements,
            boolean strictFieldAccess, boolean implicitHidden) {
        boolean stable = true;
        for(PTAState state : new LinkedList<>(pta.states.values()))
            if(pta.states.keySet().contains(state.index) &&
                    state.qualifiesForBranchId(BranchId.Type.CONDITIONAL,
                        false, true) &&
                    state.getOutputsNum() == 2) {
                PTAState target = null;
                BranchDescription b1 = null;
                BranchDescription b2 = null;
                boolean failed = false;
                SCAN:
                for(int inputIndices : state.output.keySet())
                    for(PTATransition tt : state.output.get(inputIndices)) {
                        BranchDescription b = analyzeBranch(tt);
                        if(b == null) {
                            failed = true;
                            break SCAN;
                        }
                        if(target == null) {
                            target = b.target;
                            b1 = b;
                        } else if(target != b.target) {
                            failed = true;
                            break SCAN;
                        } else
                            b2 = b;
                    }
                if(!failed) {
                    if(b1.update == null || b2.update == null) {
                        BranchDescription preserved;
                        BranchDescription discarded;
                        AbstractPTAExpression update;
                        if(b1.update != null) {
                            preserved = b1;
                            discarded = b2;
                            update = b1.update;
                        } else {
                            preserved = b2;
                            discarded = b1;
                            update = b2.update;
                        }
                        // accept exactly a single update within the
                        // two branches
                        if(update != null && update.isNestable()) {
                            PTATransition start = preserved.start;
                            Set<AbstractPTAVariable> all = start.getSources();
                            all.addAll(update.getVariables());
                            // states to remove
                            Set<PTAState> allInside = new HashSet<>(
                                    preserved.inside);
                            allInside.addAll(discarded.inside);
                            boolean cancelModification = false;
                            if(fieldsAccessed(all, traced, writtenElements)) {
                                for(PTAState s : allInside)
                                    if(!s.isHidden(implicitHidden)) {
                                        cancelModification = true;
                                        break;
                                    }
                            }
                            for(PTAState s : allInside)
                                if(pta.inStateFormula(s)) {
                                    cancelModification = true;
                                    break;
                                }
                            if(!cancelModification) {
                                for(PTAState s : allInside) {
                                    if(s.getOutputsNum() != 1)
                                        throw new RuntimeException("unexpected fork");
                                    start.backendOps.addAll(s.getSingleOutput().backendOps);
                                }
                                preserved.remove(pta);
                                discarded.remove(pta);
                                PTANestedExpression branch = new PTANestedExpression(
                                        start.guard, update);
                                start.guard = null;
                                start.update.clear();
                                start.update.add(branch);
                                start.targetState = target;
// if(branch.leftExpr.toString().equals("outA1<<-70, 70>>(<<0>>)!=outA2<<-70, 70>>(<<0>>)"))
//     branch.toString();
                                pta.addTransition(start);
                                stable = false;
                            }
                        }
                    }
                }
            }
        return !stable;
    }
    /**
     * Attempts to nest expressions.<br>
     * 
     * This PTA modification is state formula--sensitive, see
     * <code>PTA.testFormulas()</code> for details.
     * 
     * @param pta pta to optimize
     * @param traced variables traced for stops
     * @param writtenElements passed to <code>fieldsAccessed()</code>,
     * see that method for details
     * @param strictFieldAccess defined as in
     * <code>TADDOptions.strictFieldAccess</code>
     * @param implicitHidden passed to <code>PTAState.isHidden()</code>
     * @param rangeChecking if range checking is enabled, so that updates
     * with source--level ranges can not be removed
     * @return if <code>pta</code> has been modified by this
     * method
     */
    public static boolean nestExpressions(PTA pta, Set<PTAScalarVariable> traced,
            Set<PTAElementVariable> writtenElements, boolean strictFieldAccess,
            boolean implicitHidden, boolean rangeChecking) {
        // new transitions added by this method are resets, and should not
        // be tested for being nested
        List<PTATransition> origTransitions = new LinkedList<>(pta.transitions);
        boolean totalStable = true;
        boolean stable;
        List<PTATransition> resets = new ArrayList();
        do {
            stable = true;
            for(PTATransition in : origTransitions)
                for(AbstractPTAExpression u : new LinkedList<>(in.update)) {
//if(u.target.toString().indexOf("c12") != -1)
//    u = u;
                    List<ExpressionWithTransition> expressions =
                            new LinkedList<>();
                    boolean onlyImmediate = false;
                    for(AbstractPTAVariable v : u.getSources())
                        if(!(v instanceof PTAScalarVariable) ||
                                !traced.contains((PTAScalarVariable)v)) {
                            onlyImmediate = true;
                            break;
                        }
                    
//if(pta.name.indexOf("House") != -1 && u.target.toString().indexOf("c17") != -1)
//    u = u;
                    Set<PTATransition> visited = new HashSet<>();
                    visited.add(in);
                    Set<PTATransition> safeJumps = new HashSet<>(visited);
                    Set<PTATransition> sideJumps = new HashSet<>();
//if(u.toString().indexOf("-8)*2") != -1)
//    pta = pta;
                    if(
                            // the update to propagate is considered as a valid part of the
                            // region, with nothing needed to be nested into it, so it can
                            // not contain its target, as it would possibly mean, that the
                            // update should have nested itself
                            !u.getSources().contains(u.target) &&
                            nest(u, in.targetState, pta.startState, visited, expressions,
                                safeJumps, sideJumps, onlyImmediate, traced,
                                strictFieldAccess, implicitHidden)) {
                        boolean externalJump = false;
                        for(PTATransition tt : sideJumps)
                            if(!safeJumps.contains(tt)) {
                                externalJump = true;
                                break;
                            }
                        if(!externalJump) {
                            boolean updatePossible = true;
                            boolean stateFormulaFound = false;
                            // do not check states at the edges of the
                            // propagation region
                            for(PTATransition tt : safeJumps)
                                if((tt != in && pta.inStateFormula(tt.sourceState)) ||
                                        pta.inStateFormula(tt.targetState)) {
                                    stateFormulaFound = true;
                                    break;
                                }
                            if(stateFormulaFound) {
                                // propagation may tresspass a state, to which points
                                // a formula -- check if anything is propagated at all,
                                // and if yes, prevent the update
                                for(ExpressionWithTransition et : expressions)
                                    if(et.expr.getSources().contains(u.target)) {
                                        updatePossible = false;
                                        break;
                                    }
                            }
                            if(updatePossible && onlyImmediate)
                                for(ExpressionWithTransition et : expressions) {
                                    PTATransition tt = et.x;
                                    // perform the field access order check only if
                                    // at least a single start along the propagation path is not
                                    // hidden -- if <code>onlyImmediate</code> is true,
                                    // then it can only be the root start
                                    if(!in.targetState.isHidden(implicitHidden)) {
                                        // ensure, that if only immediate propagation is possible,
                                        // then the modified expressions do not contain
                                        // non--traced variables i. e. non--locals
                                        Set<AbstractPTAVariable> used = tt.getUsed();
                                        if(fieldsAccessed(used, traced, writtenElements)) {
                                            updatePossible = false;
                                            break;
                                        }
                                    }
                                }
                            if(updatePossible) {
                                Set<PTATransition> opsAdded = new HashSet<>();
                                Set<AbstractPTAVariable> updateSources = u.getSources();
                                for(ExpressionWithTransition et : expressions) {
                                    PTATransition tt = et.x;
                                    // the sources of <code>u</code> may now be needed
                                    // elsewhere in the propagation region, so remove their
                                    // stops in the region, but its leaves
                                    for(AbstractPTAVariable s : updateSources)
                                        if(safeJumps.contains(tt))
                                            et.expr.backendRemoveStop(s);
                                    if(!opsAdded.contains(tt) &&
                                            in.backendOps != null && tt.backendOps != null) {
                                        Set<AbstractPTAVariable> sources = tt.getSources();
                                        if(sources.contains(u.target))
                                            tt.backendOps.addAll(0, in.backendOps);
                                        opsAdded.add(tt);
                                    }
                                    if(tt.getSources().contains(u.target)) {
                                        PTA.TransitionBag xbag = pta.keyBefore(tt);
                                        if(tt.guard == et.expr)
                                            tt.guard = replace(tt.guard, u);
                                        else if(tt.probability == et.expr)
                                            tt.probability = replace(tt.probability, u);
                                        else {
                                            List<AbstractPTAExpression> newUpdate = new LinkedList<>();
                                            for(AbstractPTAExpression v : tt.update)
                                                if(v == et.expr)
                                                    newUpdate.add(replace(v, u));
                                                else
                                                    newUpdate.add(v);
                                            tt.update = newUpdate;
                                        }
                                        pta.keyAfter(xbag);
                                        stable = false;
                                    }
                                }
                                if(!rangeChecking || !u.target.range.backendIsSourceLevel ||
                                        u.target.range.backendGuaranteed) {
                                    // the update is no more needed
                                    PTA.TransitionBag xbag = pta.keyBefore(in);
                                    in.update.remove(u);
                                    pta.keyAfter(xbag);
                                    stable = false;
                                } else if( // if <code>u</code> has already been reset, do not
                                               // put another resetting transition
                                            in.targetState.getOutputsNum() != 1 ||
                                            !resets.contains(in.targetState.getSingleOutput())) {
                                    // leave the update for range checking, but reset it then immediately
                                    //
                                    // if the update's source range were not restrictive as defined
                                    // in <code>CodeVariable.hasRestrictiveSourceLevelRange()</code>,
                                    // then perhaps it would already be non--existant
                                    //
                                    // anyway, leaving it will be a harmless false positive at worst
                                    PTAState resetState = new PTAState(pta.states.lastKey() + 1);
                                    pta.states.put(resetState.index, resetState);
                                    PTATransition reset = new PTATransition(null);
                                    resets.add(reset);
                                    reset.backendOpIndex = in.backendOpIndex;
                                    AbstractPTAExpression resetUpdate = new PTAAssignment(
                                            u.target, new PTAConstant(0.0, u.target.floating));
                                    resetUpdate.backendComment = "reset";
                                    if(u.backendStopEnabled()) {
                                        resetUpdate.backendCopyStop(u);
                                        u.backendRemoveStop(u.target);
                                    }
                                    reset.update.add(resetUpdate);
                                    reset.sourceState = resetState;
                                    reset.targetState = in.targetState;
                                    reset.connectSourceIndex = reset.sourceState.index;
                                    reset.connectTargetIndex = reset.targetState.index;
                                    SortedSet<PTATransition> set = new TreeSet<>();
                                    set.add(reset);
                                    reset.sourceState.output.put(reset.targetState.index, set);
                                    set = new TreeSet<>();
                                    set.add(reset);
                                    reset.targetState.input.put(reset.sourceState.index, set);
                                    pta.addToLists(reset);
                                    pta.removeFromLists(in);
                                    if(!in.sourceState.output.get(in.targetState.index).remove(in))
                                        throw new RuntimeException("transition not found");
                                    if(in.sourceState.output.get(in.targetState.index).isEmpty())
                                        in.sourceState.output.remove(in.targetState.index);
                                    if(!in.targetState.input.get(in.sourceState.index).remove(in))
                                        throw new RuntimeException("transition not found");
                                    if(in.targetState.input.get(in.sourceState.index).isEmpty())
                                        in.targetState.input.remove(in.sourceState.index);
                                    in.targetState = resetState;
                                    set = new TreeSet<>();
                                    set.add(in);
                                    in.sourceState.output.put(in.targetState.index, set);
                                    set = new TreeSet<>();
                                    set.add(in);
                                    in.targetState.input.put(in.sourceState.index, set);
                                    in.connectTargetIndex = in.targetState.index;
                                    pta.addToLists(in);
                                    stable = false;
                                }
                            }
                        }
                    }
                }
            if(!stable)
                totalStable = false;
        } while(!stable);
        if(nestConditionalBranches(pta, traced, writtenElements,
                strictFieldAccess, implicitHidden))
            totalStable = false;
        return !totalStable;
    }
    /**
     * Merges a series of two transitions containing
     * (1st) only a synchronisation, (2nd) only updates containing
     * only variables which are not visible outside this PTA.
     * 
     * This PTA modification is state formula--sensitive, see
     * <code>PTA.testFormulas()</code> for details.
     *
     * @param tadd                      PTA to have its sync/local update
     * pairs merged
     * @param startState                current start state
     * @return                          if changed
     */
    public static boolean compactEmptySyncLocal(PTA tadd) {
        boolean stable = true;
        for(PTAState tState : new LinkedList<>(tadd.states.values())) {
            if(tadd.states.size() == 1)
                // only one state is left, do not delete it even if it
                // would be a redundant one
                break;
            if(tState.getOutputsNum() == 1) {
                PTATransition x1 = tState.getSingleOutput();
                PTALabel tmpLabel = x1.label;
                x1.label = null;
                boolean empty = x1.isEmpty();
                x1.label = tmpLabel;
                PTAState target = x1.targetState;
                if(x1.label != null && empty && tState.index != target.index &&
                            !tadd.inStateFormula(target)) {
                    if(target.getOutputsNum() == 1) {
                        PTATransition x2 = target.getSingleOutput();
                        List<AbstractPTAExpression> tmpUpdate = x2.update;
                        x2.update = null;
                        empty = x2.isEmpty();
                        x2.update = tmpUpdate;
                        if(empty) {
                            x1.update = x2.update;
                            x2.update = null;
                        }
                    }
                }
            }
        }
        return !stable;
    }
    /**
     * If the probability is a constant, transforms it as in the definition
     * of <code>PROB_EPSILON</code>. A variable value is left unchanged.
     * 
     * @param p probability
     * @return transformed probability
     */
    private static AbstractPTAValue quantiseProb(AbstractPTAValue p) {
        if(p instanceof PTAConstant) {
            PTAConstant c = (PTAConstant)p;
            if(c.value >= 1.0 - PROB_EPSILON)
                c = new PTAConstant(1.0, true);
            else if(c.value <= PROB_EPSILON)
                c = new PTAConstant(0.0, true);
            return c;
        } else
            return p;
    }
    /**
     * Compacts purely probabilistic branches.
     * 
     * @param pta pta to simplify
     * @param local local variables of this PTA
     * @param implicitHidden passed to <code>PTAState.isHidden()</code>
     * @return if the PTA has been modified
     */
    public static boolean compactProbabilisticBranches(PTA pta) {
        boolean stable = true;
        boolean localStable;
        do {
            localStable = true;
            for(PTAState state : new LinkedList<>(pta.states.values())) {
                if(!pta.states.values().contains(state))
                    continue;
                List<PTATransition> tts = new LinkedList<>();
                for(Set<PTATransition> set : new LinkedList<>(state.output.values()))
                    for(PTATransition tt : new LinkedList<>(set))
                        tts.add(tt);
                for(PTATransition tt : tts) {
                    if(tt.guard != null ||
                            tt.clockLimitLow != null ||
                            tt.label != null ||
                            tt.branchId == null || tt.branchId.type != BranchId.Type.PROBABILISTIC ||
                            !tt.update.isEmpty() ||
                            tt.isSpecial())
                        continue;
                    AbstractPTAValue probSimple = tt.probability.getConstant();
                    if(probSimple == null) {
                            if(tt.probability instanceof PTAUnaryExpression) {
                                PTAUnaryExpression ue = (PTAUnaryExpression)tt.probability;
                                if(ue.operator == AbstractCodeOp.Op.NONE &&
                                        ue.sub instanceof PTAScalarVariable)
                                    probSimple = ue.sub;
                            }
                    }
                    PTAState tState = tt.targetState;
                    if(tState == pta.startState)
                        continue;
                    if(tState.getInputsNum() == 1 && tState.index != state.index &&
                            !pta.inStateFormula(tState)) {
                        int count = 0;
                        boolean invalid = false;
                        boolean probabilistic = true;
                        for(Set<PTATransition> tSet : tState.output.values()) {
                            for(PTATransition tTt : tSet) {
                                if(tTt.guard != null ||
                                        tTt.clockLimitLow != null ||
                                        tTt.label != null)
                                    invalid = true;
                                if(tTt.probability != null && tTt.probability.getConstant() == null)
                                    // can not be merged
                                    invalid = true;
                                if(probSimple == null && tTt.probability != null)
                                    // two such probs can not be merged into one expression
                                    invalid = true;
                                if(tTt.branchId == null || tTt.branchId.type != BranchId.Type.PROBABILISTIC)
                                    probabilistic = false;
                                ++count;
                            }
                        }
                        if(!probabilistic && count > 1)
                            // do not nest non--deterministic branches
                            invalid = true;
                        if(!invalid) {
                            if(count > 1)
                                count = count;
                            PTA.StateBag sbag = pta.keyBefore(state);
                            state.output.get(tState.index).clear();
                            for(Set<PTATransition> tSet : new LinkedList<>(tState.output.values())) {
                                for(PTATransition x : new LinkedList<>(tSet)) {
                                    PTA.TransitionBag xbag = pta.keyBefore(x);
                                    // no more used
                                    xbag.inputSet = null;
                                    xbag.outputSet = null;
                                    x.sourceState = state;
                                    x.ndInt = tt.ndInt;
                                    x.branchId = tt.branchId;
                                    if(x.probability == null)
                                        x.probability = tt.probability;
                                    else {
                                        PTAConstant a = (PTAConstant)quantiseProb(x.probability.getConstant());
                                        AbstractPTAValue b = quantiseProb(probSimple);
                                        AbstractPTAExpression p;
                                        if(a.value == 1.0)
                                            p = new PTAUnaryExpression(b, CodeOpBinaryExpression.Op.NONE);
                                        else if(b instanceof PTAConstant && ((PTAConstant)b).value == 1.0)
                                            p = new PTAUnaryExpression(a, CodeOpBinaryExpression.Op.NONE);
                                        else
                                            p = new PTABinaryExpression(a, b,
                                                     CodeOpBinaryExpression.Op.BINARY_MULTIPLY);
                                        p.backendCopyStop(x.probability);
                                        x.probability = p;
                                    }
                                    state.addOutputTransition(x);
                                    pta.keyAfter(xbag);
                                    x.targetState.input.remove(tState.index);
//                                    done in addOutputTransition()
//                                    SortedSet<PTATransition> setO = x.targetState.input.get(state.index);
//                                    if(setO == null) {
//                                        setO = new TreeSet<>();
//                                        x.targetState.input.put(state.index, setO);
//                                    }
//                                    setO.add(x);
                                    state.output.remove(tState.index);
                                    pta.removeFromLists(tt);
                                }
                            }
                            pta.states.remove(tState.index);
                            pta.keyAfter(sbag);
                            stable = false;
                            localStable = false;
                        }
                    }
                }
            }
        } while(!localStable);
        return !stable;
    }
    /**
     * Deletes zero probability transitions, probabilities equal to 1.
     * Groups of transitions that do nothing but sum to a given probability
     * are replaced by a single equivalent transition.
     * 
     * @param pta pta to simplify
     * @param local local variables of this PTA
     * @param implicitHidden passed to <code>PTAState.isHidden()</code>
     * @return if the PTA has been modified
     */
    public static boolean removeZeroProbabilisticBranches(PTA pta) {
        boolean stable = true;
        for(PTAState state : new LinkedList<>(pta.states.values())) {
            if(!pta.states.values().contains(state))
                continue;
            List<PTATransition> tts = new LinkedList<>();
            for(Set<PTATransition> set : new LinkedList<>(state.output.values()))
                for(PTATransition tt : new LinkedList<>(set))
                    tts.add(tt);
            // merge candidates, key is target state
            Map<Integer, SortedSet<PTATransition>> doNothing = new HashMap<>();
            for(PTATransition tt : tts) {
                if(tt.clockLimitLow != null ||
                        tt.label != null ||
                        tt.branchId == null || tt.branchId.type != BranchId.Type.PROBABILISTIC ||
                        tt.isSpecial())
                    continue;
                if(tt.guard != null)
                    throw new RuntimeException("unexpected: both probability and guard");
                boolean altered = false;
                PTAConstant probSimple = tt.probability.getConstant();
                if(probSimple != null) {
                    if(probSimple.value <= PROB_EPSILON) {
                        // remove the transition
                        pta.removeTransition(tt);
                        altered = true;
                    }
                    if(probSimple.value >= 1.0 - PROB_EPSILON) {
                        PTA.TransitionBag tb = pta.keyBefore(tt);
                        tt.probability = null;
                        tt.ndInt = null;
                        tt.branchId = null;
                        pta.keyAfter(tb);
                        altered = true;
                    }
                    if(!altered && tt.update.isEmpty()) {
                        int target = tt.targetState.index;
                        SortedSet<PTATransition> set = doNothing.get(target);
                        if(set == null) {
                            set = new TreeSet<>();
                            doNothing.put(target, set);
                        }
                        set.add(tt);
                    }
                }
                if(altered)
                    stable = false;
            }
            for(int target : doNothing.keySet()) {
                SortedSet<PTATransition> parallel = doNothing.get(target);
                int num = parallel.size();
                if(num > 1) {
                    double sum = 0.0;
                    // merge
                    List<AbstractCodeOp> ops = new LinkedList<>();
                    int count = 0;
                    for(PTATransition tt : parallel) {
                        sum += tt.probability.getConstant().value;
                        if(count == num - 1) {
                            PTA.TransitionBag tb = pta.keyBefore(tt);
                            if(sum >= 1.0 - PROB_EPSILON) {
                                tt.probability = null;
                                tt.ndInt = null;
                                tt.branchId = null;
                            } else {
                                tt.probability = new PTAUnaryExpression(new PTAConstant(sum, true),
                                    CodeOpUnaryExpression.Op.NONE);
                            }
                            tt.backendOps.addAll(0, ops);
                            pta.keyAfter(tb);
                        } else {
                            if(tt.backendOps != null)
                                ops.addAll(tt.backendOps);
                            pta.removeTransition(tt);
                        }
                        ++count;
                    }
                    stable = false;
                }
            }
        }
        return !stable;
    }
    /**
     * Merges a series of two transitions being
     * (1st) only special. It canm move state formulas.
     * 
     * This PTA modification is state formula--sensitive, see
     * <code>PTA.testFormulas()</code> for details.
     *
     * @param tadd                      PTA to have its sync/local update
     * pairs merged
     * @param startState                current start state
     * @return                          if changed
     */
    public static boolean compactSpecial(PTA tadd, boolean implicitHidden) {
        boolean stable = true;
        for(PTAState tState : new LinkedList<>(tadd.states.values())) {
            if(tadd.states.size() == 1)
                // only one state is left, do not delete it even if it
                // would be a redundant one
                break;
            if(tState.getOutputsNum() == 1 && tState.clockLimitHigh == null) {
                PTATransition x1 = tState.getSingleOutput();
                List<AbstractCodeOp> bo = x1.backendOps;
                x1.backendOps = new LinkedList<>();
                boolean empty = x1.isEmpty();
                x1.backendOps = bo;
                PTAState target = x1.targetState;
                if(empty && tState.index != target.index &&
                            !tadd.inStateFormula(target)) {
                    for(PTATransition x2 : target.getFlatOutput()) {
                        PTA.TransitionBag xbag = tadd.keyBefore(x2);
                        x2.backendOps.addAll(x1.backendOps);
                        if(x2.backendOpIndex == -1)
                            x2.backendOpIndex = x1.backendOpIndex;
                        tadd.keyAfter(xbag);
                    }
                    PTA.TransitionBag xbag = tadd.keyBefore(x1);
                    x1.backendOps.clear(); 
                    x1.backendOpIndex = -1;
                    for(PTATransition x : new LinkedList<>(tadd.transitions))
                        if(x.targetState == tState) {
                            PTA.TransitionBag xbag2 = tadd.keyBefore(x);
                            x.targetState = x1.targetState;
                            x.sourceState.output.remove(tState.index);
                            x1.targetState.input.remove(tState.index);
                            x.sourceState.addOutputTransition(x);
                            tadd.keyAfter(xbag2);
                            for(StateFormula f : tadd.stateFormulas)
                                for(PTAState s : new TreeSet<>(f.states))
                                    if(s == tState) {
                                        f.states.remove(s);
                                        f.states.add(x1.targetState);
                                        break;
                                    }
                        }
                    tadd.keyAfter(xbag);
                    stable = false;
                }
            }
        }
        if(!stable)
            removeRedundant(tadd);
        return !stable;
    }
    /**
     * Deletes redundant states. A state <i>i</i> is redundant if its stopped
     * output transitions do nothing and stopped point to the same target state,
     * which is not <i>i</i>. Transitions pointing to a deleted state are
     * updated as necessary.<br>
     * 
     * This PTA modification is state formula--sensitive, see
     * <code>PTA.testFormulas()</code> for details.
     *
     * @param tadd PTA to have its redundant states deleted
     * @param startState current start state
     * @return null if nothing changed; otherwise start state after optimizations, may
     * change is the start state is redundant; <code>tadd</code> is not updated with
     * a new start state, must be done outside
     */
    protected static PTAState deleteRedundantStates(PTA tadd, PTAState startState) {
        /*Map<PTATransition, PTATransition> */
        boolean changed = false;
        for(PTAState tState : new LinkedList<>(tadd.states.values())) {
            if(tadd.states.size() == 1)
                // only one state is left, do not delete it even if it
                // would be a redundant one
                break;
            boolean empty = true;
            Set<PTAState> targets = new HashSet<>();
            for(Set<PTATransition> set : tState.output.values())
                for(PTATransition tt : set) {
                    targets.add(tt.targetState);
                    if(!tt.isEmpty()) {
                        empty = false;
                        break;
                    }
                }
            if(targets.isEmpty() && !tadd.inStateFormula(tState)) {
                
            } else if(empty && targets.size() == 1 &&
                        tState.index != targets.iterator().next().index) {
                // the transitions do nothing and stopped point to the same state
                // which is not the tested one
                PTAState target = targets.iterator().next();
                if(tadd.inStateFormula(tState))
                    tadd.modifyStateFormulas(tState, target);
                for(PTATransition tt : new LinkedList<>(tadd.transitions))
                    if(tt.targetState == tState) {
                        SortedSet<PTATransition> set = tt.sourceState.output.get(tt.targetState.index);
                        String key1 = tt.getKey();
                        if(!set.remove(tt))
                            throw new RuntimeException("transition expected");
                        if(set.isEmpty())
                            tt.sourceState.output.remove(tt.targetState.index);
                        String key2 = tt.getKey();
                        if(!key1.equals(key2))
                            key1 = key1;
                        PTA.TransitionBag xbag = tadd.keyBefore(tt);
                        tt.connectTargetIndex = target.index;
                        tt.targetState = target;
                        tt.sourceState.addOutputTransition(tt);
                        tadd.keyAfter(xbag);
                        /*
                        SortedSet<PTATransition> set = target.input.get(tt.connectSourceIndex);
                        if(set == null) {
                            set = new TreeSet<>();
                            target.input.put(tt.connectSourceIndex, set);
                        }
                        set.add(tt);
                        */
                    }
                if(startState == tState)
                    startState = target;
                for(Set<PTATransition> set : tState.output.values())
                    for(PTATransition tt : set) {
                        tadd.transitions.remove(tt);
                    }
                // SortedSet<PTATransition> prevInputSet = target.input.get(tState.index);
                // prevInputSet.clear();
                target.input.remove(tState.index);
                tadd.states.values().remove(tState);
                changed = true;
            }
        }
        if(changed)
            return startState;
        else
            return null;
    }
    /**
     * Deletes recursively redundant states by calling
     * <code>deleteRedundantStates()</code>.
     * 
     * This PTA modification is state formula--sensitive, see
     * <code>PTA.testFormulas()</code> for details.
     *
     * @param tadd                      PTA to have its redundant states
     *                                  deleted
     */
    public static void deleteRedundantStatesRecursive(PTA pta) {
        PTAState s;
        do {
            s = deleteRedundantStates(pta, pta.startState);
            if(s != null)
                pta.startState = s;
        } while(s != null);
    }
    /**
     * Deletes iteratively dead states. A state is redundant if it is not
     * the start state and no transitions points to it.<br>
     * 
     * Does not require the fields <code>PTATransition.connectSourceIndex</code>
     * and <code>PTATransition.connectTargetIndex</code> to be valid, so
     * can be used from inside the optimizer.
     *
     * @param tadd                      PTA to have its dead states
     *                                  deleted
     * @param startState                start state
     * @return                          if any state has been deleted
     */
    public static boolean deleteDeadStates(PTA tadd) {
        boolean modified = false;
        boolean stable;
        do {
            stable = true;
            for(PTAState tState : new LinkedList<>(tadd.states.values())) {
                if(tadd.states.size() == 1)
                    // only one state is left, do not delete it even if it
                    // would be a redundant one
                    break;
                if(tState == tadd.startState)
                    continue;
                boolean sourcesExist = false;
                SOURCE_STATE_SCAN:
                for(PTAState sState : tadd.states.values())
                    // a dead state can have a loop to itself
                    if(sState != tState)
                        for(Set<PTATransition> set : sState.output.values())
                            for(PTATransition tt : set) {
                                if(tt.targetState == tState) {
                                    sourcesExist = true;
                                    break SOURCE_STATE_SCAN;
                                }
                            }
                if(!sourcesExist) {
                    for(Set<PTATransition> set : tState.output.values())
                        for(PTATransition tt : set) {
                            //Set<PTATransition> inputSet = tt.targetState.input.get(tt.connectSourceIndex);
                            //inputSet.clear();
                            tt.targetState.input.remove(tt.connectSourceIndex);
                            tadd.transitions.remove(tt);
                        }
                    tadd.states.values().remove(tState);
                    stable = false;
                    modified = true;
                }
            }
        } while(!stable);
        return modified;
    }
    /**
     * A fast optimisation which removes dead and redundant states.
     * 
     * @param pta pta to modify
     */
    public static void removeRedundant(PTA pta) {
        deleteDeadStates(pta);
        deleteRedundantStatesRecursive(pta);
    }
}
