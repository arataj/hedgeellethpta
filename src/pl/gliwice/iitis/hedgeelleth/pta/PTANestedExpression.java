/*
 * PTANestedExpression.java
 *
 * Created on Oct 29, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.pta.values.*;

/**
 * An expression that can recursively nest other expressions.<br>
 * 
 * Used only to generate more optimal Prism files. Can not be parsed by
 * <code>PTAParser</code>.<br>
 * 
 * Due to its limited application, it does not support the following methods of
 * <code>AbstractPTAEXpression</code>:
 * <code>newComplementary()</code>, <code>isEquivalentCondition()</code>,
 * <code>isConditional()</code>, <code>probability()</code>,
 * <code>trueRanges()</code>, <code>evaluate()</code>. Also, stops of
 * subexpressions of this expression do not have usable values.
 * 
 * @author Artur Rataj
 */
public class PTANestedExpression extends AbstractPTAExpression {
    /**
     * A fake variable, meaning nested in a super expression, which is of the type
     * <code>PTANestedExpression</code>.
     */
    public static final AbstractPTAVariable SUPER_NESTED;
    static {
        try {
            SUPER_NESTED =  new PTAScalarVariable(
                "#SUPER_NESTED", new PTARange(null, 0, 0), false);
        } catch(PTAException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    /**
     * Left or single sub--expression, null for none.
     */
    public PTANestedExpression leftExpr;
    /**
     * Left value, if any. Not null only if <code>leftExpr</code> is null.
     */
    public AbstractPTAValue leftValue;
    /**
     * Right sub--expression, null for none. Always null for unary operators.
     */
    public PTANestedExpression rightExpr;
    /**
     * Right value, if any. Always null for unary operators. Possibly not null
     * only if <code>rightExpr</code> is null.
     */
    public AbstractPTAValue rightValue;
    /**
     * Topmost operator, unary or binary. <code>NESTED_INDEX</code>
     * is a binary expression, indexed variable on the left, index on the right;
     * <code>BRANCH</code> means: perform the update at the right
     * only if the guard at the left is true.
     */
    public AbstractCodeOp.Op topOperator;
    /**
     * If the range of values of this expression is known, then represents
     * that range. Otherwise null. Min and max are always inclusive. Not used
     * outside the backend.
     */
    public PTARange backendReadRange;

    /**
     * Creates a new nested expression, from a unary expression.
     * 
     * @param b unnary expression, not nested
     */
    public PTANestedExpression(PTAUnaryExpression u) {
        super();
        if(u.operator == AbstractCodeOp.Op.NONE &&
                u.sub instanceof PTAElementVariable) {
            init((PTAElementVariable)u.sub);
            init(u);
        } else {
            init(u);
            topOperator = u.operator;
            set(u.sub, true);
        }
    }
    /**
     * Creates a new nested expression, from a binary expression.
     * 
     * @param b binary expression, not nested
     */
    public PTANestedExpression(PTABinaryExpression b) {
        super();
        init(b);
        topOperator = b.operator;
        set(b.left, true);
        set(b.right, false);
    }
    /**
     * Creates a new nested expression, being a branch.<br>
     * 
     * A branch is modelled using a conditional operator, and
     * thus needs to know its target, so it can not be nested.
     * 
     * @param guard guard
     * @param update branch, executed only if <code>guard</code>
     * is true
     */
    public PTANestedExpression(AbstractPTAExpression guard,
            AbstractPTAExpression update) {
        super();
        init(update);
        topOperator = AbstractCodeOp.Op.BRANCH;
        leftExpr = newTopExpression(guard);
        rightExpr = newTopExpression(update);
    }
    /**
     * Creates a nested expression being an equivalent of an
     * element variable. Return value is set to null.
     * 
     * @param element element variable
     */
    public PTANestedExpression(PTAElementVariable element) {
        super();
        init(element);
    }
    /**
     * Creates a nested expression. This is a copying constructor.
     * The copy is shallow, except for sub--nested expressions.
     * 
     * @param expr original expression
     */
    public PTANestedExpression(PTANestedExpression expr) {
        super();
        init(expr);
        leftValue = expr.leftValue;
        if(expr.leftExpr != null)
            leftExpr = new PTANestedExpression(expr.leftExpr);
        else
            leftExpr = expr.leftExpr;
        rightValue = expr.rightValue;
        if(expr.rightExpr != null)
            rightExpr = new PTANestedExpression(expr.rightExpr);
        else
            rightExpr = expr.rightExpr;
        topOperator = expr.topOperator;
        backendReadRange = expr.backendReadRange;
    }
    /**
     * A common initialization method for constructors using
     * unary or binary expressions as sources.
     * 
     * @param e an expression, from which this one is constructed
     */
    private void init(AbstractPTAExpression e) {
        target = e.target;
        minMaxRange = e.minMaxRange;
        backendStop = e.backendStop;
}
    /**
     * A common initialization method for constructors using
     * an element variable as a source.
     * 
     * @param e an element
     */
    private void init(PTAElementVariable element) {
        target = null;
        topOperator = AbstractCodeOp.Op.NESTED_INDEX;
        leftValue = element.array;
        rightValue = element.index;
        // the variable does not pass stops, yet creating an empty
        // set is already allowed at the stage of nested expressions
        backendStop = new HashSet<>();
    }
    /**
     * Assigns a value to either the left or the right operand of
     * this expression. If the value is an element variable, then a
     * respective sub--expression is assigned to a given field.
     * 
     * @param value value to assign
     * @param left true for the left operand, false for the right operand
     */
    private void set(AbstractPTAValue value, boolean left) {
        if(value instanceof PTAElementVariable) {
            PTANestedExpression indexed = new PTANestedExpression(
                    (PTAElementVariable)value);
            if(left)
                leftExpr = indexed;
            else
                rightExpr = indexed;
        } else {
            if(left)
                leftValue = value;
            else
                rightValue = value;
        }
    }
    /**
     * If the top operator of this expression is unary.
     * 
     * @return if topmost expression is unary
     */
    public boolean isTopUnary() {
        return topOperator.isUnary();
    }
    /**
     * If the top operator of this expression is binary.
     * 
     * @return if topmost expression is binary
     */
    public boolean isTopBinary() {
        return topOperator.isBinary();
    }
    /**
     * If this expression is nestable. Any nested expression but
     * a BRANCH one can be nested.
     * 
     * @return if can be a subexpression
     */
    @Override
    public boolean isNestable() {
        return topOperator != AbstractCodeOp.Op.BRANCH;
    }
    /**
     * Returns all sources within this expression.
     * 
     * @return a set of PTA values
     */
    @Override
    public SortedSet<AbstractPTAVariable> getSources() {
        SortedSet<AbstractPTAVariable> out = super.getSources();
        // all variables in operands are sources in this context
        if(leftValue != null) {
            out.addAll(leftValue.getVariables());
        } else
            out.addAll(leftExpr.getSources());
        if(rightValue != null) {
            out.addAll(rightValue.getVariables());
        } else if(rightExpr != null)
            out.addAll(rightExpr.getSources());
        //for(AbstractPTAVariable v : out)
        //    if(v == PTANestedExpression.SUPER_NESTED)
        // a marker, not a true source
        //out.remove(PTANestedExpression.SUPER_NESTED);
        return out;
    }
    /**
     * Recursively searches for and returns all nested
     * expressions within this one, that use the
     * operator <code>NESTED_INDEX</code>. Can
     * also return this expression.
     * 
     * @return all nested expressions
     */
    public List<PTANestedExpression> getIndexings() {
        List<PTANestedExpression> out = new LinkedList();
        if(leftExpr != null)
            out.addAll(leftExpr.getIndexings());
        if(rightExpr != null)
            out.addAll(rightExpr.getIndexings());
        if(topOperator == AbstractCodeOp.Op.NESTED_INDEX)
            out.add(this);
        return out;
    }
    /**
     * Returns a derived range of values, that a given side of this
     * expression can have. The range can be unknown or larger
     * than the actual one.<br>
     * 
     * Should be used only in the backend.
     * 
     * @param left true for the left operand, false for the right operand
     * @return range of values, null for unknown
     */
    public PTARange getBackendKnownRange(boolean left) {
        if(left) {
            if(leftValue instanceof PTAScalarVariable) {
                PTAScalarVariable sv = (PTAScalarVariable)leftValue;
                return sv.backendWriteRange;
            } else if(leftExpr != null)
                return leftExpr.backendReadRange;
            else if(leftValue == null)
                throw new RuntimeException("no left operand");
        } else{
            if(rightValue instanceof PTAScalarVariable) {
                PTAScalarVariable sv = (PTAScalarVariable)rightValue;
                return sv.backendWriteRange;
            } else if(rightExpr != null)
                return rightExpr.backendReadRange;
            else if(rightValue == null)
                throw new RuntimeException("no right operand");
        }
        return null;
    }
    /**
     * Creates a new top nested expression from a unary or a binary
     * expression. If the original expression updates a variable with a
     * known write range or if the expression has a read range, then
     * one of these ranges, or an intersection if both exist, are copied to the
     * new expression's read range.
     * 
     * @param expr an original expression
     * @return a new top nested expression
     */
    public static PTANestedExpression newTopExpression(AbstractPTAExpression expr) {
        PTANestedExpression n;
        if(expr instanceof PTAUnaryExpression) {
            n = new PTANestedExpression((PTAUnaryExpression)expr);
        } else if(expr instanceof PTABinaryExpression) {
            n = new PTANestedExpression((PTABinaryExpression)expr);
        } else if(expr instanceof PTANestedExpression) {
            n = new PTANestedExpression((PTANestedExpression)expr);
        } else
            throw new RuntimeException("unsupported type of expression");
        if(expr.target instanceof PTAScalarVariable) {
            PTAScalarVariable s = (PTAScalarVariable)expr.target;
            if(s.backendWriteRange != null)
                // estimate expression's values; the assigned range can be
                // larger than the actual one, as the target may also be written
                // to elsewhere
                n.backendReadRange = s.backendWriteRange;
            if(expr instanceof PTANestedExpression) {
                PTANestedExpression source = (PTANestedExpression)expr;
                if(source.backendReadRange != null) {
                    if(n.backendReadRange == null)
                        n.backendReadRange = source.backendReadRange;
                    else {
                        if((n.backendReadRange =
                                n.backendReadRange.overlaps(
                                    source.backendReadRange)) == null)
                            throw new RuntimeException("conflicting ranges");
                    }
                }
            }
        }
        return n;
    }
    /**
     * Creates a nested expression from a given expression's right hand side,
     * return value is set to <code>SUPER_NESTED</code> to mark a
     * subexpression. Can never be performed on a BRANCH nested expression.
     * 
     * Simply clears stops, as these may originate from other parts, and
     * subexpressions do not need them anyway.
     * 
     * @param expr source expression
     * @return a new nested sub--expression
     */
    private PTANestedExpression newSubexpression(
            AbstractPTAExpression expr) {
        PTANestedExpression n = newTopExpression(expr);
        if(!n.isNestable())
            throw new RuntimeException("not nestable");
        n.target = SUPER_NESTED;
        n.backendStop.clear();
        return n;
    }
    /**
     * Creates a nested expression being an equivalent of an
     * element variable, return value is set to
     * <code>SUPER_NESTED</code> to mark a subexpression.
     * 
     * @param element element variable
     * @return a new nested sub--expression
     */
    private PTANestedExpression newSubexpression(
            PTAElementVariable element) {
        PTANestedExpression n = new PTANestedExpression(element);
        n.target = SUPER_NESTED;
        return n;
    }
    /**
     * Replaces all sources in this expression equal to the result
     * of <code>update</code>, with the update's right hand side.
     * 
     * @param update expression to propagate into this one
     */
    public void propagate(AbstractPTAExpression update) {
        if(leftValue != null) {
            if(leftValue.equals(update.target)) {
                leftValue = null;
                leftExpr = newSubexpression(update);
            } else if(leftValue instanceof PTAElementVariable &&
                        ((PTAElementVariable)leftValue).index.equals(update.target)) {
                leftExpr = newSubexpression((PTAElementVariable)leftValue);
                leftExpr.rightExpr = newSubexpression(update);
                leftExpr.rightValue = null;
                leftValue = null;
            }
        } else
            leftExpr.propagate(update);
        if(rightValue != null) {
            if(rightValue.equals(update.target)) {
                rightValue = null;
                rightExpr = newSubexpression(update);
            } else if(rightValue instanceof PTAElementVariable &&
                        ((PTAElementVariable)rightValue).index.equals(update.target)) {
                rightExpr = newSubexpression((PTAElementVariable)rightValue);
                rightExpr.rightExpr = newSubexpression(update);
                rightExpr.rightValue = null;
                rightValue = null;
            }
        } else if(rightExpr != null)
            rightExpr.propagate(update);
    }
    /**
     * Returns a new, recursively simplified version of this expression.
     * The new expression may have reduced redundant nestings, and
     * a boolean negation reduced by inverting a conditonal equation.
     * 
     * @return a new nested expression
     */
    public PTANestedExpression simplify() {
        PTANestedExpression s = new PTANestedExpression(this);
        if(s.leftExpr != null)
            s.leftExpr = s.leftExpr.simplify();
        if(s.rightExpr != null)
            s.rightExpr = s.rightExpr.simplify();
        if(s.topOperator == AbstractCodeOp.Op.NONE &&
              s.leftExpr != null) {
            s = s.leftExpr;
            s.init(this);
        }
        if(topOperator == AbstractCodeOp.Op.UNARY_CONDITIONAL_NEGATION &&
                s.leftExpr != null) {
            switch(s.leftExpr.topOperator) {
                case BINARY_EQUAL:
                case BINARY_INEQUAL:
                case BINARY_LESS:
                case BINARY_LESS_OR_EQUAL:
                {
                    BinaryExpression.Op o = CodeOpBinaryExpression.
                            operatorCodeToTree(s.leftExpr.topOperator).newOpposite();
                    boolean flip = false;
                    // flip operators that do not exist as code operators
                    switch(o) {
                        case GREATER:
                            o = BinaryExpression.Op.LESS;
                            flip = true;
                            break;
                            
                        case GREATER_OR_EQUAL:
                            o = BinaryExpression.Op.LESS_OR_EQUAL;
                            flip = true;
                            break;
                            
                    }
                    s.leftExpr.topOperator = CodeOpBinaryExpression.operatorTreeToCode(o);
                    s = s.leftExpr;
                    s.init(this);
                    if(flip) {
                        AbstractPTAValue v = s.leftValue;
                        PTANestedExpression e = s.leftExpr;
                        s.leftValue = s.rightValue;
                        s.leftExpr = s.rightExpr;
                        s.rightValue = v;
                        s.rightExpr = e;
                    }
                    break;
                }
            }
        }
        return s;
    }
    @Override
    public AbstractPTAExpression newComplementary() {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public boolean isEquivalentCondition(AbstractPTAExpression other) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public boolean isFloating() {
        boolean floating = false;
        if(leftValue != null) {
            if(leftValue.floating)
                floating = true;
        } else {
            if(leftExpr.isFloating())
                floating = true;
        }
        if(rightValue != null) {
            if(rightValue.floating)
                floating = true;
        } else if(rightExpr != null) {
            if(rightExpr.isFloating())
                floating = true;
        }
        return floating;
    }
    @Override
    public boolean isConditional() {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public AbstractPTAExpression probability() {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public List<PTARange> trueRanges() {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public PTAConstant evaluate(Map<AbstractPTAVariable, PTAConstant> knowns) {
        throw new UnsupportedOperationException("not supported");
    }
    @Override
    public PTANestedExpression clone() {
        PTANestedExpression copy = (PTANestedExpression)super.clone();
        copy.leftValue = leftValue;
        if(leftExpr != null)
            copy.leftExpr = leftExpr.clone();
        copy.rightValue = rightValue;
        if(rightExpr != null)
            copy.rightExpr = rightExpr.clone();
        copy.topOperator = topOperator;
        return copy;
    }
    @Override
    public String toString() {
        String left;
        if(leftExpr != null)
            left = "(" + leftExpr.toString() + ")";
        else
            left = leftValue.toString();
        String right;
        if(rightExpr != null)
            right = "(" + rightExpr.toString() + ")";
        else if(rightValue != null)
            right = rightValue.toString();
        else
            right = null;
        String s;
        String op;
        switch(topOperator) {
            case NESTED_INDEX:
                s = left + "[" + right + "]";
                break;
                
            case BRANCH:
                PTANestedExpression e = rightExpr.clone();
                e.target = null;
                e.minMaxRange = null;
                s = "(" + left + " ? (" + e.toString() + ") : " +
                        target.toString() + ")";
                break;
                
            case NONE:
                s = left;
                break;
                
            default:
                if(right == null) {
                    if(topOperator == CodeOpUnaryExpression.Op.UNARY_ABS) {
                        s = "abs(" + left + ")";
                    } else {
                        op = UnaryExpression.getOperatorName(
                            CodeOpUnaryExpression.operatorCodeToTree(topOperator),
                                null)[0];
                        s = op + left;
                    }
                } else {
                    op = BinaryExpression.getOperatorName(
                        CodeOpBinaryExpression.operatorCodeToTree(topOperator));
                    s = left + op + right;
                }
                break;
                
        }
        if(target != null && target != SUPER_NESTED)
            s = target.toString() + " := " + s;
        return s;
    }
}
