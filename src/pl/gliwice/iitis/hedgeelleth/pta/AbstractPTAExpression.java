/*
 * AbstractPTAExpression.java
 *
 * Created on May 8, 2008
 *
 * Copyright (c) 2008, 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeConstant;
import pl.gliwice.iitis.hedgeelleth.compiler.code.RangedCodeValue;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAValue;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAVariable;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAConstant;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAScalarVariable;

/**
 * An abstract expression of PTA transition, with target variable.
 * The target variable can be null if this expression is a branch
 * condition.<br>
 * 
 * The cloning makes deep copies, except for fields of the class
 * <code>AbstractPTAValue</code>, but these are either
 * immutables or references to the same object anyway.
 * 
 * @author Artur Rataj
 */
abstract public class AbstractPTAExpression implements Cloneable {
    /**
     * Target variable, null for none.
     */
    public AbstractPTAVariable target;
    /**
     * The min/max range, to which this expression's value
     * can be trimmed.<br>
     * 
     * This range is for Prism, but is also included in PTA files.
     * It is likely that the interpreter of a PTA file finds no use of it.<br>
     * 
     * Always null in the backend.
     */
    public PTARange minMaxRange;
    /**
     * If this expression comes from <code>CodeOpBinaryExpression</code>
     * whose <code>singleEvaluationId != -1</code>, then that
     * field is copied here.<br>
     * 
     * It is assured in <code>TADDFactory.checkUniqueEvaluationIds</code>,
     * that the identifier is either equal to the default of -1, or otherwise
     * unique across an automaton.
     */
    public long singleEvaluationId = -1;
    /**
     * Which of variables in this equations are known to not transfer
     * meaningful values further. The trace may not cover all variables,
     * but must cover all local scalar variables, that stem from the code
     * compilation, at least until nested expressions are produced, <br>
     * 
     * Used only within the backend, where it is never null, possibly empty. Always null
     * outside the backend.<br>
     */
    protected Set<AbstractPTAVariable> backendStop = null;
    /**
     * An optional comment, null for none. Always null outside the backend.
     */
    public String backendComment = null;

    /**
     * Creates a new instance of AbstractPTAExpression, with null target.
     */
    public AbstractPTAExpression() {
        target = null;
        minMaxRange = null;
    }
    /**
     * Returns all variables used in this expression.
     * 
     * @return a set of variables, sorted for determined compiler output
     */
    public SortedSet<AbstractPTAVariable> getVariables() {
        SortedSet<AbstractPTAVariable> out = getSources();
        if(target != null)
            out.add(target);
        return out;
    }
    /**
     * Returns all variables read by this expression. This implementation
     * returns <code>target.getSources()</code>, if the target is not null;
     * otherwise, an empty set is returned.
     * 
     * @return set of variables, empty for no variables; sorted for determined
     * compiler output
     */
    public SortedSet<AbstractPTAVariable> getSources() {
        if(target != null)
            return target.getKnownSources();
        else
            return new TreeSet<>();
    }
    /**
     * If this expression is nestable in <code>PTANestedExpression</code>.
     * 
     * @return if can be a subexpression
     */
    abstract public boolean isNestable();
    /**
     * Creates a conditional expression that is complementary
     * to this expression. Algorithmically, these two expressions
     * always yield an opposite result when compared.<br>
     * 
     * This expression must be conditional too or
     * a runtime exception will be thrown.<br>
     * 
     * No backed stops are enabled in the new expression.<br>
     * 
     * @return a new expression
     */
    abstract public AbstractPTAExpression newComplementary();
    /**
     * If two expressions are equivalent conditions.<br>
     * 
     * If this expression or <code>other</code> are not
     * conditional, a runtime exception is thrown.
     * 
     * @return 
     */
    abstract public boolean isEquivalentCondition(
            AbstractPTAExpression other);
    /**
     * If this expression has a constant value, this method may
     * voluntarily return it. It always returns the constant
     * in the case of assignments of a constant value.<br>
     *
     * This implementation returns null.
     *
     * @return                          constant value of null if
     *                                  this expression is variable
     *                                  or it did not determine that
     *                                  it is constant
     */
    public PTAConstant getConstant() {
        return null;
    }
    /**
     * If this expression's return type is floating. In the return type is
     * none, returns false.<br>
     * 
     */
    abstract public boolean isFloating();
    /**
     * If this is a conditional expression.<br>
     * 
     * A conditional expression has a relation operator, but
     * still can have a non--null target.
     */
    abstract public boolean isConditional();
    /**
     * If this expression represents a probability, i. e. is an inequality
     * between <code>uni()</code> and a constant or a variable, then the probaility
     * value is returned. Otherwise, null is returned.
     * 
     * @return a new unary expression representing a constant or a variable,
     * or a new a binary expression of the form <code>1.0 - variable</code>,
     * or null
     */
    abstract public AbstractPTAExpression probability();
    /**
     * Converts a value to a ranged code constant, if possible. This is a helper
     * method for <code>evaluate()</code>.
     * 
     * @param value value to convert
     * @param knowns list of knowns
     * @return constant or null for unknown
     */
    protected static RangedCodeValue toConstant(AbstractPTAValue value,
            Map<AbstractPTAVariable, PTAConstant> knowns) {
        PTAConstant out =  null;
        if(value instanceof PTAConstant)
            out = (PTAConstant)value;
        else if(value instanceof AbstractPTAVariable)
            out = knowns.get((AbstractPTAVariable)value);
        if(out == null)
            return null;
        else
            return new RangedCodeValue(new CodeConstant(null,
                new Literal(out.value)));
    }
    /**
     * Converts a literal to a PTA constant. This is a helper
     * method for <code>evaluate()</code>.<br>
     * 
     * If the literal not not be converted, a runtime exception
     * is thrown.
     * 
     * @param l literal
     * @return a constant
     */
    protected static PTAConstant fromLiteral(Literal l) {
        if(l.type.numericPrecision() != 0)
            return new PTAConstant(
                    l.getMaxPrecisionFloatingPoint(),
                    l.type.isOfFloatingPointTypes());
        else if(l.type.isBoolean()) {
            if(l.getBoolean())
                return new PTAConstant(1, false);
            else
                return new PTAConstant(0, false);
        } else
            throw new RuntimeException("can not convert: " +
                    l.toString());
    }
    /**
     * Returns a list of ranges, that together cover all values, for
     * which a variable in a conditional expression makes the
     * expression true.<br>
     * 
     * This expression must be conditional and the condition must
     * contain a single variable, possibly compared to a constant.
     * Otherwise, <code>ArithmeticException<code> is thrown.<br>
     * 
     * For binary expressions excluding an inequality expression, always
     * at most a single range is returned. For an inequality expression
     * none, one or two ranges can be returned. For an identity
     * expression or a conditional negation, no range or respectively (1, 1) and (0, 0)
     * are returned.<br>
     * 
     * Note that for floating--point variables, the exclusion by a strict
     * inequality operator is represented by a respective field in the
     * range.
     * 
     * @return set of ranges, null if the range can not be deduced or
     * the expression is not supported
     */
    abstract public List<PTARange> trueRanges();
    /**
     * Tests, if two conditional expressions overlap.<br>
     * 
     * Throws <code>ArithmeticException</code>, if one of the expressions
     * is not a conditional one, or if the expressions test
     * different variables, or if a variable is compared
     * against another variable, and the set of variables is
     * different in each of the expressions.<br>
     * 
     * @return if two conditional expressions overlap
     */
    public boolean overlaps(AbstractPTAExpression other) {
        if(!isConditional() || !other.isConditional())
            throw new ArithmeticException("both expressions must be conditional");
        Set<AbstractPTAVariable> sources = getSources();
        Set<AbstractPTAVariable> otherSources = other.getSources();
        if(sources.size() < 1 || otherSources.size() < 1)
            throw new ArithmeticException("missing variable");
        if(sources.size() > 1 || otherSources.size() > 1) {
            if(sources.equals(otherSources)) {
                AbstractPTAExpression complementary = other.newComplementary();
                return !isEquivalentCondition(complementary);
            } else
                throw new ArithmeticException("different pairs of variables");
        }
        AbstractPTAVariable variable = sources.iterator().next();
        if(variable != otherSources.iterator().next())
            throw new ArithmeticException("no common variable");
        List<PTARange> ranges = trueRanges();
        List<PTARange> otherRanges = other.trueRanges();
        for(PTARange r1 : ranges)
            for(PTARange r2 : otherRanges)
                if(r1.overlaps(r2) != null)
                    return true;
        return false;
    }
    /**
     * Evaluates this expression, given a list of knowns.
     * 
     * @param knowns knowns
     * @return value, null for unknown
     */
    abstract public PTAConstant evaluate(Map<AbstractPTAVariable,
            PTAConstant> knowns);
    /**
     * Creates a set, referred by <code>backendStop</code>. To be used
     * exclusively in the backend, on each expression that ends up in a PTA.
     */
    public void backendEnableStop() {
        if(backendStopEnabled())
            throw new RuntimeException("transfer stop enabled twice");
        backendStop = new HashSet<>();
    }
    /**
     * If <code>backendEnableStop</code> has already been called on this
     * expression.
     */
    public boolean backendStopEnabled() {
        return backendStop != null;
    }
    /**
     * Adds a variable to <code>backendStop</code>. To be used
     * exclusively in the backend.
     * 
     * @param v variable
     */
    public void backendAddStop(AbstractPTAVariable v) {
        backendStop.add(v);
    }
    /**
     * Removes a variable from <code>backendStop</code>, if any. To be used
     * exclusively in the backend.
     * 
     * @param s variable
     * @return if the variable has been removed
     */
    public boolean backendRemoveStop(AbstractPTAVariable s) {
        return backendStop.remove(s);
    }
    /**
     * If a variable exists in <code>backendStop</code>. To be used
     * exclusively in the backend.
     * 
     * @param v variable
     * @return if it is known, that the variable does not transfer a meaningfull
     * value after this expression
     */
    public boolean backendHasStop(AbstractPTAVariable v) {
if(backendStop == null)
    v = v;
        return backendStop.contains(v);
    }
    /**
     * Copies stops from another expression. A new set is created.
     * 
     * @param orig source of the stops.
     */
    public void backendCopyStop(AbstractPTAExpression orig) {
        backendEnableStop();
        backendAddStops(orig);
    }
    /**
     * Adds stops from another expression.
     * 
     * @param orig source of the new stops.
     */
    public void backendAddStops(AbstractPTAExpression orig) {
        if(orig.backendStop == null)
            throw new RuntimeException("stop missing");
        for(AbstractPTAVariable v : orig.backendStop)
            backendAddStop(v);
    }
    /**
     * If an expression is going to be copied, this method can be used to
     * remove its stops, so that they can be put later by the calling code
     * into an expression, to which the original expression and all its
     * copies eventually follow.
     * 
     * @return stops
     */
    public void backendRemoveStops() {
         backendStop.clear();
    }
    /**
     * Helper method for <code>main()</code>.
     * 
     * @param title title to print
     * @param ranges list of ranges to print
     * @param pattern respective list of valid prints
     */
    private static void display(String title, List<PTARange> ranges,
            String... prints) {
        System.out.print(title + "\n   ");
        int count = 0;
        for(PTARange r : ranges) {
            String s = r.toString();
            System.out.print(" " + s);
        if(count >= prints.length)
            throw new RuntimeException("test failed : missing pattern");
            String p = prints[count];
            if(!s.equals(p))
                throw new RuntimeException("test failed : expected " + p);
            ++count;
        }
        System.out.println();
        if(count < prints.length)
            throw new RuntimeException("test failed : extra pattern");
    }
    @Override
    public AbstractPTAExpression clone() {
        try {
            AbstractPTAExpression copy = (AbstractPTAExpression)super.clone();
            copy.target = target;
            if(minMaxRange != null)
                copy.minMaxRange = new PTARange(minMaxRange);
            copy.singleEvaluationId = singleEvaluationId;
if(backendStop == null)
    copy = copy;
            copy.backendStop = new HashSet<>(backendStop);
            return copy;
        } catch(CloneNotSupportedException e) {
            throw new RuntimeException(e.toString());
        }
    }
    @Override
    public String toString() {
        if(target == null)
            return "";
        else
            return target.toString() + " := ";
    }
    /**
     * Tests <code>overlaps()</code>.
     * 
     * @param args 
     */
    public static void main(String[] args) throws PTAException {
        AbstractPTAVariable i = new PTAScalarVariable("i",
                new PTARange(null, 3, 7), false);
        AbstractPTAVariable f = new PTAScalarVariable("f",
                new PTARange(null, 3, 7), true);
        
        PTABinaryExpression b_fl5 = new PTABinaryExpression(
                f, new PTAConstant(5, false),
                CodeOpBinaryExpression.Op.BINARY_LESS);
        display("b_fl5", b_fl5.trueRanges(), "<<3.0, 5.0))");
        PTABinaryExpression b_fle5 = new PTABinaryExpression(
                f, new PTAConstant(5, false),
                CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL);
        display("b_fle5", b_fle5.trueRanges(), "<<3.0, 5.0>>");
        PTABinaryExpression b_il5 = new PTABinaryExpression(
                i, new PTAConstant(5, true),
                CodeOpBinaryExpression.Op.BINARY_LESS);
        display("b_il5", b_il5.trueRanges(), "<<3.0, 4.0>>");
        PTABinaryExpression b_ile5 = new PTABinaryExpression(
                i, new PTAConstant(5, true),
                CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL);
        display("b_ile5", b_ile5.trueRanges(), "<<3.0, 5.0>>");
        PTABinaryExpression b_5lf = new PTABinaryExpression(
                new PTAConstant(5, false), f,
                CodeOpBinaryExpression.Op.BINARY_LESS);
        display("b_5lf", b_5lf.trueRanges(), "((5.0, 7.0>>");
        PTABinaryExpression b_5lef = new PTABinaryExpression(
                new PTAConstant(5, false), f,
                CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL);
        display("b_5lef", b_5lef.trueRanges(), "<<5.0, 7.0>>");
        PTABinaryExpression b_5li = new PTABinaryExpression(
                new PTAConstant(5, true), i,
                CodeOpBinaryExpression.Op.BINARY_LESS);
        display("b_5li", b_5li.trueRanges(), "<<6.0, 7.0>>");
        PTABinaryExpression b_5lei = new PTABinaryExpression(
                new PTAConstant(5, true), i,
                CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL);
        display("b_5lei", b_5lei.trueRanges(), "<<5.0, 7.0>>");
        PTABinaryExpression b_5nf = new PTABinaryExpression(
                new PTAConstant(5, false), f,
                CodeOpBinaryExpression.Op.BINARY_INEQUAL);
        display("b_5nf", b_5nf.trueRanges(), "<<3.0, 5.0))", "((5.0, 7.0>>");
        PTABinaryExpression b_5ni = new PTABinaryExpression(
                new PTAConstant(5, true), i,
                CodeOpBinaryExpression.Op.BINARY_INEQUAL);
        display("b_5ni", b_5ni.trueRanges(), "<<3.0, 4.0>>", "<<6.0, 7.0>>");

        PTABinaryExpression b_fl55 = new PTABinaryExpression(
                f, new PTAConstant(5.5, false),
                CodeOpBinaryExpression.Op.BINARY_LESS);
        display("b_fl55", b_fl55.trueRanges(), "<<3.0, 5.5))");
        PTABinaryExpression b_fle55 = new PTABinaryExpression(
                f, new PTAConstant(5.5, false),
                CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL);
        display("b_fle55", b_fle55.trueRanges(), "<<3.0, 5.5>>");
        PTABinaryExpression b_il55 = new PTABinaryExpression(
                i, new PTAConstant(5.5, true),
                CodeOpBinaryExpression.Op.BINARY_LESS);
        display("b_il55", b_il55.trueRanges(), "<<3.0, 5.0>>");
        PTABinaryExpression b_ile55 = new PTABinaryExpression(
                i, new PTAConstant(5.5, true),
                CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL);
        display("b_ile55", b_ile55.trueRanges(), "<<3.0, 5.0>>");
        PTABinaryExpression b_55lf = new PTABinaryExpression(
                new PTAConstant(5.5, false), f,
                CodeOpBinaryExpression.Op.BINARY_LESS);
        display("b_55lf", b_55lf.trueRanges(), "((5.5, 7.0>>");
        PTABinaryExpression b_55lef = new PTABinaryExpression(
                new PTAConstant(5.5, false), f,
                CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL);
        display("b_55lef", b_55lef.trueRanges(), "<<5.5, 7.0>>");
        PTABinaryExpression b_55li = new PTABinaryExpression(
                new PTAConstant(5.5, true), i,
                CodeOpBinaryExpression.Op.BINARY_LESS);
        display("b_55li", b_55li.trueRanges(), "<<6.0, 7.0>>");
        PTABinaryExpression b_55lei = new PTABinaryExpression(
                new PTAConstant(5.5, true), i,
                CodeOpBinaryExpression.Op.BINARY_LESS_OR_EQUAL);
        display("b_55lei", b_55lei.trueRanges(), "<<6.0, 7.0>>");
        PTABinaryExpression b_55ef = new PTABinaryExpression(
                new PTAConstant(5.5, false), f,
                CodeOpBinaryExpression.Op.BINARY_EQUAL);
        display("b_55ef", b_55ef.trueRanges(), "<<5.5, 5.5>>");
        PTABinaryExpression b_55ei = new PTABinaryExpression(
                new PTAConstant(5.5, true), i,
                CodeOpBinaryExpression.Op.BINARY_EQUAL);
        display("b_55ei", b_55ei.trueRanges());
        PTABinaryExpression b_55nf = new PTABinaryExpression(
                new PTAConstant(5.5, false), f,
                CodeOpBinaryExpression.Op.BINARY_INEQUAL);
        display("b_55nf", b_55nf.trueRanges(), "<<3.0, 5.5))", "((5.5, 7.0>>");
        PTABinaryExpression b_55ni = new PTABinaryExpression(
                new PTAConstant(5.5, true), i,
                CodeOpBinaryExpression.Op.BINARY_INEQUAL);
        display("b_55ni", b_55ni.trueRanges(), "<<3.0, 7.0>>");
        
        AbstractPTAVariable b = new PTAScalarVariable("b",
                new PTARange(null, 0, 1), false);
        AbstractPTAVariable b0 = new PTAScalarVariable("b0",
                new PTARange(null, 0, 0), false);
        AbstractPTAVariable b1 = new PTAScalarVariable("b1",
                new PTARange(null, 1, 1), false);
        
        PTAUnaryExpression u_b = new PTAUnaryExpression(
                b, CodeOpUnaryExpression.Op.NONE);
        display("u_b", u_b.trueRanges(), "<<1.0, 1.0>>");
        PTAUnaryExpression un_b = new PTAUnaryExpression(
                b, CodeOpUnaryExpression.Op.UNARY_CONDITIONAL_NEGATION);
        display("un_b", un_b.trueRanges(), "<<0.0, 0.0>>");
        PTAUnaryExpression u_0 = new PTAUnaryExpression(
                b0, CodeOpUnaryExpression.Op.NONE);
        display("u_0", u_0.trueRanges());
        PTAUnaryExpression un_0 = new PTAUnaryExpression(
                b0, CodeOpUnaryExpression.Op.UNARY_CONDITIONAL_NEGATION);
        display("un_0", un_0.trueRanges(), "<<0.0, 0.0>>");
        PTAUnaryExpression u_1 = new PTAUnaryExpression(
                b1, CodeOpUnaryExpression.Op.NONE);
        display("u_1", u_1.trueRanges(), "<<1.0, 1.0>>");
        PTAUnaryExpression un_1 = new PTAUnaryExpression(
                b1, CodeOpUnaryExpression.Op.UNARY_CONDITIONAL_NEGATION);
        display("un_1", un_1.trueRanges());
        boolean o_f = b_fl5.overlaps(b_5lf);
        boolean o_t = b_fle5.overlaps(b_5lef);
        System.out.println("b_fl5 * b_5lf = " + o_f);
        if(o_f)
            throw new RuntimeException("should not overlap");
        System.out.println("b_fle5 * b_5lef = " + o_t);
        if(!o_t)
            throw new RuntimeException("should overlap");
    }
}
