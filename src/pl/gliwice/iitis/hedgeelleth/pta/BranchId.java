/*
 * BranchId.java
 *
 * Created on Jul 31, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.TextRange;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAVariable;

/**
 * A branch identifier. Transitions, of which a branch consists, should
 * share a common instance of the identifier.<br>
 * 
 * @author Artur Rataj
 */
public class BranchId implements Comparable {
    public enum Type {
        CONDITIONAL,
        PROBABILISTIC,
        NONDETERMINISTIC,
    }
    public final Type type;
    /**
     * Text range of a statement in the source code which produced this branch.
     * Null for non--existing or unknown.
     */
    public TextRange textRange;
    /**
     * If this is a non--deterministic @RANDOM method takes state variables/parameters,
     * this list contains them. Empty list if no arguments, like for <code>Random.nextInt()</code>.
     * If no @RANDOM method, null.
     */
    public List<AbstractPTAVariable> arguments;
    /**
     * If not empty, attemps to label the transitions of a non-deterministic branch,
     * as late as possible so as to not interfere with optimizations. Keyed with
     * one of the transition's <code>ndInt</code>s.
     */
    public Map<Integer, PTALabel> label;

    /**
     * Creates a new branch identificator.
     * 
     * @param type type of the identified branch
     */
    public BranchId(Type type) {
//if(type == Type.NONDETERMINISTIC)
//    type = type;
        this.type = type;
        label = new HashMap<>();
    }
    @Override
    public int compareTo(Object o) {
        BranchId other = (BranchId)o;
        if(textRange == null)
            throw new RuntimeException("branch ids can be compared only if " +
                    "text range is known");
        return textRange.BEG.compareTo(other.textRange.BEG);
    }
}
