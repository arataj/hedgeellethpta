/*
 * PTA.java
 *
 * Created on Apr 21, 2008
 *
 * Copyright (c) 2008, 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.op.AbstractCodeOp;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAVariable;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTABufferedConstant;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAClock;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAConstant;

/**
 * A PTA.
 * 
 * REFRESHOK: tt.label tt.sourceState
 * 
 * @author Artur Rataj
 */
public class PTA /*implements Comparable*/ {
    /**
     * Name of this PTA.
     */
    public String name;
    /**
     * List of states, sorted by number.
     */
    public SortedMap<Integer, PTAState> states;
    /**
     * Collection of transitions.
     */
    public List<PTATransition> transitions;
    /**
     * Transitions keyed with labels. Transitions without
     * labels have a null key.
     */
    public Map<PTALabel, Set<PTATransition>> labeled;
    /**
     * Clock of this PTA, null for none.
     */
    public PTAClock clock;
    /**
     * Start state of this PTA.
     */
    public PTAState startState;
    /**
     * Valuation.
     */
    public TreeMap<PTAState, Integer> valuation;
    /**
     * Formulas, for none either empty set or, within the backend,
     * possibly null for a PTA not created from a runtime method.<br>
     * 
     * Null before <code>addFormulas()</code> is called.
     */
    public SortedSet<StateFormula> stateFormulas;
    /**
     * If there is a non--determinism within this PTA.
     */
    public boolean nonDeterministic;
    /**
     * A comment about this PTA. Null inside the backend. Outside the backend
     * can be not null, and typically is the original name of the module.
     */
    public String comment;
    
    /**
     * Creates a new instance of PTA.
     * 
     * @param name                      name of this PTA
     */
    public PTA(String name) {
        this.name = name;
        states = new TreeMap<>();
        transitions = new ArrayList<>();
        labeled = new HashMap<>();
        clock = null;
        startState = null;
        valuation = new TreeMap<>();
    }
    /**
     * Adds a new transition. The transition must have the source and
     * target state fields already set.
     * 
     * The transition is added to the map of output transitions of
     * the source state, if not already there.
     * 
     * @param tt                        transition
     */
    public void addTransition(PTATransition tt) {
        addToLists(tt);
        tt.sourceState.addOutputTransition(tt);
        if(tt.ownerName != null && !tt.ownerName.equals(name))
            throw new RuntimeException("name mismatch: `" + tt.ownerName +
                    "' `" + name + "'");
        tt.ownerName = name;
    }
    /**
     * Adds a number of transitions to this PTA. On the base of the state indices in them,
     * creates the new states or gets them from a list of custom states.<br>
     * 
     * The target index may point to index of an operation that does not have an according
     * state -- if this is true, the index is increased until a respective state is found.
     * It can be done as operations without states are never jumps. The target index
     * can also be -1, meaning no output, and results in only adding the source state.
     * 
     * @param transitionMap transitions to be added, keyed with source indices,
     * within a transitions the source and target states should be null, only their respective
     * indices should be specified
     * @param customStates an optional list of custom states, null or empty for none
     * @param sorted if not null, forces the order in <code>transitions</code>;
     * may contain transitions that do not exist in <code>transitions</code>
     */
    public void add(Map<Integer, PriorityQueue<PTATransition>> transitionMap,
            Map<Integer, PTAState> customStates, List<PTATransition> sorted) {
        if(!states.isEmpty())
            throw new RuntimeException("PTA must have no transitions");
        //
        // create states
        //
        Map<Integer, PTAState> customStatesLeft = new HashMap<>(customStates);
        Set<Integer> stateIndices = new HashSet<>();
        for(int index: transitionMap.keySet())
            stateIndices.add(index);
        for(int index : stateIndices) {
            PTAState tState = customStates != null ?
                    customStates.get(index) : null;
            if(tState == null)
                tState = new PTAState(index);
            else
                customStatesLeft.values().remove(tState);
            states.put(index, tState);
        }
        states.putAll(customStatesLeft);
        //
        // connect
        //
        for(PriorityQueue<PTATransition> set : transitionMap.values())
            for(PTATransition tt : set) {
/*if(tt.sourceIndex == 21)
    tt = tt;*/
                tt.sourceState = states.get(tt.connectSourceIndex);
                while(true) {
                    if(tt.connectTargetIndex == -1) {
                        tt.targetState = null;
                        break;
                    } else {
                        tt.targetState = states.get(tt.connectTargetIndex);
                        if(tt.targetState != null)
                            break;
                        else
                            ++tt.connectTargetIndex;
                    }
                }
                if(tt.targetState != null)
                    addTransition(tt);
            }
        if(sorted != null) {
            Set<PTATransition> presence = new HashSet<>(transitions);
            transitions.clear();
            for(PTATransition x : sorted)
                if(presence.contains(x))
                    transitions.add(x);
        }
    }
    /**
     * Replaces all instances of <code>PTABufferedConstant</code>
     * with <code>PTAConstant</code>.
     * 
     * @return if anything has been replaced
     */
    public boolean replaceBufferedConstants() {
        boolean modified = false;
        for(PTATransition x : new LinkedList<>(transitions)) {
            if(x.guard instanceof PTABinaryExpression) {
                PTA.TransitionBag xbag = keyBefore(x);
                PTABinaryExpression be = (PTABinaryExpression) x.guard;
                if(be.left instanceof PTABufferedConstant) {
                    be.left = ((PTABufferedConstant)be.left).getConstant();
                    modified = true;
                }
                if(be.right instanceof PTABufferedConstant) {
                    be.right = ((PTABufferedConstant)be.right).getConstant();
                    modified = true;
                }
                keyAfter(xbag);
            }
            for(AbstractPTAExpression u : x.update)
                if(u instanceof PTAAssignment) {
                    PTA.TransitionBag xbag = keyBefore(x);
                    PTAAssignment a = (PTAAssignment)u;
                    if(a.sub instanceof PTABufferedConstant) {
                        a.sub = ((PTABufferedConstant)a.sub).getConstant();
                        modified = true;
                    }
                    keyAfter(xbag);
                }
        }
        return modified;
    }
    /**
     * Adds formulas to this PTA. Must be called after states are
     * already present in <code>states</code>, e. g. by calling
     * <code>add()</code>.<br>
     * 
     * If this PTA will be optimized, the formulas should be checked later using
     * <code>testFormulas()</code>.<br>
     * 
     * This method is used by the backend.
     * 
     * @param formulas state formulae, defines as in
     * <code>TADDFactory.stateFormulas</code>
     * @param formulasOr types of state formulae, defines as in
     * <code>TADDFactory.stateFormulasOr</code>
     */
    public void addFormulas(Map<String, Map<Integer, AbstractCodeOp>>
            formulas, Map<String, Boolean> formulasOr) throws PTAException {
        if(stateFormulas != null)
            throw new RuntimeException("state formulas can only be added " +
                    "a single time using this method");
        stateFormulas = new TreeSet<>();
        for(String formulaName : formulas.keySet()) {
            Map<Integer, AbstractCodeOp> indices = formulas.get(formulaName);
            boolean or = formulasOr.get(formulaName);
            SortedSet<PTAState> set = new TreeSet<>();
            SortedMap<PTAState, StreamPos> pos = new TreeMap<>();
            for(int index : indices.keySet()) {
                PTAState s = states.get(index);
                if(s == null)
                    throw new RuntimeException("state not found");
                set.add(s);
                AbstractCodeOp op = indices.get(index);
                if(op != null)
                    pos.put(s, op.getStreamPos());
            }
            stateFormulas.add(new StateFormula(formulaName, or, set, pos));
        }
    }
    /**
     * Adds formulas to this PTA. Must be called after states are
     * already present in <code>states</code>, e. g. by calling
     * <code>add()</code>.<br>
     * 
     * @param formulas state formulae, added to
     * <code>stateFormulas</code>
     */
    public void addFormulas(Set<StateFormula> formulas)
            throws PTAException {
        if(stateFormulas == null)
            stateFormulas = new TreeSet<>();
        stateFormulas.addAll(formulas);
    }
    /**
     * If a given state is in some state formula.
     * 
     * @param s state to test
     * @return if in a formula
     */
    public boolean inStateFormula(PTAState s) {
        for(StateFormula f : stateFormulas)
            if(f.states.contains(s))
                return true;
        return false;
    }
    /**
     * Replaces a given state in state formulas with
     * another state.
     * 
     * @param curr state to replace
     * @param replacement replacing state
     * @return if any formula has been modified
     */
    public boolean modifyStateFormulas(PTAState curr,
            PTAState replacement) {
        boolean changed = false;
        for(StateFormula f : stateFormulas)
            if(f.states.contains(curr)) {
                f.states.remove(curr);
                f.states.add(replacement);
                changed = true;
            }
        return changed;
    }
    /**
     * Tests, if states within formulas were not lost during optimizations.
     * If yes, a respective <code>PTAException</code> is thrown.<br>
     * 
     * This method should be called after all formulae--insensitive optimizations
     * have been performed. Formulae--sensitive optimizations can still be called
     * after this method finished.<br>
     * 
     * Current public PTA modifiers, that can not delete a state within a formula,
     * possibly at the cost of backing--off from performing certain optimizations:
     * <code>PTAOptimizer.closeStops()</code>*,
     * <code>PTAOptimizer.compactUpdate()</code>*,
     * <code>PTAOptimizer.nestExpressions()</code>*,
     * <code>PTAOptimizer.removeConstGuards()</code>*,
     * <code>TADDFactory.completeClockResetStops()</code>*,
     * <code>TADDFactory.deleteRedundantStates()</code>*,
     * <code>TADDFactory.nestAndCompactExpressions()</code>*,
     * <code>TADDFactory.updateLabeled()</code>*,
     * <code>Scheduler.applyInvariants()</code>*.
     * Before any of these modifiers is used, <code>PTA.addFormulas()</code>
     * must be called.
     */
    public void testFormulas() throws PTAException {
if(stateFormulas == null)
    startState = startState;
        for(StateFormula f : stateFormulas)
            for(PTAState s : f.states)
                if(!states.containsKey(s.index))
                    throw new PTAException(f.pos.get(s),
                            "formula `" + name + "' is lost");
    }
    /**
     * Adds only to lists <code>transitions</code> and
     * <code>labels</code>. The caller should plug/remove/reconnect
     * the transition by itself.
     * 
     * @param tt a transition removed
     */
    public boolean addToLists(PTATransition tt) {
        Set<PTATransition> set = labeled.get(tt.label);
        if(set == null) {
            set = new HashSet<>();
            labeled.put(tt.label == null ? null : tt.label.clone(), set);
        }
        set.add(tt);
        return transitions.add(tt);
    }
    /**
     * Removes only from lists <code>transitions</code> and
     * <code>labels</code>. The caller should unplug/reconnect
     * the transition by itself.
     * 
     * @param tt a transition removed
     */
    public boolean removeFromLists(PTATransition tt) {
        Set<PTATransition> set = labeled.get(tt.label);
        if(set != null)
            set.remove(tt);
        return transitions.remove(tt);
    }
    /**
     * Fully removes a transition.
     * 
     * @param tt transition to remove
     */
    public void removeTransition(PTATransition tt) {
        keyBefore(tt);
        PTAState state = tt.sourceState;
        if(state.output.get(tt.targetState.index).isEmpty()) {
            StateBag sb = keyBefore(state);
            state.output.remove(tt.targetState.index);
            keyAfter(sb);
        }
        if(tt.targetState.input.get(state.index).isEmpty()) {
            StateBag sb = keyBefore(tt.targetState);
            tt.targetState.input.remove(state.index);
            keyAfter(sb);
        }
    }
//    @Override
//    public int compareTo(Object o) {
//        return name.compareTo(((PTA)o).name);
//    }
    /**
     * A bag with information needed to refresh a state's key.
     */
    public static class StateBag {
        PTAState state;
        Integer valuation;
        
        /**
         * Creates a new bag.
         * 
         * @param state state to modify its key
         */
        protected StateBag(PTAState state) {
            this.state = state;
        }
    }
    /**
     * <p>If a state changes its key, but wtill exists in this PTA, then it should
     * be refreshed in this PTA's sets. This method prepares the state for this,
     * returning then its "bag", which can be user to reinsert the state, after the
     * key changed.</p>
     * 
     * <p>No other modification, that the change of the key, should occur to the PTA
     * in the meantime.</p>
     * 
     * @param state state to modify its key
     * @return information needed to restore the state
     */
    public StateBag keyBefore(PTAState state) {
        StateBag bag = new StateBag(state);
        bag.valuation = valuation.get(state);
        if(bag.valuation != null)
            valuation.remove(bag.state);
        return bag;
    }
    /**
     * Restores a state after key change.
     * 
     * @param bag information used to restore the state
     */
    public void keyAfter(StateBag bag) {
        if(bag.valuation != null)
            valuation.put(bag.state, bag.valuation);
    }
    /**
     * A bag with information needed to refresh a transition's key.
     */
    public static class TransitionBag {
        PTATransition transition;
        public Set<PTATransition> inputSet;
        public Set<PTATransition> outputSet;
        
        /**
         * Creates a new bag.
         * 
         * @param transition transition to modify its key
         */
        protected TransitionBag(PTATransition transition) {
            this.transition = transition;
        }
    }
    public void checkKeys() {
        for(Set<PTATransition> set : labeled.values())
            for(PTATransition tt : set)
                if(!set.contains(tt))
                {tt.getKey(); throw new RuntimeException("key modified");}
        for(PTATransition tt : transitions) {
            for(PTATransition tt2 : transitions)
                if(tt != tt2 && tt.label != null && tt.label == tt2.label)
                    throw new RuntimeException("duplicated label");
            if(labeled.get(tt.label) == null || !labeled.get(tt.label).contains(tt))
                    throw new RuntimeException("transition absent");
        }
    }
    /**
     * <p>If a transition changes its key, but still exists in this PTA, then it should
     * be refreshed in this PTA's sets. This method prepares the transition for this,
     * returning then its "bag", which can be user to reinsert it, after the key changed.
     * </p>
     * 
     * <p>No other modification, that the change of the key, should occur to the PTA
     * in the meantime.</p>
     * 
     * @param transition transition to modify its key
     * @return information needed to restore the transition
     */
    public TransitionBag keyBefore(PTATransition transition) {
        TransitionBag bag = new TransitionBag(transition);
        Set<PTATransition> set = labeled.get(transition.label);
        if(!set.remove(transition))
            throw new RuntimeException("not found");
        if(set.isEmpty())
            labeled.remove(transition.label);
        if(!transitions.remove(transition))
            throw new RuntimeException("not found");
        /////if(!transitions.remove(transition))
        /////    throw new RuntimeException("not found");
        SCAN_IN:
        for(PTAState s : states.values())
            for(Set<PTATransition> sets : s.input.values()) {
                if(sets.contains(transition)) {
                    bag.inputSet = sets;
                    sets.remove(transition);
                    break SCAN_IN;
                }
            }
        SCAN_OUT:
        for(PTAState s : states.values())
            for(Set<PTATransition> sets : s.output.values()) {
                if(sets.contains(transition)) {
                    bag.outputSet = sets;
                    sets.remove(transition);
                    break SCAN_OUT;
                }
            }
        return bag;
    }
    /**
     * Restores a transition after key change.
     * 
     * @param bag information used to restore the transition
     */
    public void keyAfter(TransitionBag bag) {
        Set<PTATransition> set = labeled.get(bag.transition.label);
        if(set == null) {
            set = new HashSet<>();
            PTALabel label = bag.transition.label;
            labeled.put(label == null ? null : label.clone(), set);
        }
        //
        // check is the transition is redundant
        //
        if(!set.contains(bag.transition)) {
            if(transitions.contains(bag.transition))
                bag = bag;//throw new RuntimeException("found");
            transitions.add(bag.transition);
            set.add(bag.transition);
            /////transitions.add(bag.transition);
            if(bag.inputSet != null)
                bag.inputSet.add(bag.transition);
            if(bag.outputSet != null)
                bag.outputSet.add(bag.transition);
        }
    }
    @Override
    public String toString() {
        String s = name + " {\n";
        if(startState != null)
            s += CompilerUtils.indent(1,
                    "start " + startState.index);
        if(clock != null)
            s += CompilerUtils.indent(1,
                    "clock " + clock.toString());
        s += "\n";
        for(PTAState tState : states.values()) {
            s += CompilerUtils.indent(1, tState.toString());
        }
        for(PTAState state : valuation.keySet())
            s += state.index + " : " + valuation.get(state) + "\n";
        s += "}\n";
        return s;
    }
}
