/*
 * PTATransition.java
 *
 * Created on Apr 21, 2008
 *
 * Copyright (c) 2008, 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.op.AbstractCodeOp;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.CodeOpNone;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.StringAnalyze;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAVariable;

/**
 * <p>Transition in a PTA.</p>
 * 
 * <p>There should never exist, even temporarily, two exactly the same
 * transitions going out of  a single location, as transitions implement
 * comparable and are stored in sets.</p>
 * 
 * @author Artur Rataj
 */
public class PTATransition implements Cloneable, Comparable<PTATransition> {
    /**
     * Annotation of the method emptyTransition(). An empty transition
     * with this annotation is not reported empty by <code>empty()</code>
     * and thus is not deleted in the method
     * <code>TADDFactory.deleteRedundantStates()</code>.<br>
     * 
     * To be used only by the backend.
     */
    public static final String ANNOTATION_SPECIAL_EMPTY_TRANSITION_STRING =
            "@EMPTY_TRANSITION";
    /**
     * Label of this transition, null for none.
     */
    public PTALabel label;
    /**
     * Source state or null if not known yet.
     */
    public PTAState sourceState = null;
    /**
     * Target state or null if not known yet.
     */
    public PTAState targetState = null;
    /**
     * Guard condition, null for none. Its target is always null.<br>
     * 
     * A guard whose <code>probability()</code> is not <code>Double.NaN</code>
     * is eventually transformed into a multiplier of <code>probability</code>,
     * leaving this field null.
     * 
     * @see TADDFactory.findProbabilities(PTA)
     */
    public AbstractPTAExpression guard = null;
    /**
     * <p>Typically used to ensure that <code>guard</code> in a barrier is atomic, which
     * translates e.g. to this transition being also a synchronising point, and looping to itself.</p>
     * 
     * <p>The constraint is checked on the final automatons in the backend, so that
     * all of involved optimizers have a chance to make the guard atomic.</p>
     */
    public boolean loopsToItself = false;
    /**
     * State update expressions, empty list for none. Their targets are never null.<br>
     * 
     * These updates should not depend on each other, can be non--local
     * only in hidden states, and can be executed in any order.
     */
    public List<AbstractPTAExpression> update = new LinkedList<>();
    /**
     * <p>Clock condition for lower limit if compared to a determined
     * value, or an equation determining the time if compared to a density.
     * Null for none.</p>
     * 
     * <p>If not null, requires the clock field to be not null.</p>
     * 
     * <p>Can temporarily, before locaions are constructed, express a higher
     * limit or an equation. These will be modified by adding a respective
     * clock invariant at the source location.</p>
     */
    public PTAClockCondition clockLimitLow = null;
    /**
     * Probability of being taken, within a probabilitic branch.
     * Represents a constant or a variable or 1 - variable. Otherwise null.
     */
    public AbstractPTAExpression probability;
    /**
     * If not null, this transition is one of a set of transitions going
     * from some state, and being an effect of a common conditional,
     * probabilistic or nondeterministic branch.
     * 
     * All transitions in such a set have a common <code>branchId</code>,
     * and in the case of a probabilistic branch, their probabilities sum to 1.0.
     */
    public BranchId branchId;
    /**
     * Meaningfull only for non--deterministic branches made with
     * <code>Random.nextInt()</code>, holds the values returned by that
     * function for a given branch. In any other case null.
     */
    public List<Integer> ndInt;
    /**
     * Index of the source operation in code method, -1 for
     * undefined. Used only to connect the transitions when
     * building a PTA. In other cases, <code>sourceState.index</code>
     * should be used instead.
     */
    public int connectSourceIndex = -1;
    /**
     * Index of the target operation in code method, -1 for
     * undefined. Used only to connect the transitions when
     * building a PTA. In other cases, <code>targetState.index</code>
     * should be used instead.
     */
    public int connectTargetIndex = -1;
    /**
     * Comments of this transition. Possibly not null only outside
     * the backend.<br>
     * 
     * Within the backend, use <code>backendOps</code> instead.
     */
    public List<String> comments;
    /**
     * Name of the PTA which owns this transition. Set in <code>PTA.addTransition()</code>
     * or earlier if needed.
     */
    public String ownerName = null;
    /**
     * Code operations, on whose is this transition is based.
     * An empty list if there are no such operations.<br>
     * 
     * Used only in the backend and only to generate comments. Outside
     * the backend it is null. To access comments outside the backend,
     * use <code>comments</code>.
     */
    public List<AbstractCodeOp> backendOps = new LinkedList<>();
    /**
     * Index of a chosen operation within <code>backendOps</code>
     * within the inlined method, translated to the PTA; -1 for unknown.
     * 
     * If a PTA is generated from a method, then all these indices must be specified.
     * If a PTA does not originate from a method, e. g. it is a lock, then either
     * all indices should be unknown or all indices should be specified.
     */
    public int backendOpIndex = -1;
//    /**
//     * Which of local variables in a method, from which this transition had
//     * been generated, do not transfer meaningfull values
//     * further. May not cover all variables, but must cover all local scalar variables, that
//     * stem from the compilation.<br>
//     * 
//     * Used only within the backend, where it is never null, possibly empty. Always null
//     * outside the backend.<br>
//     */
//    protected Set<AbstractPTAVariable> backendStop;
    /**
     * <p>A temporary uniqueness identifier, required if other elements of the key
     * are temporarily the same for more transitions. The default of -1 is for none.</p>
     * 
     * <p>Set to none after use.</p>
     */
    public int backendUniquenessId = -1;
    /**
     * <p>If this transition should never take place, if any set of transitions synchronised with
     * each other can take place.</p>
     * 
     * <p>If true, no transition with both a synchronisation and a guard other than
     * <code>PTALocationGuard</code> is allowed in the whole system, or a translation
     * error occurs.</p>
     */
    public boolean backendWaitForSync = false;
    /**
     * Starts the part of the key which provides uniqueness to two
     * identical transitions.
     */
    final public static String PROB_UNIQUENESS_PREFIX = " #uq";
    /**
     * Provides key uniqueness for probabilistic branches.
     */
    long probUniqueness = 0;
    /**
     * Probabilistic key uniqueness pool.
     */
    static long probUniquenessPool = 0;
    
    /**
     * Creates a new instance of PTATransition, with an unspecified
     * probability.
     * 
     * @param label                     label of this transition,
     *                                  null for none
     */
    public PTATransition(PTALabel label) {
        this.label = label;
        probability = null;
        probUniqueness = probUniquenessPool++;
    }
    /**
     * If this transition is marked as a special empty transition by the
     * annotation whose name is defined by
     * <code>ANNOTATION_SPECIAL_EMPTY_TRANSITION_STRING</code>.
     * 
     * @return if a special empty transition, that should not be optimised
     * out
     */
    public boolean isSpecial() {
        boolean special = false;
        CHECK_SPECIAL_EMPTY:
        for(AbstractCodeOp op : backendOps)
            if(op instanceof CodeOpNone) {
                CodeOpNone n = (CodeOpNone)op;
                for(String a : n.annotations)
                    if(a.equals(ANNOTATION_SPECIAL_EMPTY_TRANSITION_STRING)) {
                        special = true;
                        break CHECK_SPECIAL_EMPTY;
                    }
            }
        return special;
    }
//    /**
//     * If this transition is marked as a special empty transition by the
//     * annotation whose name is defined by
//     * <code>ANNOTATION_SPECIAL_EMPTY_TRANSITION_STRING</code>,
//     * then that annotation is removed.
//     * 
//     * @return if the annotation has been removed
//     */
//    public boolean removeSpecial() {
//        boolean special = false;
//        for(AbstractCodeOp op : backendOps)
//            if(op instanceof CodeOpNone) {
//                CodeOpNone n = (CodeOpNone)op;
//                for(String a : new HashSet<>(n.annotations))
//                    if(a.equals(ANNOTATION_SPECIAL_EMPTY_TRANSITION_STRING)) {
//                        n.annotations.remove(a);
//                        special = true;
//                    }
//            }
//        return special;
//    }
    /**
     * If this transition does not have any of the following:
     * label, condition, expression, clock, and
     * <code>isSpecial()</code> returns false.
     * 
     * @return                          if this is an empty transition
     *                                  and not a non--removable transition
     */
    public boolean isEmpty() {
//        if(label != null && guard != null)
//            throw new RuntimeException("00");
        boolean empty = label == null && guard == null &&
                probability == null && update.isEmpty() &&
                clockLimitLow == null;
        return empty && !isSpecial();
    }
    /**
     * Returns all source variables used in this transition.
     * 
     * @return a set of variables
     */
    public Set<AbstractPTAVariable> getSources() {
        Set<AbstractPTAVariable> sources = new HashSet<>();
        if(guard != null)
            sources.addAll(guard.getSources());
        if(probability != null)
            sources.addAll(probability.getSources());
        if(clockLimitLow != null)
            sources.addAll(clockLimitLow.getSources());
        for(AbstractPTAExpression v : update)
            sources.addAll(v.getSources());
        return sources;
    }
    /**
     * Returns all variables used in this transition.
     * 
     * @return a set of variables
     */
    public Set<AbstractPTAVariable> getUsed() {
        Set<AbstractPTAVariable> used = getSources();
        for(AbstractPTAExpression v : update)
            used.add(v.target);
        return used;
    }
    //    /**
//     * Creates a set, referred by <code>backendStop</code>. To be used
//     * exclusively in the backend, on each transition that ends up in a PTA.
//     */
//    public void backendEnableStop() {
//        if(backendStop != null)
//            throw new RuntimeException("transfer stop enabled twice");
//        backendStop = new HashSet<>();
//    }
//    /**
//     * Adds a variable to <code>backendStop</code>. To be used
//     * exclusively in the backend.
//     */
//    public void backendAddStop(AbstractPTAVariable v) {
//        backendStop.add(v);
//    }
//    /**
//     * If a variable exists in <code>backendStop</code>. To be used
//     * exclusively in the backend.
//     * 
//     * @param v variable
//     * @return if it is known, that the variable does not transfer a meaningfull
//     * value after this transition
//     */
//    public boolean backendHasStop(AbstractPTAVariable v) {
//        return backendStop.contains(v);
//    }
    @Override
    public int compareTo(PTATransition tt) {
        String name = getKey();
        String otherName = tt.getKey();
        return name.compareTo(otherName);
    }
    @Override
    public boolean equals(Object o) {
        PTATransition tt = (PTATransition)o;
        String name = getKey();
        String otherName = tt.getKey();
        return name.equals(otherName);
    }
    @Override
    public int hashCode() {
        return getKey().hashCode();
    }
    /**
     * Creates a cloned instance of PTATransition, which is
     * parallel to this transition, which in turn must already be
     * connected to the input and output states. The copy
     * is not connected, <code>copy.sourceState.addOutputTransition(copy);</code>
     * can be used for that, but only if the copy is modified to be different
     * from the original, as otherwise the connection will be ignored as redundant.
     */
    @Override
    public PTATransition clone() {
        try {
            PTATransition copy = (PTATransition)super.clone();
            copy.label = label == null ? null : label.clone();
            copy.sourceState = sourceState;
            copy.connectSourceIndex = connectSourceIndex;
            copy.targetState = targetState;
            copy.connectTargetIndex = connectTargetIndex;
            if(backendOps != null)
                copy.backendOps = new LinkedList(backendOps);
            copy.backendOpIndex = backendOpIndex;
            if(ndInt != null)
                copy.ndInt = new LinkedList<>(ndInt);
            copy.branchId = branchId;
            if(clockLimitLow != null)
                copy.clockLimitLow = clockLimitLow.clone();
            if(probability != null)
                copy.probability = probability.clone();
            if(comments != null)
                copy.comments = new LinkedList(comments);
            if(guard != null)
                copy.guard = guard.clone();
            copy.update = new LinkedList<>();
            for(AbstractPTAExpression expr : update)
                copy.update.add(expr.clone());
            return copy;
        } catch(CloneNotSupportedException e) {
            throw new RuntimeException(e.toString());
        }
    }
    private String removeRanges(String in) {
        return StringAnalyze.removeWithin(
                StringAnalyze.removeWithin(in, "[<<", ">>]"), "<<", ">>");
    }
    /**
     * Returns the key of this transition.
     * 
     * @return key
     */
    public String getKey() {
        String s = "";
        if(sourceState != null)
            s += sourceState.index;
        if(targetState != null)
            s += " -> " + targetState.index;
        if(label != null)
            s += " <" + label.toString() + ">";
        s += " {";
        if(clockLimitLow != null) {
            s += " ";
            s += clockLimitLow.toString();
        }
        if(probability != null) {
            s += " ";
//try {
            s += probability.toString();
//} catch(Exception e) {
//    probability.toString();
//}
        }
        if(guard != null)
            s += " " + guard.toString();
        for(AbstractPTAExpression expr : update)
            s += " " + expr.toString();
        s += " }";
        if(backendUniquenessId != -1)
            s += " UNIQUE" + backendUniquenessId;
        if(probability != null)
            s += PROB_UNIQUENESS_PREFIX + probUniqueness;            
        return removeRanges(s);
    }
    @Override
    public String toString() {
        String s = getKey();
        if(s.indexOf(PROB_UNIQUENESS_PREFIX) != -1)
            s = s.substring(0, s.indexOf(PROB_UNIQUENESS_PREFIX));
        for(AbstractCodeOp op : backendOps)
            s += " " + op.toString() +
                " (" + op.getStreamPos().toString() + ")";
        if(comments != null)
            for(String comment: comments)
                s += " # " + comment;
        return s;
    }
}
