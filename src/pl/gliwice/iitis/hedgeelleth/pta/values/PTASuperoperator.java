/*
 * PTASuperoperator.java
 *
 * Created on Dec 13, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.values;

import java.util.*;

/**
 * A superoperator.
 * 
 * @author Artur Rataj
 */
public class PTASuperoperator extends AbstractPTAValue {
    /**
     * Creates a new instance of <code>PTASuperoperator</code>.
     * 
     */
    public PTASuperoperator() {
        super(true);
    }
    @Override
    public SortedSet<AbstractPTAVariable> getVariables() {
        SortedSet<AbstractPTAVariable> out = new TreeSet<>();
        return out;
    }
}
