/*
 * PTABufferedConstant.java
 *
 * Created on Nov 29, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.values;

import java.util.*;
import java.nio.*;

/**
 * <p>Like <code>PTAConstant</code>, but its value is not known
 * a priori. Instead, there is a reference to a buffer, that might eventually hold
 * the value.</p>
 * 
 * <p>Should usually be put only into <code>PTAAssignment</code>, or
 * otherwise it will not be replaced automatically in
 * <code>PTA.replaceBufferedConstants()</code> by
 * <code>PTAConstant</code>.</p>
 * 
 * @author Artur Rataj
 */
public class PTABufferedConstant extends AbstractPTAValue {
    /**
     * If not floating, references an integer buffer, which is expected to be
     * either empty, or to contain a single value.
     */
    IntBuffer INT;
    /**
     * If floating, references a double buffer, which is expected to be
     * either empty, or to contain a single value.
     */
    DoubleBuffer DOUBLE;
    /**
     * Default integer value, used if the buffer is never filled.
     */
    int INT_DEFAULT;
    /**
     * Default double value, used if the buffer is never filled.
     */
    double DOUBLE_DEFAULT;

    /**
     * Creates a new instance of PTABufferedConstant, for
     * holding integer values.
     * 
     * @param intBuffer a buffer, that will possibly hold the constant
     * @param defaultValue default value, used if the buffer is never
     * filled
     */
    public PTABufferedConstant(IntBuffer intBuffer, int defaultValue) {
        super(false);
        INT = intBuffer;
        DOUBLE = null;
        INT_DEFAULT = defaultValue;
    }
    /**
     * Creates a new instance of PTABufferedConstant, for
     * holding double values.
     * 
     * @param doubleBuffer a buffer, that will possibly hold the constant
     * @param defaultValue default value, used if the buffer is never
     * filled
     */
    public PTABufferedConstant(DoubleBuffer doubleBuffer, double defaultValue) {
        super(false);
        INT = null;
        DOUBLE = doubleBuffer;
        DOUBLE_DEFAULT = defaultValue;
    }
    /**
     * If the integer constant is found in the the buffer, it is returned.
     * Otherwise, if the buffer is empty, null is returned.
     * 
     * @return contents of the buffer, null if none
     */
    public Integer getInt() {
        if(floating)
            throw new RuntimeException("called on a floating constant");
        INT.rewind();
        if(INT.hasRemaining()) {
            int i = INT.get();
            INT.rewind();
            return i;
        } else
            return null;
    }
    /**
     * If the constant is found in the the buffer, it is returned.
     * Otherwise, if the buffer is empty, null is returned.
     * 
     * @return contents of the buffer, null if none
     */
    public Double getDouble() {
        if(!floating)
            throw new RuntimeException("called on an integer constant");
        DOUBLE.rewind();
        if(DOUBLE.hasRemaining()) {
            double d = DOUBLE.get();
            DOUBLE.rewind();
            return d;
        } else
            return null;
    }
    /**
     * Returns the constant, that would replace this one, given
     * the current buffer contents.
     * 
     * @return a constant, with either a buffered or the default value
     */
    public PTAConstant getConstant() {
        if(floating) {
            Double d = getDouble();
            if(d == null)
                d = DOUBLE_DEFAULT;
            return new PTAConstant(d, true);
        } else {
            Integer i = getInt();
            if(i == null)
                i = INT_DEFAULT;
            return new PTAConstant(i, false);
        }
    }
    @Override
    public SortedSet<AbstractPTAVariable> getVariables() {
        return new TreeSet<>();
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof PTABufferedConstant) {
            PTABufferedConstant b = (PTABufferedConstant)o;
            return INT == b.INT && DOUBLE == b.DOUBLE;
        } else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + Objects.hashCode(this.INT);
        hash = 43 * hash + Objects.hashCode(this.DOUBLE);
        return hash;
    }
    @Override
    public String toString() {
        if(floating)
            return "<<" + getDouble() + " (" + DOUBLE_DEFAULT + ")>>";
        else
            return "<<" + getInt() + " (" + INT_DEFAULT + ")>>";
    }
}
