/*
 * PTAClock.java
 *
 * Created on May 15, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.values;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;
import pl.gliwice.iitis.hedgeelleth.pta.PTARange;

import java.util.*;

/**
 * A clock for TADD. The initial value is meaningless for this
 * clock.
 * 
 * @author Artur Rataj
 */
public class PTAClock extends AbstractPTAVariable {
    /**
     * Range of values allowed for a clock.
     */
    protected static final PTARange CLOCK_RANGE = new PTARange(
            new Type(Type.PrimitiveOrVoid.INT), false);

    /**
     * Creates a new instance of PTAClock. Only one instance of
     * clock per thread is allowed.
     * 
     * @param name                      name of the clock
     */
    public PTAClock(String name) {
        super(name, CLOCK_RANGE, true);
    }
    @Override
    public SortedSet<AbstractPTAVariable> getKnownSources() {
        return new TreeSet<>();
    }
    @Override
    public String toString() {
        return name;
    }
}
