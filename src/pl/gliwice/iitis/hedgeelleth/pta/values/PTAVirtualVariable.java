/*
 * PTAVirtualVariable.java
 *
 * Created on Jun 18, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.values;

import java.util.SortedSet;
import pl.gliwice.iitis.hedgeelleth.compiler.code.CodeConstant;


/**
 * A scalar variable optimised out. This merely object holds its name and the
 * value, which replaced the original variable.
 * 
 * @author Artur Rataj
 */
public class PTAVirtualVariable extends PTAScalarVariable {
    /**
     * Creates a new virtual variable.
     * 
     * @param name name
     * @param floating if floating
     * @param c the replacing constant
     */
    public PTAVirtualVariable(String name, boolean floating, CodeConstant value) {
        super(name, null, floating);
        initValue = value.value.getMaxPrecisionFloatingPoint();
    }
    @Override
    public String toString() {
        return "(virtual)" + super.toString();
    }
}
