/*
 * PTADensity.java
 *
 * Created on Jul 16, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.values;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.CompilerAnnotation;
import pl.gliwice.iitis.hedgeelleth.pta.PTARange;

/**
 * A probability density function. Can contain variables and thus is not a subclass
 * of <code>PTAConstant</code>.
 * 
 * The range is always mathematically know, but the backend may impose
 * its own value, it is typically needed only to set a double value for
 * <code><= 1 exclusive</code>.
 * 
 * @author Artur Rataj
 */
public class PTADensity extends AbstractPTAValue {
    /**
     * Parameters representing the type of this distribution: uniform, negative exponential,
     * two arrays, linearly interpolated values.
     */
    public enum ParameterType {
        UNI,
        NXP,
        ARRAY,
    }
    /**
     * Type of this distribution.
     */
    public final ParameterType type;
    /**
     * Range of allowed values.
     */
    public final PTARange range;
    /**
     * Parameters of this distribution.
     */
    public final AbstractPTAValue[] params;
    
    /**
     * Creates a new instance of <code>PTADensity</code>.
     * 
     * @param type type of this distribution
     * @param range range, typically not null only in the backend
     * @param params parameters of this distribution
     * 
     */
    public PTADensity(ParameterType type, PTARange range, AbstractPTAValue[] params) {
        super(true);
        this.type = type;
        this.range = range;
        this.params = params;
        int numArgs = getNumArgs(this.type);
        if(numArgs != this.params.length)
            throw new RuntimeException("mismatch of number of parameters");
        Object argType = getArgsType(this.type);
        for(AbstractPTAValue p : this.params) {
            if(!(p instanceof PTAConstant)) {
                if(!p.getClass().isInstance(argType))
                    throw new RuntimeException("mismatch of type of parameters");
            }
        }
    }
    /**
     * Creates a new instance of <code>PTADensity</code>. This is a convenience constructor.
     * 
     * @param type type of this distribution
     * @param params parameters of this distribution
     * 
     */
    public PTADensity(ParameterType type, AbstractPTAValue[] params) {
        this(type, null, params);
    }
    /**
     * Creates a new instance of <code>PTADensity</code>, with constant
     * scalar parameters.<br>
     * 
     * This is a convenience constructor.
     * 
     * @param type type of this distribution
     * @param range range, typically not null only in the backend
     * @param params parameters of this distribution
     * 
     */
    public PTADensity(ParameterType type, PTARange range, double[] params) {
        this(type, range, toValues(params));
    }
    /**
     * Creates a new instance of <code>PTADensity</code>, with constant
     * scalar parameters.<br>
     * 
     * This is a convenience constructor.
     * 
     * @param type type of this distribution
     * @param params parameters of this distribution
     * 
     */
    public PTADensity(ParameterType type, double[] params) {
        this(type, null, toValues(params));
    }
    /**
     * Converts an array of doubles to an array of 
     * <code>AbstractPTAValue</code>.
     * 
     * @param in input array
     * @return output array
     */
    private static AbstractPTAValue[] toValues(double[] in) {
        int size = in.length;
        AbstractPTAValue[] out = new AbstractPTAValue[size];
        for(int i = 0; i < size; ++i)
            out[i] = new PTAConstant(in[i], true);
        return out;
    }
    /**
     * Returns the number of arguments of a distribution, that is
     * associated with a given compiler annotation.
     * 
     * @param type density type
     * @return number of arguments
     */
    public static int getNumArgs(ParameterType type) {
        switch(type) {
            case UNI:
                return 0;

            case NXP:
                return 1;
                
            case ARRAY:
                return 2;
                
            default:
                throw new RuntimeException("unknown type");
        }
    }
    /**
     * Returns an object, whose runtime class must be
     * a superclass or a class of all the density function's
     * variable arguments.
     * 
     * @param type density type
     * @return a fake variable, being either <code>PTAScalarVariable</code> or
     * <code>PTAArrayVariable</code>
     */
    public static Object getArgsType(ParameterType type) {
        switch(type) {
            case UNI:
            case NXP:
                return new PTAScalarVariable(null, null, false);
                
            case ARRAY:
                return new PTAArrayVariable(null, 0, new double[0], false);
                
            default:
                throw new RuntimeException("unknown type");
        }
    }
    @Override
    public SortedSet<AbstractPTAVariable> getVariables() {
        SortedSet<AbstractPTAVariable> out = new TreeSet<>();
        for(AbstractPTAValue v: params)
            out.addAll(v.getVariables());
        return out;
    }
    /**
     * Returns the type of distribution, that is associated with a given
     * compiler annotation.
     * 
     * @param annotation annotation
     * @return distribution type, null if the annotation does not represent any
     * distribution
     */
    public static ParameterType getType(CompilerAnnotation annotation) {
        switch(annotation) {
            case DIST_UNI:
                return PTADensity.ParameterType.UNI;
                
            case DIST_NXP:
                return PTADensity.ParameterType.NXP;
                
            case DIST_ARRAY:
                return PTADensity.ParameterType.ARRAY;
                
            default:
                return null;
        }
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof PTADensity) {
            PTADensity other = (PTADensity)o;
            if(type != other.type)
                return false;
            if(other.params == null)
                return false;
            for(int i = 0; i < getNumArgs(type); ++i)
                if(!params[i].equals(other.params[i]))
                    return false;
            return true;
        } else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 73 * hash + Arrays.deepHashCode(this.params);
        return hash;
    }
    /**
     * Returns a textual representation of this distribution's type.
     * 
     * @return "uni" or "nxp"
     */
    public String typeToString() {
        switch(type) {
            case UNI:
                return "uni";
                
            case NXP:
                return "nxp";
                
            case ARRAY:
                return "tab";
                
            default:
                throw new RuntimeException("unknown type");
        }
    }
    @Override
    public String toString() {
        String s = typeToString() + "(";
        for(int i = 0; i < params.length; ++i) {
            s += params[i].toString();
            if(i < params.length - 1)
                s += ", ";
        }
        s += ")";
        return s;
    }
}
