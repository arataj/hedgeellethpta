/*
 * PTAConstant.java
 *
 * Created on May 8, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.values;

import java.util.*;

/**
 * An arithmetic constant in a PTA.<br>
 * 
 * Boolean values are encoded as <code>PTAConstant(0, false)</code>
 * for false and <code>PTAConstant(1, false)</code> for true.<br>
 * 
 * To become an expression, the constant must be wrapped into
 * <code>PTAUnaryExpression</code> with an operator
 * <code>CodeOpUnaryExpression.Op.NONE</code>.<br>
 * 
 * Despite densities being replaced by literals of annotated types
 * in method operations, densities in PTAs are not constants,
 * but have their own type <code>PTADensity</code>.
 * 
 * @author Artur Rataj
 */
public class PTAConstant extends AbstractPTAValue {
    /**
     * Value of this constant.
     */
    public final double value;
    
    /**
     * Creates a new instance of PTAConstant.
     * 
     * @param value                     value of this constant
     * @param floating                  if a floating point or integer value
         */
    public PTAConstant(double value, boolean floating) {
        super(floating);
        this.value = value;
    }
    @Override
    public SortedSet<AbstractPTAVariable> getVariables() {
        return new TreeSet<>();
    }
    /*
    @Override
    public String getCodeString() {
        return "" + value;
    }
     */
    @Override
    public boolean equals(Object o) {
        if(o instanceof PTAConstant)
            return value == ((PTAConstant)o).value;
        else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.value) ^ (Double.doubleToLongBits(this.value) >>> 32));
        return hash;
    }
    @Override
    public String toString() {
        if(floating)
            return "" + value;
        else
            return "" + (int)value;
    }
}
