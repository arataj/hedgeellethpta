/*
 * TADDValue.java
 *
 * Created on May 8, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.values;

import java.util.*;

/**
 * A value in TADD, either a constant or a variable.<br>
 * 
 * It is not an expression, as expressons have less functionality, for example
 * can be operands only in <code>PTANestedExpression</code>.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractPTAValue {
    /**
     * True for floating point values, false for integer values.
     */
    public boolean floating;
    
    /**
     * Creates a new instance of TADDValue.
     * 
     * @param floating                  if a floating point or integer value or array of
     *                                  values
     */
    public AbstractPTAValue(boolean floating) {
        this.floating = floating;
    }
    /**
     * Returns all variables used in this value.
     * 
     * @return a set of variables, sorted for determined compiler output
     */
    abstract public SortedSet<AbstractPTAVariable> getVariables();
    /*
     * Returns a textual description of this value, as it usually
     * would appear in the code, for example a constant's value,
     * a variable's name or, if a is an array and b is an index,
     * "a[b]".
     */
    // public abstract String getCodeString();
    @Override
    public boolean equals(Object o) {
        // subclasses must implement this method
        throw new RuntimeException("not implemented");
    }
}
