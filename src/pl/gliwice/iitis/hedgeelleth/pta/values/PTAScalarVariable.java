/*
 * IntegerTADDVariable.java
 *
 * Created on Jan 22, 2009
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.values;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.pta.PTARange;

/**
 * A scalar TADD variable.
 * 
 * @author Artur Rataj
 */
public class PTAScalarVariable extends AbstractPTAVariable {
    /**
     * Initial value of this variable.
     */
    public double initValue;
    /**
     * If this variable is known to be derived from a local variable. Enables
     * additional optimizations in the backend. Not used outside the backend.
     */
    public boolean backendLocal;
    /**
     * If this variable is known to be derived from an internal local variable. Enables
     * additional optimizations in the backend. Not used outside the backend.<br>
     * 
     * If true, then <code>backendLocal</code> is true as well.
     */
    public boolean backendInternal;
    
    /**
     * Creates a new instance of PTAScalarVariable, with the default
     * initial value of 0.
     *
     * @param name                      name of this variable
     * @param range                     range of values, if null, then
     *                                  must be completed later
     * @param floating                  if a floating point or integer variable
     */
    public PTAScalarVariable(String name,
            PTARange range, boolean floating) {
        super(name, range, floating);
        initValue = 0;
    }
    @Override
    public SortedSet<AbstractPTAVariable> getKnownSources() {
        return new TreeSet<>();
    }
    @Override
    public String toString() {
        String s;
        if(floating)
            s = "" + initValue;
        else
            s = "" + (int)initValue;
        return super.toString() + "(<<" + s + ">>)";
    }
}
