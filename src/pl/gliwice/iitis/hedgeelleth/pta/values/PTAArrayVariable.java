/*
 * IntegerArrayTADDVariable.java
 *
 * Created on Jan 22, 2009
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.values;

import java.util.*;

/**
 * An array of bounded values.<br>
 *
 * @author Artur Rataj
 */
public class PTAArrayVariable extends AbstractPTAVariable {
    /**
     * Length of this array.
     */
    public final int length;
    /**
     * Initial value of this variable.
     */
    public final double[] initValue;

    /**
     * Creates a new instance of IntegerArrayTADDVariable, with the default
     * initial element values of 0.<br>
     *
     * This variable has a special way of determining the range of its
     * elements -- see the method
     * <code>BackendPTAArrayVariable.registerVariableRange</code>
     * for details.
     *
     * 
     * @param name                      name of this variable
     * @param length                    length of this variable
     * @param initValue                 initial values, to be verified in
     *                                  <code>BackendPTAArrayVariable.registerVariableRange</code>
     * @param floating                  if elements of a floating point type
     */
    public PTAArrayVariable(String name,
            int length, double[] initValue, boolean floating) {
        super(name, null, floating);
        if(length < 0)
            throw new RuntimeException("invalid array length: " + length);
        this.length = length;
        this.initValue = new double[this.length];
        for(int i = 0; i < this.length; ++i)
            this.initValue[i] = initValue[i];
    }
    @Override
    public SortedSet<AbstractPTAVariable> getKnownSources() {
        return new TreeSet<>();
    }
        /*
    @Override
    public String getCodeString() {
        return name;
    }
     */
    @Override
    public String toString() {
        String name = super.toString() + " (length = " + length + " contents = [";
        double previous = 0;
        int repeated = 0;
        for(int i = 0; i < length; ++i) {
            double current = initValue[i];
            if(i == 0)
                // just to make it different from the current
                // value
                previous = current - 1;
            if(current != previous) {
                if(repeated > 5)
                    name += " rep. " + previous + " tim.";
                else
                    for(int j = 1; j < repeated; ++j)
                        name += " " + previous;
                if(floating)
                    name += " <<" + current + ">>";
                else
                    name += " <<" + (int)current + ">>";
                repeated = 1;
            } else
                ++repeated;
            previous = current;
            if(i > 100) {
                name += " too long to display, truncating";
                break;
            }
        }
        name += " ])";
        return name;
    }
}
