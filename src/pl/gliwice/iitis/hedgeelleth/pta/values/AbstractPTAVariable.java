/*
 * AbstractPTAVariable.java
 *
 * Created on Apr 21, 2008
 *
 * Copyright (c) 2008 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.values;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.pta.PTARange;

/**
 * An abstract variable of a TADD. TADD variables are runtime ones.<br>
 * 
 * Each variable is represented by a single instance, expect for element variables,
 * which may have separate instances, for example in each subsequent indexing of
 * some array.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractPTAVariable extends AbstractPTAValue implements Comparable {
    /**
     * Name of this variable, unique within a system of PTAs.
     */
    public final String name;
    /**
     * Range allowed for this variable or, if it is an array,
     * its elements.
     */
    public PTARange range;
    /**
     * A comment about this variable. Null inside the backend. Outside the backend
     * can be not null, and typically is the original name of the variable.
     */
    public String comment;
    /**
     * If the range of values written to this variable is known, then represents
     * that range. Otherwise null. Min and max are always inclusive. Not used outside
     * the backend.
     */
    public PTARange backendWriteRange;
    
    /**
     * Creates a new instance of AbstractPTAVariable, with the default
     * initial value of 0.
     * 
     * @param name                      name of this variable
     * @param range                     range allowed for this variable or,
     *                                  if it is an array, its elements,
     *                                  if null, must be completed later
     * @param floating                  if a floating point or integer variable
     */
    public AbstractPTAVariable(String name,
            PTARange range, boolean floating) {
        super(floating);
        this.name = name;
        this.range = range;
    }
    @Override
    public SortedSet<AbstractPTAVariable> getVariables() {
        SortedSet<AbstractPTAVariable> out = getKnownSources();
        out.add(this);
        return out;
    }
    /**
     * Returns all variables within this one, that are known sources,
     * e. g. variable indices. This variable is not returned, as it is unknown
     * without a context, if this variable is a source or a target.
     * 
     * @return a set of variables, sorted for determined compiler output
     */
    abstract public SortedSet<AbstractPTAVariable> getKnownSources();
    /*
    @Override
    public String getCodeString() {
        return name;
    }
     */
    @Override
    public boolean equals(Object o) {
        if(o instanceof AbstractPTAVariable)
            return name.equals(((AbstractPTAVariable)o).name);
        else
            return false;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.name);
        return hash;
    }
    @Override
    public int compareTo(Object o) {
        if(o instanceof AbstractPTAVariable) {
            AbstractPTAVariable other = (AbstractPTAVariable)o;
            int relation = name.compareTo(other.name);
            if(relation == 0 && (this != other) &&
                    // element variables are allowed to be not uniquely identified
                    // by instances
                    !(this instanceof PTAElementVariable))
                throw new RuntimeException("unexpected lack of uniqueness");
            return relation;
        } else
            throw new RuntimeException("invalid comparison");
    }
    @Override
    public String toString() {
        // a container's name is usually included in the name of
        // this variable, see <code>PTACompilation.newVariable</code>
        // for example of constructing a TADD variable
        String s = name;
        if(range != null)
            s += range.toString();
        return s;
    }
}
