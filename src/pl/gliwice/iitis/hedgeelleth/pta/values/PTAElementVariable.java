/*
 * PTAElementVariable.java
 *
 * Created on Mar 11, 2009, 11:23:12 AM
 *
 * Copyright (c) 2009 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta.values;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.pta.PTARange;

/**
 * Represents an array's element. It is just a compound of an array
 * variable and an index value. It is itself never registered as
 * a separate TADD variable, and a single array's element might be
 * represented many times by objects of <code>PTAElementVariable</code>.
 *
 * @author Artur Rataj
 */
public class PTAElementVariable extends AbstractPTAVariable {
    /**
     * Array.
     */
    public final PTAArrayVariable array;
    /**
     * Index.
     */
    public final AbstractPTAValue index;

    /**
     * Creates a new instance of PTAElementVariable.
     *
     * @param array                     array this element belongs to
     * @param index                     index of this element within
     *                                  the array
     * @param range                     range of values, if null, then
     *                                  must be completed later
     * @param floating                  if of a floating point type
     */
    public PTAElementVariable(PTAArrayVariable array,
            AbstractPTAValue index, PTARange range, boolean floating) {
        super(array.name + "[" + index.toString() + "]",
                range, floating);
        this.array = array;
        this.index = index;
    }
    @Override
    public SortedSet<AbstractPTAVariable> getKnownSources() {
        SortedSet<AbstractPTAVariable> out = new TreeSet<>();
        out.addAll(index.getVariables());
        return out;
    }
    /*
    @Override
    public String getCodeString() {
        return array.name + "[" + index.getCodeString() + "]";
    }
     */
    @Override
    public String toString() {
        return array.toString() + " [ " + index.toString() + " ]";
    }
}
