/*
 * PTASystem.java
 *
 * Created on Apr 21, 2008
 *
 * Copyright (c) 2008, 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type.TypeRange;
import pl.gliwice.iitis.hedgeelleth.compiler.util.ModelType;
import pl.gliwice.iitis.hedgeelleth.compiler.util.PSpace;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAVariable;

/**
 * A system of PTAs, with common variables and synchronization.
 *
 * @author Artur Rataj
 */
public class PTASystem {
    /**
     * Type of this system. In the backend can be null before determined.
     */
    public ModelType type;
    /**
     * P space, default is the plain probability space.
     */
    public PSpace pSpace = new PSpace();
    /**
     * Variables, keyed with their names.<br>
     * 
     * This field is not used in the backend, which uses
     * <code>BackendPTACompilation.variables</code>
     * instead. In effect, this field is not null only outside the backend.
     */
    public SortedMap<String, AbstractPTAVariable> variables;
    /**
     * Labels, keyed with their names.
     */
    public SortedMap<String, PTALabel> labels;
    /**
     * PTAs, keyed with their names. Includes lock PTAs.
     */
    public SortedMap<String, PTA> ptas;
    /**
     * Player PTAs keyed with names. Empty map for no players.<br>
     */
    public SortedMap<String, List<PTA>> playerPTAs;
    /**
     * Player labels keyed with names. Empty map for no players.
     * The set of keys should be equal to that in <code>playerPTAs</code>.
     */
    public SortedMap<String, List<PTALabel>> playerLabels;
    /**
     * Names of players, in the order of declaration. This list is for
     * backends that identify players by numbers.
     */
    public List<String> playerList;
    /**
     * An implicit scheduler, that should be applied to this system. Null
     * if none.
     */
    public SchedulerType implicitScheduler;
    /**
     * Variable representing the error, corresponding to the wrapping
     * <code>BackendPTACompilation.errorCodeVariable</code>.
     */
    public AbstractPTAVariable errorVariable;
    
    /**
     * Creates a new instance of PTASystem. It sets <code>variables<code>
     * and <code>labels</code> to null.
     */
    public PTASystem() {
        variables = null;
        labels = null;
        ptas = new TreeMap<>();
        playerPTAs = new TreeMap<>();
        playerLabels = new TreeMap<>();
        playerList = new LinkedList<>();
        implicitScheduler = null;
    }
    /**
     * Wraps integer or floating--point, depending on type, minimum value into a double.
     * 
     * @param range range
     * @return double minimum value
     */
    public static double getMin(TypeRange range) {
        if(range.isFloating())
            return range.getMinF();
        else
            return (long)range.getMinI();
    }
    /**
     * Wraps integer or floating--point, depending on type, maximum value into a double.
     * 
     * @param range range
     * @return double maximum value
     */
    public static double getMax(TypeRange range) {
        if(range.isFloating())
            return range.getMaxF();
        else
            return (long)range.getMaxI();
    }
    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for(PTA pta : ptas.values())
            out.append(pta.toString());
        return out.toString();
    }
}
