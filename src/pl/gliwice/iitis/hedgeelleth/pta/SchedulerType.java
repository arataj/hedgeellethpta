/*
 * SchedulerType.java
 *
 * Created on Aug 22, 2013
 *
 * Copyright (c) 2013  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

/**
 * Type of a scheduler. In the case of <code>PTASystem</code> used
 * only if the scheduler is implicit, as in the other case the scheduling
 * rules are explicitly written to the PTAs.
 */
public enum SchedulerType {
    /**
     * No additional time constraints. Typically, means that a state
     * can last an arbitrary time.
     */
    FREE,
    /**
     * Make all states without explicit time constraints immediate.
     */
    IMMEDIATE,
    /**
     * Make all states without explicit time constraints hidden.
     * 
     * @see PTAState.isHidden()
     */
    HIDDEN,
    /**
     * Make all states without explicit time last a given constant
     * time period.
     */
    CONST,
    /**
     * Make all states without explicit time last an exponential time.
     */
    NXP,
};
