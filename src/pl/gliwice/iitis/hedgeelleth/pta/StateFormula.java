/*
 * StateFormula.java
 *
 * Created on Nov 9, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import java.util.*;
import java.util.regex.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * Represents a state formula in a PTA.
 * 
 * @author Artur Rataj
 */
public class StateFormula implements Comparable {
    /**
     * Name of this formula.
     */
    public String name;
    /**
     * Type of this formula: true for OR, false for AND. Specifies the operator,
     * by which multiple states of this formula are joined.
     */
    public boolean or;
    /**
     * States within this formula.<br>
     * 
     * Must be sorted for determined error messages.

     */
    public SortedSet<PTAState> states;
    /**
     * Position of the operations, from which this formula originates,
     * for each of the state. Some states can be missing from this map,
     * the respective operations are none or unknown.<br>
     * 
     * Must be sorted for determined error messages.
     */
    public SortedMap<PTAState, StreamPos> pos;
    /**
     * A pattern for checking of the validity of formula's name.
     */
    private static Pattern nameBegins = Pattern.compile("[^a-zA-Z]");
    /**
     * A pattern for checking of the validity of formula's name.
     */
    private static Pattern nameFurther = Pattern.compile("[^a-zA-Z0-9]");
    
    /**
     * Creates a new state formula.<br>
     * 
     * The collections must be sorted for determined error messages.
     * 
     * @param name name of the formula to create; must contain only
     * ASCII alphanumeric characters and must begin with an ASCII letter,
     * or <code>PTAException<code> is thrown
     * @param or true for OR formula, false for AND formula
     * @param states states within the formula
     * @param pos copied to <code>pos<code>, can be null for no
     * positions
     */
    public StateFormula(String name, boolean or, SortedSet<PTAState> states,
            SortedMap<PTAState, StreamPos> pos) throws PTAException {
        if(pos == null)
            pos = new TreeMap<>();
        if(nameBegins.matcher(name.substring(0, 1)).find() ||
                nameFurther.matcher(name.substring(1)).find()) {
            StreamPos p;
            if(pos.values().isEmpty())
                p = null;
            else
                p = pos.values().iterator().next();
            throw new PTAException(p, "state formula contains " +
                    "invalid characters: `" + name + "'");
        }
        this.name = name;
        this.or = or;
        this.states = states;
        this.pos = pos;
    }
    @Override
    public int compareTo(Object o) {
        return name.compareTo(((StateFormula)o).name);
    }
    @Override
    public String toString() {
        String out = name + " " + (or ? "|" : "&") + " [ ";
        for(PTAState s : states)
            out += s.index + " ";
        out += "]";
        return out;
    }
}
