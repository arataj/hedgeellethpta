/*
 * PTAState.java
 *
 * Created on May 8, 2008
 *
 * Copyright (c) 2008, 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.code.op.AbstractCodeOp;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAConstant;

/**
 * <p>A PTA location, holds output transitions.</p>
 * 
 * <p>This class is a misnomer -- it is really a location</p>.
 * 
 * @author Artur Rataj
 */
public class PTAState implements Comparable<PTAState> {
    /**
     * Owner of this state. Assigned only in <code>TADDFactory.checkConnectivity()</code>.
     */
    public PTA owner;
//    /**
//     * Key of transitions with no target state. The key is used by
//     * the field <code>output</code>.
//     */
//    public static final int OUTPUT_NO_STATE_INDEX = Integer.MIN_VALUE;
    /**
     * Index of this state.
     */
    public int index;
    /**
     * Input transitions of this state, keyed with indices of
     * their source states.
     */
    public SortedMap<Integer, SortedSet<PTATransition>> input;
    /**
     * Output transitions of this state, keyed with indices of
     * their target states.
     */
    public SortedMap<Integer, SortedSet<PTATransition>> output;
    /**
     * Clock condition for equality or upper limit, null for none.
     * Condition of the form <code>x &lt; 0</code> means, that a
     * state is not only urgent, but also invisible.<br>
     * 
     * If not null, <code>TADDFactory</code> adds a respective clock reset
     * to all transitions, that point to this state.<br>
     * 
     * Set by for example by immediate states or schedulers.
     */
    public PTAClockCondition clockLimitHigh = null;
    /**
     * A flat list of inputs, in the order specified by <code>input</code>.<br>
     * 
     * This cache is only created after the PTA is fully connected, and
     * <code>getFlatInput()</code> is called. Before that, it is null.
     */
    protected Collection<PTATransition> inputListCache = null;
    /**
     * A flat list of outputs, in the order specified by <code>input</code>.<br>
     * 
     * This cache is only created after the PTA is fully connected, and
     * <code>getFlatOutput()</code> is called. Before that, it is null.
     */
    protected Collection<PTATransition> outputListCache = null;
    
    /**
     * Creates a new instance of PTAState.
     * 
     * @param index                     index of this state
     */
    public PTAState(int index) {
        this.index = index;
        /*if(index == -5)
            index = index;*/
        input = new TreeMap<>();
        output = new TreeMap<>();
    }
    /**
     * If any of the output connections can introduce a delay, by
     * sincing or its clock condition.
     * 
     * @return if a potential delay found in the output transitons
     */
    protected boolean outputDelayFound() {
        for(Set<PTATransition> set : output.values())
            for(PTATransition tt : set)
                if(tt.label != null || tt.clockLimitLow != null)
                    return true;
        return false;
    }
    /**
     * If clock invariant is of the form <code>x &lt;= 0</code> or
     * <code>x &lt; 0</code>, and there is no delay possible
     * within the output transitons.
     * 
     * @param backendImplicitHidden as in <code>isHidden()<code>;
     * possibly true only within the backend
     * @return if this state is immediate
     */
    public boolean isImmediate(boolean backendImplicitHidden) {
        if(outputDelayFound())
            return false;
        if(backendImplicitHidden && clockLimitHigh == null)
            return true;
        return clockLimitHigh != null &&
            clockLimitHigh.getLimitHigh() instanceof PTAConstant &&
                ((PTAConstant)clockLimitHigh.getLimitHigh()).value == 0.0;
    }
    /**
     * If this state is immediate. This is a convenience method, that calls
     * <code>isImmediate(false)</code>, and is to be used outside
     * the backend.
     * 
     * @return if this state is immediate
     */
    public boolean _isImmediate() {
        return isImmediate(false);
    }
    /**
     * If clock invariant is of the form <code>x &lt; 0</code>, and
     * there is no delay possible within the output transitions.<br>
     * 
     * A hidden state is always also an immediate one.
     * 
     * @param backendImplicitHidden if this state's PTA has a scheduler,
     * that implicitly makes all states, that have no clock invariants,
     * and whose output transitions have no potential delay, hidden;
     * possibly true only within the backend
     * @return if this state is hidden
     */
    public boolean isHidden(boolean backendImplicitHidden) {
        if(outputDelayFound())
            return false;
        if(backendImplicitHidden && clockLimitHigh == null)
            return true;
        return isImmediate(backendImplicitHidden) &&
                clockLimitHigh.operator == AbstractCodeOp.Op.BINARY_LESS;
    }
    /**
     * If this state is hidden. This is a convenience method, that calls
     * <code>isHidden(false)</code>, and is to be used outside
     * the backend.
     * 
     * @return if this state is hidden
     */
    public boolean _isHidden() {
        return isHidden(false);
    }
    /**
     * Adds an output transition to this state. Also takes care about
     * connecting the other end of <code>tt</code>.<br>
     * 
     * A connection of a transition identical to some other parallel transition
     * will be ignored as redundant. The transitions must be different
     * at least at the level of comments to coexist in a PTA.
     * 
     * @param tt                        transition to add
     */
    public void addOutputTransition(PTATransition tt) {
        if(inputListCache != null || outputListCache != null)
            throw new RuntimeException("connections already cached");
        if(tt.sourceState != this)
            throw new RuntimeException("mismatch of source states");
        int targetState;
        if(tt.targetState != null)
            targetState = tt.targetState.index;
        else
            //targetState = OUTPUT_NO_STATE_INDEX;
            throw new RuntimeException("adding a transition with " +
                    "missing target state index");
        /*if(tt.toString().indexOf("c3") != -1)
            tt = tt;*/
        SortedSet<PTATransition> set = output.get(targetState);
        if(set == null) {
            set = new TreeSet<>();
            output.put(targetState, set);
        }
        set.add(tt);
        set = tt.targetState.input.get(tt.sourceState.index);
        if(set == null) {
            set = new TreeSet<>();
            tt.targetState.input.put(tt.sourceState.index, set);
        }
        set.add(tt);
    }
    /**
     * Returns the number of input transitions.
     * 
     * @return number of transitions
     */
    public int getInputsNum() {
        int count = 0;
        for(Set<PTATransition> set : input.values())
            count += set.size();
        return count;
    }
    /**
     * Returns the number of output transitions.
     * 
     * @return number of transitions
     */
    public int getOutputsNum() {
        int count = 0;
        for(Set<PTATransition> set : output.values())
            count += set.size();
        return count;
    }
    /**
     * If this state has a single input transition, then that transition is
     * returned. Otherwise, a runtime exception is thrown.
     * 
     * @return input transition
     */
    public PTATransition getSingleInput() {
        if(getInputsNum() != 1)
            throw new RuntimeException("an exactly single input expected");
        return input.values().iterator().next().iterator().next();
    }
    /**
     * If this state has a single output transition, then that transition is
     * returned. Otherwise, a runtime exception is thrown.
     * 
     * @return output transition
     */
    public PTATransition getSingleOutput() {
        if(getOutputsNum() != 1)
            throw new RuntimeException("an exactly single output expected");
        return output.values().iterator().next().iterator().next();
    }
    /**
     * Returns a set of input transitions, in a flat form.
     * If the cache does not exist, it is created. This method should
     * be called only after connections in the PTA are no more
     * added or removed.<br>
     * 
     * This method is never used by the backend, nor by
     * <code>PTAIO</code>. These use <code>input</code>
     * instead. The method should be called only by a
     * code not directly related to the construction of the automatons,
     * but rather one that reads the automatons.
     * 
     * @return a list of input transitions
     */
    public Collection<PTATransition> getFlatInput() {
        if(inputListCache == null) {
            inputListCache = new LinkedList<>();
            for(Set<PTATransition> set : input.values())
                for(PTATransition tt : set)
                    inputListCache.add(tt);
        }
        return inputListCache;
    }
    /**
     * Returns a cache of output transitions, in a flat form.
     * If the cache does not exist, it is created. This method should
     * be called only after connections in the PTA are no more
     * added or removed.<br>
     * 
     * This method is never used by the backend, nor by
     * <code>PTAIO</code>. These use <code>output</code>
     * instead. The method should be called only by a
     * code not directly related to the construction of the automatons,
     * but rather one that reads the automatons.
     * 
     * @return a list of input transitions
     */
    public Collection<PTATransition> getFlatOutput() {
        if(outputListCache == null) {
            outputListCache = new LinkedList<>();
            for(Set<PTATransition> set : output.values())
                for(PTATransition tt : set)
                    outputListCache.add(tt);
        }
        return outputListCache;
    }
    /**
     * If outputs of this state form a given type of branch.
     * 
     * @param type type of branch to detect
     * @param mark if the branch of a given type is detected, if to
     * mark the transitions with a respective branch identifier;
     * if a transition to mark is already marked, a runtime exception
     * is thrown
     * @param ignoreNested if a nested expression is found,
     * then if this parameter is true, nothing is modified and false is returned;
     * if this parameter is false, a runtime exception is thrown
     * @return if a given branch type has been detected
     */
    public boolean qualifiesForBranchId(BranchId.Type type,
            boolean mark, boolean ignoreNested) {
        List<PTATransition> transitions = new LinkedList<>();
        PTALabel label = null;
        boolean branch = true;
        boolean guardsFound = false;
        SCAN:
        for(Set<PTATransition> set : output.values())
            for(PTATransition tt : set) {
                if(label != null && label != tt.label) {
                    branch = false;
                    break SCAN;
                }
                if(tt.probability != null) {
                    branch = false;
                    break SCAN;
                }
                if(tt.guard != null)
                    guardsFound = true;
                transitions.add(tt);
                label = tt.label;
            }
        if(branch && transitions.size() > 1) {
            BranchId id = null;
            switch(type) {
                case CONDITIONAL:
                {
                    if(guardsFound) {
                        boolean overlap = false;
                        // a conditional branch is required to have a number of
                        // transitions with non--overlapping guards
                        SCAN:
                        for(PTATransition ttX : transitions)
                            for(PTATransition ttY : transitions)
                                if(ttX != ttY) {
                                    AbstractPTAExpression guard1 = ttX.guard;
                                    AbstractPTAExpression guard2 = ttY.guard;
                                    if(guard1 instanceof PTANestedExpression ||
                                            guard2 instanceof PTANestedExpression) {
                                        if(ignoreNested) {
                                            overlap = true;
                                            break SCAN;
                                        } else
                                            throw new RuntimeException("can not test a nested expression");
                                    } else {
                                        try {
                                            if(guard1 == null || guard2 == null ||
                                                    guard1.overlaps(guard2)) {
                                                overlap = true;
                                                break SCAN;
                                            }
                                        } catch(ArithmeticException e) {
                                            // can not test overlapping
                                            overlap = true;
                                            break SCAN;
                                        }
                                    }
                                }
                        if(!overlap)
                            id = new BranchId(type);
                    }
                    break;
                }
                case NONDETERMINISTIC:
                {
                    if(!guardsFound)
                        // a nondeterministic branch is required to have no
                        // guards
                        id = new BranchId(type);
                    break;
                }
                default:
                    throw new RuntimeException("bad type");
            }
            if(id != null) {
                if(mark)
                    for(PTATransition tt : transitions) {
                        if(tt.branchId != null)
                            throw new RuntimeException("branch already marked");
                        if(tt.ndInt != null)
                            throw new RuntimeException("unexpected nd ids");
                        tt.branchId = id;
                    }
                return true;
            }
        }
        return false;
    }
    @Override
    public int compareTo(PTAState s) {
        int otherIndex = s.index;
        if(index < otherIndex)
            return -1;
        else if(index > otherIndex)
            return 1;
        else
            return 0;
    }
    @Override
    public String toString() {
        String s = "" + index;
        if(clockLimitHigh != null)
            s += " (" + clockLimitHigh.toString() + ")";
        s += " {\n";
        for(Set<PTATransition> set : output.values())
            for(PTATransition tt : set) {
                s += "  " + tt.toString() + "\n";
            }
        s += "}\n";
        return s;
    }
}
