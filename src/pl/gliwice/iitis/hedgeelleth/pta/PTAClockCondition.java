/*
 * PTAClockCondition.java
 *
 * Created on May 15, 2008
 *
 * Copyright (c) 2008, 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.pta;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.pta.values.PTAClock;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAValue;
import pl.gliwice.iitis.hedgeelleth.compiler.code.op.CodeOpBinaryExpression;
import pl.gliwice.iitis.hedgeelleth.pta.values.AbstractPTAVariable;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTAConstant;
import pl.gliwice.iitis.hedgeelleth.pta.values.PTADensity;

/**
 * A binary expression representing a clock condition.
 * 
 * It has the following additional constraints: the left part
 * must be the clock, the right part must be a constant,
 * target must be null.
 * 
 * @author Artur Rataj
 */
public class PTAClockCondition extends PTABinaryExpression {
    /**
     * If the clock is on the left or right side of the equation.
     */
    protected final boolean clockOnLeft;
    
    /**
     * Creates a new instance of PTAClockCondition, with clock on the right.
     * 
     * @param value                  value, it is the left part
     *                                  of this expression
     * @param clock                     clock, it is the right part
     *                                  of this expression
     * @param operator                  operator, must be = or &lt; or &lt;=
     */
    public PTAClockCondition(AbstractPTAValue value, PTAClock clock,
            CodeOpBinaryExpression.Op operator) {
        super(value, clock, operator);
        if(!validOperator(this.operator))
            throw new RuntimeException("invalid operator");
        clockOnLeft = false;
    }
    /**
     * Returns, if a given operator is valid for a clock condition.
     * 
     * @param operator operator to test
     * @return if valid for <code>PTAClockCondition</code>
     */
    public static boolean validOperator(CodeOpBinaryExpression.Op operator) {
        switch(operator) {
            case BINARY_EQUAL:
            case BINARY_LESS:
            case BINARY_LESS_OR_EQUAL:
                return true;

            default:
                return false;
        }
    }
    /**
     * Creates a new instance of PTAClockCondition, with clock on the left.
     * 
     * @param clock                     clock, it is the left part
     *                                  of this expression
     * @param value                  value, it is the right part
     *                                  of this expression
     * @param operator                  operator
     */
    public PTAClockCondition(PTAClock clock, AbstractPTAValue value,
            CodeOpBinaryExpression.Op operator) {
        super(clock, value, operator);
        clockOnLeft = true;
    }
    /**
     * Returns the clock related to this condition.
     * 
     * @return                          clock
     */
    public PTAClock getClock() {
        if(clockOnLeft)
            return (PTAClock)left;
        else
            return (PTAClock)right;
    }
    /**
     * Returns the value in this condition.
     * 
     * @return                          constant
     */
    public AbstractPTAValue getValue() {
        if(clockOnLeft)
            return (AbstractPTAValue)right;
        else
            return (AbstractPTAValue)left;
    }
    /**
     * If the operator is different from "&lt;". If yes, the non--null value reported by
     * <code>getLimitLow()</code> or <code>getLimitHight()</code>
     * is inclusive.
     * 
     * @return if the limits are inclusive
     */
    public boolean isLimitInclusive() {
        return operator != CodeOpBinaryExpression.Op.BINARY_LESS;
    }
    /**
     * Returns the lower limit on the clock, null for none.
     * 
     * @return minimum possible clock value
     */
    public AbstractPTAValue getLimitLow() {
        switch(operator) {
            case BINARY_LESS:
            case BINARY_LESS_OR_EQUAL:
                if(clockOnLeft)
                    return null;
                break;
                
            case BINARY_EQUAL:
                break;

            default:
                throw new RuntimeException("unknown operator");
        }
        return getValue();
    }
    /**
     * Returns the higher limit on the clock, null for none.
     * 
     * @return maximum possible clock value
     */
    public AbstractPTAValue getLimitHigh() {
        switch(operator) {
            case BINARY_LESS:
            case BINARY_LESS_OR_EQUAL:
                if(!clockOnLeft)
                    return null;
                break;
                
            case BINARY_EQUAL:
                break;

            default:
                throw new RuntimeException("unknown operator");
        }
        return getValue();
    }
    @Override
    public PTAConstant evaluate(Map<AbstractPTAVariable,
            PTAConstant> knowns) {
        throw new RuntimeException("can not evaluate a clock " +
                "condition");
    }
    @Override
    public PTAClockCondition clone() {
        PTAClockCondition copy = (PTAClockCondition)super.clone();
        // <code>clockOnLeft</code> is a primitive
        return copy;
    }
    /**
     * If the expression contains <code>PTADensity</code>.
     * 
     * @return if involves comparison to a probability distribution
     */
    public boolean containsDensity() {
        return left instanceof PTADensity || right instanceof PTADensity;
    }
}
